# hcc-flow

#### Description
hcc-flow是一个轻量级的流程系统，全开源，无套路，无挖矿植入，无流量偷跑，无广告植入
后端：spring boot2.0+、spring security、redis/ehcache[修改配置参数随意切换]、myBatis、pageHelper等框架
前端：html、bootstrap、layui、jqury、dataTable、canvas[流程设计]等
功能模块：流程模型（业务类型、流程设计图）、流程业务（相关流程任务发起及管理）、流程工作台（流程任务处理、状态信息反馈）、系统管理（机构/角色/用户管理、菜单管理、字典管理、日志管理、代码生成）、等。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
