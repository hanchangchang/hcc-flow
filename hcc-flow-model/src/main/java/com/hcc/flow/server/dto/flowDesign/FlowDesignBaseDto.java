package com.hcc.flow.server.dto.flowDesign;

import java.io.Serializable;

/**
 * 工作流 流程
 * @author 韩长志 2019-11-19
 *
 */
public class FlowDesignBaseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6096761967550851049L;
	private String flowId;//流程id
	private String flowCode;//流程编码
	private String flowName;//流程名称
	private String flowType;//流程归属类型(1素材2订单3刊播4换刊5客户6品牌)
	private String flowAttribute = "0";//流程属性 0普通流程 1复合流程
	private String followOrgId;//归属机构id
	private String flowRemarks;//流程说明

	public String getFlowId() {
		return flowId;
	}
	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}
	public String getFlowName() {
		return flowName;
	}
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	public String getFlowType() {
		return flowType;
	}
	public void setFlowType(String flowType) {
		this.flowType = flowType;
	}
	public String getFollowOrgId() {
		return followOrgId;
	}
	public void setFollowOrgId(String followOrgId) {
		this.followOrgId = followOrgId;
	}
	public String getFlowCode() {
		return flowCode;
	}
	public void setFlowCode(String flowCode) {
		this.flowCode = flowCode;
	}
	public String getFlowRemarks() {
		return flowRemarks;
	}
	public void setFlowRemarks(String flowRemarks) {
		this.flowRemarks = flowRemarks;
	}
	public String getFlowAttribute() {
		return flowAttribute;
	}
	public void setFlowAttribute(String flowAttribute) {
		this.flowAttribute = flowAttribute;
	}
	
}
