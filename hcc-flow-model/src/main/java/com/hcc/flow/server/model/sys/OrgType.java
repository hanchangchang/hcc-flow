package com.hcc.flow.server.model.sys;

import java.io.Serializable;

/**
 * 机构类型实体
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class OrgType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2332081177517585872L;

	private Long orgTypeId;// 机构类型ID
	private String orgTypeName;// 机构类型名称
	private String status;// 状态（C正常R停用）
	

	public Long getOrgTypeId() {
		return orgTypeId;
	}

	public void setOrgTypeId(Long orgTypeId) {
		this.orgTypeId = orgTypeId;
	}

	public String getOrgTypeName() {
		return orgTypeName;
	}

	public void setOrgTypeName(String orgTypeName) {
		this.orgTypeName = orgTypeName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
