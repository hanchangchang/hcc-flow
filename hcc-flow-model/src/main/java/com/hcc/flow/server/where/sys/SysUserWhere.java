package com.hcc.flow.server.where.sys;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.hcc.flow.server.common.enums.DataStatusType;
import com.hcc.flow.server.common.page.table.PageTableRequest;
import com.hcc.flow.server.common.utils.CommUtil;
import com.hcc.flow.server.common.utils.StringUtilsV2;
import com.hcc.flow.server.common.where.PublicPara;

/**
 * @author 韩长志 2019-11-19
 * 
 */
public class SysUserWhere extends PublicPara implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String userName;// 用户名称
	String userAcct;// 账号
	List<String> orgIds;// 机构IDs
	String orgId;// 机构IDs
	List<String> depIds;// 部门IDs
	String depId;// 部门ID
	List<String> roldIds;// 角色IDs
	String phone;// 手机
	String status;// 状态
	String sqlOrder;
	Integer offset;
	Integer limit;
	List<String> statuss;// 状态
	String notStatus = DataStatusType.DELETE.getCode();// 不用查询的状态

	public String getNotStatus() {
		return notStatus;
	}

	public void setNotStatus(String notStatus) {
		this.notStatus = notStatus;
	}

	public List<String> getStatuss() {
		return statuss;
	}

	public void setStatuss(List<String> statuss) {
		this.statuss = statuss;
	}

	public String getSqlOrder() {
		return sqlOrder;
	}

	public void setSqlOrder(String sqlOrder) {
		this.sqlOrder = sqlOrder;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserAcct() {
		return userAcct;
	}

	public void setUserAcct(String userAcct) {
		this.userAcct = userAcct;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<String> getOrgIds() {
		return orgIds;
	}

	public void setOrgIds(List<String> orgIds) {
		this.orgIds = orgIds;
	}

	public List<String> getDepIds() {
		return depIds;
	}

	public void setDepIds(List<String> depIds) {
		this.depIds = depIds;
	}

	public List<String> getRoldIds() {
		return roldIds;
	}

	public void setRoldIds(List<String> roldIds) {
		this.roldIds = roldIds;
	}
	
	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
		if(StringUtilsV2.isNotBlank(orgId) && CollectionUtils.isEmpty(orgIds)) {
			this.orgIds = Arrays.asList(orgId);
		}
	}
	
	public String getDepId() {
		return depId;
	}

	public void setDepId(String depId) {
		this.depId = depId;
		if(StringUtilsV2.isNotBlank(depId) && CollectionUtils.isEmpty(depIds)) {
			this.depIds = Arrays.asList(depId);
		}
	}
	
	public SysUserWhere() {
		super();
	}

	public SysUserWhere(PageTableRequest strCon) {
		super();
		this.limit = strCon.getLimit();
		this.offset = strCon.getOffset();
		this.userName = CommUtil.null2String(strCon.getParams().get("userName"));
		this.userAcct = CommUtil.null2String(strCon.getParams().get("userAcct"));
		String orgId = CommUtil.null2String(strCon.getParams().get("orgId"));
		if(!"".equals(orgId)) {
			this.orgIds = Arrays.asList(orgId);
		}
	}

	

	
	
}
