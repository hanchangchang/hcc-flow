package com.hcc.flow.server.dto.sys;

import java.io.Serializable;

import com.hcc.flow.server.common.constant.Constant;



public class ChangePasswordDto implements Serializable {

	private static final long serialVersionUID = 421959216876101983L;
	private String oldPassword;
	private String newPassword;
	private String headImgUrl;

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getHeadImgUrl() {
		return headImgUrl;
	}

	public void setHeadImgUrl(String headImgUrl) {
		if(!Constant.PLATFORM_USER_IMG_MAN.equals(headImgUrl) && !Constant.PLATFORM_USER_IMG_WOMAN.equals(headImgUrl)) {
			this.headImgUrl = headImgUrl;
		}
	}

}
