package com.hcc.flow.server.model.common;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hcc.flow.server.common.constant.Constant;
import com.hcc.flow.server.common.utils.StringUtilsV2;
import com.hcc.flow.server.model.sys.SysUser;

public class LoginUser extends SysUser implements UserDetails {

private static final long serialVersionUID = -1379274258881257107L;
	
	/** 用户菜单权限列表  */
	private Set<String> permissions;
	
	private String token;
	/** 登陆时间戳（毫秒） */
	private Long loginTime;
	/** 过期时间戳 */
	private Long expireTime;
	
	/** 用户所属角色id,逗号隔开**/
	private String roldIds;
	
	private String orgName;
	
	private String parentOrgId;
	
	private String parentOrgName;
	
	@SuppressWarnings("unused")
	private Boolean isSuperAdminRole;
	
	public Set<String> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<String> permissions) {
		this.permissions = permissions;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getRoldIds() {
		return roldIds;
	}

	public void setRoldIds(String roldIds) {
		this.roldIds = roldIds;
	}

	@Override
	@JsonIgnore
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return permissions.parallelStream().map(p -> new SimpleGrantedAuthority(p)).collect(Collectors.toSet());
	}

	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		// do nothing
	}

	// 账户是否未过期
	@JsonIgnore
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	// 账户是否未锁定
	@JsonIgnore
	@Override
	public boolean isAccountNonLocked() {
		return getStatus() != "R";
	}

	// 密码是否未过期
	@JsonIgnore
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	// 账户是否激活
	@JsonIgnore
	@Override
	public boolean isEnabled() {
		return true;
	}

	public Long getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Long loginTime) {
		this.loginTime = loginTime;
	}

	public Long getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(Long expireTime) {
		this.expireTime = expireTime;
	}

	public String getParentOrgId() {
		return parentOrgId;
	}

	public void setParentOrgId(String parentOrgId) {
		this.parentOrgId = parentOrgId;
	}

	public String getParentOrgName() {
		return parentOrgName;
	}

	public void setParentOrgName(String parentOrgName) {
		this.parentOrgName = parentOrgName;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}


	public boolean getIsSuperAdminRole() {
		if(StringUtilsV2.isNotBlank(roldIds) && Arrays.asList(roldIds.split(",")).contains(Constant.SYSTEM_MANAGE_ROLE_ID)) {
			return true;
		}
		return false;
	}

}
