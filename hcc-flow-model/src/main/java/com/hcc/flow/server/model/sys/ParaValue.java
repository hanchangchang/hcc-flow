package com.hcc.flow.server.model.sys;

import com.hcc.flow.server.model.common.BaseModel;

/**
 * 参数实体
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class ParaValue extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6440972911667536686L;
	private String paraId;// 参数ID
	private Long paraTypeId;// 参数类型ID
	private String paraCode;// 参数编码
	private String paraName;// 参数名称
	private String paraValue;// 参数名称
	private String isDel;// 是否可删除(Y是N否)
	private String isUpdate;// 是否可修改(Y是N否)
	private int orderNo;// 排序

	// 不是数据库字段
	private String paraTypeName;// 参数类型名称

	public String getParaValue() {
		return paraValue;
	}

	public void setParaValue(String paraValue) {
		this.paraValue = paraValue;
	}

	public String getParaId() {
		return paraId;
	}

	public void setParaId(String paraId) {
		this.paraId = paraId;
	}

	public Long getParaTypeId() {
		return paraTypeId;
	}

	public void setParaTypeId(Long paraTypeId) {
		this.paraTypeId = paraTypeId;
	}

	public String getParaCode() {
		return paraCode;
	}

	public void setParaCode(String paraCode) {
		this.paraCode = paraCode;
	}

	public String getParaName() {
		return paraName;
	}

	public void setParaName(String paraName) {
		this.paraName = paraName;
	}

	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	public String getIsUpdate() {
		return isUpdate;
	}

	public void setIsUpdate(String isUpdate) {
		this.isUpdate = isUpdate;
	}

	public int getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(int orderNo) {
		this.orderNo = orderNo;
	}

	public String getParaTypeName() {
		return paraTypeName;
	}

	public void setParaTypeName(String paraTypeName) {
		this.paraTypeName = paraTypeName;
	}

}
