package com.hcc.flow.server.vo.sys;

import java.io.Serializable;
import java.util.List;

/**
 * 区域
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class AreaCheckVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4513193760099299996L;

	private Long parentId;// 上级区域ID
	private String parentName;// 上级区域名称
	private List<AreaVO> content;// 上级区域名称
	
	public List<AreaVO> getContent() {
		return content;
	}

	public void setContent(List<AreaVO> content) {
		this.content = content;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
}
