package com.hcc.flow.server.where.sys;

import java.io.Serializable;
import java.util.List;

import com.hcc.flow.server.common.enums.UseStatusType;
import com.hcc.flow.server.common.page.table.PageTableRequest;
import com.hcc.flow.server.common.utils.CommUtil;
import com.hcc.flow.server.common.where.PublicPara;

/**
 * @author 韩长长 
 * @version createTime：2018年9月28日 上午11:18:59
 * 
 */
public class RoleWhere extends PublicPara implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String roleName;// 角色名称

	String orgId;// 机构ID

	String sqlOrder;
	Integer offset;
	Integer limit;
	List<String> statuss;// 状态
	String notStatus = UseStatusType.DELETE.getCode();// 不用查询的状态

	public String getNotStatus() {
		return notStatus;
	}

	public void setNotStatus(String notStatus) {
		this.notStatus = notStatus;
	}

	public List<String> getStatuss() {
		return statuss;
	}

	public void setStatuss(List<String> statuss) {
		this.statuss = statuss;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getSqlOrder() {
		return sqlOrder;
	}

	public void setSqlOrder(String sqlOrder) {
		this.sqlOrder = sqlOrder;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	
	public RoleWhere() {
		super();
	}

	public RoleWhere(PageTableRequest strCon) {
		super();
		this.limit = strCon.getLimit();
		this.offset = strCon.getOffset();
		this.roleName = CommUtil.null2String(strCon.getParams().get("roleName"));
		this.orgId = CommUtil.null2String(strCon.getParams().get("orgId"));
	}
}
