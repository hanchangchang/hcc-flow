package com.hcc.flow.server.vo.sys;

import com.hcc.flow.server.common.enums.UseStatusType;
import com.hcc.flow.server.model.sys.Role;

/**
 * 角色实体
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class RoleVO extends Role {

	private static final long serialVersionUID = -3802292814767103648L;
	
	private String orgName;// 对应机构名称
	private String statusName;// 状态
	private String parentName;
	/*private String createTimeCheck;// 创建时间
	private String updateTimeCheck;// 更新时间
	@JsonIgnore
	private Date now = new Date();//当前日期
	
	public String getCreateTimeCheck() {
		return CommUtil.dateCheck(super.getCreateTime(),now);
	}

	public void setCreateTimeCheck(String createTimeCheck) {
		this.createTimeCheck = createTimeCheck;
	}

	public String getUpdateTimeCheck() {
		return CommUtil.dateCheck(super.getUpdateTime(),now);
	}

	public void setUpdateTimeCheck(String updateTimeCheck) {
		this.updateTimeCheck = updateTimeCheck;
	}*/
	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getStatusName() {
		return super.getStatus() != null ? UseStatusType.getName(super.getStatus()) : statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
}
