package com.hcc.flow.server.model.sys;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hcc.flow.server.common.utils.CommUtil;
import com.hcc.flow.server.common.utils.StringUtilsV2;

/**
 * 机构实体
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class Org implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2332081177517585832L;

	private Long indexId;//唯一标识id(只作展示用，不用作业务处理)
	
	private String orgId;// 机构ID
	private String orgCode;// 机构编码
	@JsonIgnore
	private String orgTypeId;// 机构类型ID
	private String orgTradeTypeId;// 机构商业类型s（书店、景区）
	private String orgName;// 机构名称
	private String orgDes;// 机构描述
	private String orgStatus;// 状态(C正常R停用D删除)
	private String createUserId;// 创建人ID
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date createTime;// 创建时间
	
	private String parentId;// 上级权限
	private Integer level;// 机构所在层级
	private String createOrgId;//创建人员所属机构
	private String resource;// 资源是否分配
	
	private String webLoginLogo;//网站登陆页log
	private String webLogo;//网站log
	private String webTitle;//网站标题
	
	private List<Integer> orgTypeIds;// 客户类型id
	private List<Integer> orgTradeTypeIds;// 客户商业类型id
	private String followUserId;//业务归属人
	private String followOrgId;//业务归属机构
	
	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public String getCreateOrgId() {
		return createOrgId;
	}

	public void setCreateOrgId(String createOrgId) {
		this.createOrgId = createOrgId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgTypeId() {
		return orgTypeId;
	}

	public void setOrgTypeId(String orgTypeId) {
		this.orgTypeId = orgTypeId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgDes() {
		return orgDes;
	}

	public void setOrgDes(String orgDes) {
		this.orgDes = orgDes;
	}

	public String getOrgStatus() {
		return orgStatus;
	}

	public void setOrgStatus(String orgStatus) {
		this.orgStatus = orgStatus;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getIndexId() {
		return indexId;
	}

	public void setIndexId(Long indexId) {
		this.indexId = indexId;
	}

	public String getWebLoginLogo() {
		return webLoginLogo;
	}

	public void setWebLoginLogo(String webLoginLogo) {
		this.webLoginLogo = webLoginLogo;
	}

	public String getWebLogo() {
		return webLogo;
	}

	public void setWebLogo(String webLogo) {
		this.webLogo = webLogo;
	}

	public String getWebTitle() {
		return webTitle;
	}

	public void setWebTitle(String webTitle) {
		this.webTitle = webTitle;
	}

	public List<Integer> getOrgTypeIds() {
		if(StringUtilsV2.isNotBlank(orgTypeId)) {
			orgTypeIds = new ArrayList<Integer>();
			for (String t : orgTypeId.split(",")) {
				orgTypeIds.add(CommUtil.null2Int(t));
			} 
		}
		return orgTypeIds;
	}

	public void setOrgTypeIds(List<Integer> orgTypeIds) {
		this.orgTypeIds = orgTypeIds;
		if(!CollectionUtils.isEmpty(orgTypeIds)) {
			this.orgTypeId = StringUtilsV2.join(orgTypeIds,",");
		}
	}

	public String getOrgTradeTypeId() {
		return orgTradeTypeId;
	}

	public List<Integer> getOrgTradeTypeIds() {
		if(StringUtilsV2.isNotBlank(orgTradeTypeId)) {
			orgTradeTypeIds = new ArrayList<Integer>();
			for (String t : orgTradeTypeId.split(",")) {
				orgTradeTypeIds.add(CommUtil.null2Int(t));
			} 
		}
		return orgTradeTypeIds;
	}

	public void setOrgTradeTypeIds(List<Integer> orgTradeTypeIds) {
		this.orgTradeTypeIds = orgTradeTypeIds;
		if(!CollectionUtils.isEmpty(orgTradeTypeIds)) {
			this.orgTradeTypeId = StringUtilsV2.join(orgTradeTypeIds,",");
		}
	}

	public void setOrgTradeTypeId(String orgTradeTypeId) {
		this.orgTradeTypeId = orgTradeTypeId;
	}

	public String getFollowUserId() {
		return followUserId;
	}

	public void setFollowUserId(String followUserId) {
		this.followUserId = followUserId;
	}

	public String getFollowOrgId() {
		return followOrgId;
	}

	public void setFollowOrgId(String followOrgId) {
		this.followOrgId = followOrgId;
	}
	
	
}
