package com.hcc.flow.server.vo.sys;

import com.hcc.flow.server.common.constant.Constant;
import com.hcc.flow.server.common.enums.DataStatusType;
import com.hcc.flow.server.common.utils.CommUtil;
import com.hcc.flow.server.common.utils.StringUtilsV2;
import com.hcc.flow.server.model.sys.SysUser;

/**
 * 系统用户实体VO
 * 
 * @author hanchangzhi
 *
 */
@SuppressWarnings("unused")
public class SysUserVO extends SysUser {

	private static final long serialVersionUID = -6525908145032868837L;
	private String depName;// 部门Name
	private String orgName;// 机构Name
	private String orgTypeId;// 机构类型ID
	private String orgTypeName;// 机构类型Name
	private String roleName;
	private String userNameNew;// 用户名称
	private String statusName;
	private String headImgUrlAll;// 头像图片

	public String getDepName() {
		return depName;
	}

	public void setDepName(String depName) {
		this.depName = depName;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgTypeId() {
		return orgTypeId;
	}

	public void setOrgTypeId(String orgTypeId) {
		this.orgTypeId = orgTypeId;
	}

	public String getOrgTypeName() {
		return orgTypeName;
	}

	public void setOrgTypeName(String orgTypeName) {
		this.orgTypeName = orgTypeName;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getUserNameNew() {
		return userNameNew;
	}

	public void setUserNameNew(String userNameNew) {
		this.userNameNew = userNameNew;
	}

	public String getStatusName() {
		return super.getStatus() != null ? DataStatusType.getName(super.getStatus()) : statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	
	public String getHeadImgUrlAll() {
		if(StringUtilsV2.isBlank(super.getHeadImgUrl())) {
			if(StringUtilsV2.isBlank(super.getSex()) || "1".equals(super.getSex())) {
				return Constant.PLATFORM_USER_IMG_MAN;
			}
			return Constant.PLATFORM_USER_IMG_WOMAN;
		}
		return CommUtil.imgAllUrl(super.getHeadImgUrl());
	}

	public void setHeadImgUrlAll(String headImgUrlAll) {
		this.headImgUrlAll = headImgUrlAll;
	}
}
