package com.hcc.flow.server.vo.sys;

import java.io.Serializable;
import java.util.List;

/**
 * 权限实体
 * 
 * @author hanchangzhi
 *
 */
public class PermissionChildVO implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -4038625433886629941L;
	private String permissionId;// 权限ID
	private String parentId;// 上级权限
	private String permissionName;// 权限名称
	private String css;// 样式
	private String href;// 链接地址
	private Integer type;// 类型(1目录2菜单3按钮4数据)
	private Integer sort;// 排序
	private Integer level;// 菜单所在层级

	// 不存入数据库
	private List<PermissionChildVO> child;// 对应子菜单

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public List<PermissionChildVO> getChild() {
		return child;
	}

	public void setChild(List<PermissionChildVO> child) {
		this.child = child;
	}

	public String getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(String permissionId) {
		this.permissionId = permissionId;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getCss() {
		return css;
	}

	public void setCss(String css) {
		this.css = css;
	}

}
