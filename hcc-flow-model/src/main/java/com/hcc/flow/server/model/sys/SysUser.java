package com.hcc.flow.server.model.sys;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hcc.flow.server.model.common.BaseModel;

/**
 * 系统用户实体
 * 
 * @author hanchangzhi
 *
 */
public class SysUser extends BaseModel {

	private static final long serialVersionUID = -6525908145032868837L;
	private String userId;// 用户ID
	private String depId;// 部门ID
	private String orgId;// 机构ID
	private String userName;// 用户名称
	private String userAcct;// 用户账号
	private String faxphone;// 传真号码
	private String isLogin;// 是否允许登录(Y是N否)
	private String isPhoneLogin;// 是否允许手机登录(Y是N否)
	private String allowMac;// 允许登录mac地址(逗号分隔)
	//@JsonIgnore
	private String password;// 用户密码
	private String headImgUrl;// 头像图片
	private String phone;// 手机
	private String telephone;// 固定电话
	private String email;// 邮件
	@JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
	private Date birthday;// 出生年月
	private String sex;// 性别 1男 2女
	private String status;// 状态（C正常R停用）
	private String addr;// 办公地址
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date lastLoginTime;// 上次登录时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date currentLoginTime;// 本次登录时间
	
	private String isEditPassword;// 是否需要修改密码(Y是N否)
	@JsonIgnore
	private Date lastEditPasswordTime;// 上次修改密码时间
	private String createOrgId;//创建人员所属机构
	
	public String getCreateOrgId() {
		return createOrgId;
	}

	public void setCreateOrgId(String createOrgId) {
		this.createOrgId = createOrgId;
	}

	public String getIsEditPassword() {
		return isEditPassword;
	}

	public void setIsEditPassword(String isEditPassword) {
		this.isEditPassword = isEditPassword;
	}

	public Date getLastEditPasswordTime() {
		return lastEditPasswordTime;
	}

	public void setLastEditPasswordTime(Date lastEditPasswordTime) {
		this.lastEditPasswordTime = lastEditPasswordTime;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHeadImgUrl() {
		return headImgUrl;
	}

	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDepId() {
		return depId;
	}

	public void setDepId(String depId) {
		this.depId = depId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserAcct() {
		return userAcct;
	}

	public void setUserAcct(String userAcct) {
		this.userAcct = userAcct;
	}

	public String getFaxphone() {
		return faxphone;
	}

	public void setFaxphone(String faxphone) {
		this.faxphone = faxphone;
	}

	public String getIsLogin() {
		return isLogin;
	}

	public void setIsLogin(String isLogin) {
		this.isLogin = isLogin;
	}

	public String getIsPhoneLogin() {
		return isPhoneLogin;
	}

	public void setIsPhoneLogin(String isPhoneLogin) {
		this.isPhoneLogin = isPhoneLogin;
	}

	public String getAllowMac() {
		return allowMac;
	}

	public void setAllowMac(String allowMac) {
		this.allowMac = allowMac;
	}

	// 因框架需要，实现这个用户名的获取，对应后台数据库的登录帐号。
	public String getUsername() {
		return userAcct;
	}

	public void setUsername(String userName) {
		this.userAcct = userName;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public Date getCurrentLoginTime() {
		return currentLoginTime;
	}

	public void setCurrentLoginTime(Date currentLoginTime) {
		this.currentLoginTime = currentLoginTime;
	}

}
