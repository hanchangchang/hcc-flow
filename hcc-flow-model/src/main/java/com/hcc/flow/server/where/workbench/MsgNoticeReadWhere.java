package com.hcc.flow.server.where.workbench;

import java.io.Serializable;

import com.hcc.flow.server.common.page.table.PageTableRequest;
import com.hcc.flow.server.common.utils.CommUtil;
import com.hcc.flow.server.common.where.PublicPara;

/**
 * @author 韩长志 2019-11-19
 * 
 */
public class MsgNoticeReadWhere extends PublicPara implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String keywords;// 标题或者文字
	String selectTypeId;// 消息类型
	String verifyType;// 具体任务类型
	String readUserId;
	String flowVerifyStatus;//审核状态(2审核中 3已核准 4已驳回 6已转办)
	String status;// 状态（0未读 1已读 2删除）
	String notStatus = "2";// 不用查询的状态
	String sqlOrder;
	Integer offset;
	Integer limit;
	public String getReadUserId() {
		return readUserId;
	}

	public void setReadUserId(String readUserId) {
		this.readUserId = readUserId;
	}

	public String getNotStatus() {
		return notStatus;
	}

	public void setNotStatus(String notStatus) {
		this.notStatus = notStatus;
	}

	public String getSqlOrder() {
		return sqlOrder;
	}

	public void setSqlOrder(String sqlOrder) {
		this.sqlOrder = sqlOrder;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		if("-1".equals(status)){
			status = null;
		}
		this.status = status;
	}

	public String getFlowVerifyStatus() {
		return flowVerifyStatus;
	}

	public void setFlowVerifyStatus(String flowVerifyStatus) {
		this.flowVerifyStatus = flowVerifyStatus;
	}


	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getSelectTypeId() {
		return selectTypeId;
	}

	public void setSelectTypeId(String selectTypeId) {
		this.selectTypeId = selectTypeId;
	}

	public String getVerifyType() {
		return verifyType;
	}

	public void setVerifyType(String verifyType) {
		this.verifyType = verifyType;
	}

	public MsgNoticeReadWhere(PageTableRequest strCon) {
		super();
		this.limit = strCon.getLimit();
		this.offset = strCon.getOffset();
		this.keywords = CommUtil.null2String(strCon.getParams().get("keywords"));
		this.selectTypeId = CommUtil.null2String(strCon.getParams().get("selectTypeId"));
		this.status = CommUtil.null2String(strCon.getParams().get("status"));
		this.flowVerifyStatus = CommUtil.null2String(strCon.getParams().get("flowVerifyStatus"));
		this.verifyType = CommUtil.null2String(strCon.getParams().get("verifyType"));
	}

	public MsgNoticeReadWhere() {
		super();
	}
}
