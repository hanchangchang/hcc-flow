package com.hcc.flow.server.vo.sys;

import com.hcc.flow.server.common.vo.SelectVO;

/**
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class SysListVO extends SelectVO {


	/**
	 * 
	 */
	private static final long serialVersionUID = -6064310917444647330L;
	private String url;// url

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	

}
