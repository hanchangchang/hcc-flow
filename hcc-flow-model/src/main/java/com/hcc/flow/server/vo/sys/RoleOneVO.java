package com.hcc.flow.server.vo.sys;

import java.util.List;

import com.hcc.flow.server.dto.sys.RoleDto;

/**
 * 角色实体
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class RoleOneVO extends RoleDto {

	private static final long serialVersionUID = -3802292814767103648L;
	
	private String parentName;
	// 不存入数据库
	private List<String> parentIds;
	private List<String> parentNames;
	
	private List<String> parentOrgIds;
	private List<String> parentOrgNames;
	
	/*private String createTimeCheck;// 创建时间
	private String updateTimeCheck;// 更新时间
	@JsonIgnore
	private Date now = new Date();//当前日期
	
	public String getCreateTimeCheck() {
		return CommUtil.dateCheck(super.getCreateTime(),now);
	}

	public void setCreateTimeCheck(String createTimeCheck) {
		this.createTimeCheck = createTimeCheck;
	}

	public String getUpdateTimeCheck() {
		return CommUtil.dateCheck(super.getUpdateTime(),now);
	}

	public void setUpdateTimeCheck(String updateTimeCheck) {
		this.updateTimeCheck = updateTimeCheck;
	}*/
	
	public List<String> getParentOrgIds() {
		return parentOrgIds;
	}

	public void setParentOrgIds(List<String> parentOrgIds) {
		this.parentOrgIds = parentOrgIds;
	}

	public List<String> getParentOrgNames() {
		return parentOrgNames;
	}

	public void setParentOrgNames(List<String> parentOrgNames) {
		this.parentOrgNames = parentOrgNames;
	}

	public List<String> getParentNames() {
		return parentNames;
	}

	public void setParentNames(List<String> parentNames) {
		this.parentNames = parentNames;
	}

	public List<String> getParentIds() {
		return parentIds;
	}

	public void setParentIds(List<String> parentIds) {
		this.parentIds = parentIds;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
}
