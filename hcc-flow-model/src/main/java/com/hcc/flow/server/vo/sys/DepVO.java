package com.hcc.flow.server.vo.sys;

import com.hcc.flow.server.common.enums.DataStatusType;
import com.hcc.flow.server.model.sys.Dep;

/**
 * 部门实体
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class DepVO extends Dep {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2332081177517535832L;

	private String createUserName;// 创建人Name

	private String statusName;// 状态
	/*@SuppressWarnings("unused")
	private String createTimeCheck;// 创建时间
	@JsonIgnore
	private Date now = new Date();//当前日期
	
	public String getCreateTimeCheck() {
		return CommUtil.dateCheck(super.getCreateTime(),now);
	}

	public void setCreateTimeCheck(String createTimeCheck) {
		this.createTimeCheck = createTimeCheck;
	}*/
	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getStatusName() {
		return super.getDepStatus() != null ? DataStatusType.getName(super.getDepStatus()) : statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
}
