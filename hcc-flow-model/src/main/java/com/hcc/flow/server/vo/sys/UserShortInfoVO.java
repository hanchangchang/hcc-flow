package com.hcc.flow.server.vo.sys;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hcc.flow.server.common.constant.Constant;
import com.hcc.flow.server.common.utils.CommUtil;
import com.hcc.flow.server.common.utils.StringUtilsV2;

/**
 * @author 韩长长 
 * @version createTime：2018年9月27日 上午10:02:09 简单用户消息
 */
public class UserShortInfoVO {

	@JsonIgnore
	Date lastLoginTime;
	String userAcct;
	String sex;
	String userName;
	@JsonIgnore
	String userHeadImgUrl;// 头像图片
	String avatar;// 头像图片
	String orgId;
	String orgName;
	String orgIndexId;
	String webLoginLogoAll;
	String webLogoAll;
	String webTitle;
	String roleName;
	String lastLoginDate;
	String isEditPassword;// 是否需要修改密码(Y是N否)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	Date lastEditPasswordTime;// 上次修改密码时间

	public String getOrgIndexId() {
		return orgIndexId;
	}

	public void setOrgIndexId(String orgIndexId) {
		this.orgIndexId = orgIndexId;
	}

	public String getWebLoginLogoAll() {
		return webLoginLogoAll;
	}

	public void setWebLoginLogoAll(String webLoginLogoAll) {
		this.webLoginLogoAll = webLoginLogoAll;
	}

	public String getWebLogoAll() {
		return webLogoAll;
	}

	public void setWebLogoAll(String webLogoAll) {
		this.webLogoAll = webLogoAll;
	}

	public String getWebTitle() {
		return webTitle;
	}

	public void setWebTitle(String webTitle) {
		this.webTitle = webTitle;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getIsEditPassword() {
		if("N".equals(isEditPassword)){
			//上次修改密码时间为空或者超过90天
			if(lastEditPasswordTime != null && CommUtil.DateUntil(new Date(), lastEditPasswordTime, null) >= 90){
				return "Y";
			}
		}
		return isEditPassword;
	}

	public void setIsEditPassword(String isEditPassword) {
		this.isEditPassword = isEditPassword;
	}

	public Date getLastEditPasswordTime() {
		return lastEditPasswordTime;
	}

	public void setLastEditPasswordTime(Date lastEditPasswordTime) {
		this.lastEditPasswordTime = lastEditPasswordTime;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getUserAcct() {
		return userAcct;
	}

	public void setUserAcct(String userAcct) {
		this.userAcct = userAcct;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getLastLoginDate() {
		if(lastLoginTime != null){
			return CommUtil.formatLongDate(lastLoginTime);
		}else{
			return "您是首次登陆";
		}
	}

	public void setLastLoginDate(String lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getUserHeadImgUrl() {
		return userHeadImgUrl;
	}

	public void setUserHeadImgUrl(String userHeadImgUrl) {
		this.userHeadImgUrl = userHeadImgUrl;
	}

	public String getAvatar() {
		if(StringUtilsV2.isBlank(userHeadImgUrl)) {
			if(StringUtilsV2.isBlank(sex) || "1".equals(sex)) {
				return Constant.PLATFORM_USER_IMG_MAN;
			}
			return Constant.PLATFORM_USER_IMG_WOMAN;
		}
		return CommUtil.imgAllUrl(userHeadImgUrl);
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
}
