package com.hcc.flow.server.dto.sys;

import java.io.Serializable;

import com.hcc.flow.server.common.utils.StringUtilsV2;

public class VerifyDto implements Serializable {

	private static final long serialVersionUID = 35735494737590569L;

	private String id;
	private String rejectReason;//审核宋
	private String readId;//消息阅读id
	private String verifyType;//审核码
	private String doofUserId;//转办的用户
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRejectReason() {
		if(StringUtilsV2.isBlank(rejectReason)){
			rejectReason = "无";
		}
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public VerifyDto() {
		super();
	}

	public VerifyDto(String id, String rejectReason) {
		super();
		this.id = id;
		this.rejectReason = rejectReason;
	}

	public String getReadId() {
		return readId;
	}

	public void setReadId(String readId) {
		this.readId = readId;
	}

	public String getVerifyType() {
		return verifyType;
	}

	public void setVerifyType(String verifyType) {
		this.verifyType = verifyType;
	}

	public String getDoofUserId() {
		return doofUserId;
	}

	public void setDoofUserId(String doofUserId) {
		this.doofUserId = doofUserId;
	}

}
