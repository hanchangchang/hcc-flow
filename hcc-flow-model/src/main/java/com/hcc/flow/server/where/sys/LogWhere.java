package com.hcc.flow.server.where.sys;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hcc.flow.server.common.page.table.PageTableRequest;
import com.hcc.flow.server.common.utils.CommUtil;
import com.hcc.flow.server.common.where.PublicWhere;

/**
 * @author 韩长长 
 * @version createTime：2018年9月28日 上午11:18:59
 * 
 */
public class LogWhere extends PublicWhere implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String userName;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	Date beginTime;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	Date endTime;

	Integer flag;
	
	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getBeginTime() {
		return beginTime != null?CommUtil.getStartOfDay(beginTime):beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = CommUtil.getStartOfDay(beginTime);
	}

	public Date getEndTime() {
		return endTime!=null?CommUtil.getEndOfDay(endTime):endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = CommUtil.getEndOfDay(endTime);
	}

	@Override
	public String toString() {
		return "LogWhere [userName=" + userName + ", beginTime=" + beginTime + ", endTime=" + endTime + ", sqlOrder="
				+ super.getSqlOrder() + ", offset=" + super.getOffset() + ", limit=" + super.getLimit() + ", flag=" + flag + "]";
	}
	public LogWhere() {
		super();
	}
	public LogWhere(PageTableRequest strCon) {
		super();
		super.setLimit(strCon.getLimit());
		super.setOffset(strCon.getOffset());
		this.userName = CommUtil.null2String(strCon.getParams().get("userName"));
	}
}
