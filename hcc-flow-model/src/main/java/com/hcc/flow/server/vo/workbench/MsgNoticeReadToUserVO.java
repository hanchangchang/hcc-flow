package com.hcc.flow.server.vo.workbench;


import com.hcc.flow.server.model.workbench.MsgNoticeRead;

public class MsgNoticeReadToUserVO extends MsgNoticeRead {

	private static final long serialVersionUID = -4401913568806243091L;

	private String readUserName;//阅读人
	
	public String getReadUserName() {
		return readUserName;
	}
}
