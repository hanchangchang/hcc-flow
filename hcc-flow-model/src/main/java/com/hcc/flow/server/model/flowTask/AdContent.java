package com.hcc.flow.server.model.flowTask;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 素材表
 * 
 * @author 韩长志
 *
 */
public class AdContent implements Serializable {

	private static final long serialVersionUID = 2494033534892594657L;
	private String adId;// 素材ID
	private Long indexId;//唯一标识id(只作展示用，不用作业务处理)
	private Integer adTypeId;// 广告类型ID  1图片 2视频 3...
	private String adName;// 素材名称
	private Integer adSize;// 素材大小
	private Integer adSecond;// 时长(S)
	private String fileType;// 格式
	private String resolution;// 分辨率
	private String status;// 状态(0待提交1审核中2已核准3已拒绝4已删除)
	
	private String md5;// MD5码
	private String adUrl;// 素材地址
	private String adContent;// 文件内容
	
	private String createUserId;// 创建人员ID
	private String createOrgId;//创建人员所属机构
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date createTime;// 创建时间
	
	private String submitterId;// 提交人ID
	private String verifyUserId;//终审人员
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date verifyTime;//终审时间
	public String getAdId() {
		return adId;
	}
	public void setAdId(String adId) {
		this.adId = adId;
	}
	public Long getIndexId() {
		return indexId;
	}
	public void setIndexId(Long indexId) {
		this.indexId = indexId;
	}
	public Integer getAdTypeId() {
		return adTypeId;
	}
	public void setAdTypeId(Integer adTypeId) {
		this.adTypeId = adTypeId;
	}
	public String getAdName() {
		return adName;
	}
	public void setAdName(String adName) {
		this.adName = adName;
	}
	public Integer getAdSize() {
		return adSize;
	}
	public void setAdSize(Integer adSize) {
		this.adSize = adSize;
	}
	public Integer getAdSecond() {
		return adSecond;
	}
	public void setAdSecond(Integer adSecond) {
		this.adSecond = adSecond;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String getResolution() {
		return resolution;
	}
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMd5() {
		return md5;
	}
	public void setMd5(String md5) {
		this.md5 = md5;
	}
	public String getAdUrl() {
		return adUrl;
	}
	public void setAdUrl(String adUrl) {
		this.adUrl = adUrl;
	}
	public String getAdContent() {
		return adContent;
	}
	public void setAdContent(String adContent) {
		this.adContent = adContent;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getCreateOrgId() {
		return createOrgId;
	}
	public void setCreateOrgId(String createOrgId) {
		this.createOrgId = createOrgId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getSubmitterId() {
		return submitterId;
	}
	public void setSubmitterId(String submitterId) {
		this.submitterId = submitterId;
	}
	public String getVerifyUserId() {
		return verifyUserId;
	}
	public void setVerifyUserId(String verifyUserId) {
		this.verifyUserId = verifyUserId;
	}
	public Date getVerifyTime() {
		return verifyTime;
	}
	public void setVerifyTime(Date verifyTime) {
		this.verifyTime = verifyTime;
	}
	
	public AdContent() {
		super();
	}
	
	public AdContent(Integer adTypeId, String adName, Integer adSize, Integer adSecond,
			String fileType, String resolution, String status, String md5, String adUrl, String adContent) {
		super();
		this.adTypeId = adTypeId;
		this.adName = adName;
		this.adSize = adSize;
		this.adSecond = adSecond;
		this.fileType = fileType;
		this.resolution = resolution;
		this.status = status;
		this.md5 = md5;
		this.adUrl = adUrl;
		this.adContent = adContent;
	}

}
