package com.hcc.flow.server.model.common;

import org.springframework.security.core.AuthenticationException;

/**
 * @author 韩长长
 * @version createTime：2020年6月9日 下午1:29:44 自定义 验证码异常类
 */
public class ValidateCodeException extends AuthenticationException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ValidateCodeException(String msg) {
		super(msg);
	}
}