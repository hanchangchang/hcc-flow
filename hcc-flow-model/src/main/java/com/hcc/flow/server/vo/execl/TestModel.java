package com.hcc.flow.server.vo.execl;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;

public class TestModel extends BaseRowModel {

	@ExcelProperty(value = "TEST",index = 0)
	private String createUserId;
	
	
	
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	
}
