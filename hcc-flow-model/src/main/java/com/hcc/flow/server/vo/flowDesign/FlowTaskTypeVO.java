package com.hcc.flow.server.vo.flowDesign;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hcc.flow.server.common.enums.DataStatusType;

/**
 * 流程业务类型VO
 * @author 韩长志
 *
 */
public class FlowTaskTypeVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9113202610952062463L;
	private Integer taskTypeId;//类型id
	private String taskTypeName;//类型名
	private String href;//详情页地址
	private String model;//状态
	private String status;//状态
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date createTime;//创建时间

	@SuppressWarnings("unused")
	private String statusName;//状态
	public Integer getTaskTypeId() {
		return taskTypeId;
	}
	public void setTaskTypeId(Integer taskTypeId) {
		this.taskTypeId = taskTypeId;
	}
	public String getTaskTypeName() {
		return taskTypeName;
	}
	public void setTaskTypeName(String taskTypeName) {
		this.taskTypeName = taskTypeName;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getStatusName() {
		return DataStatusType.getName(status);
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}

}
