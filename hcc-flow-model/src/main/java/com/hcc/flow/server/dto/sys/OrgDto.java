package com.hcc.flow.server.dto.sys;


import java.util.List;

import com.hcc.flow.server.model.sys.Org;

/**
 * 组织机构
 * @author 韩长志 2019-11-19
 *
 */
public class OrgDto extends Org {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4703086701269765385L;
	
	private List<Long> provinceAreaIds;// 选中省份区域IDs
	private List<Long> cityAreaIds;// 选中城市区域IDs
	private List<Long> districtAreaIds;// 选中区县区域IDs
	private List<Long> positionTypeIds;// 选中点位分类ids
	private List<String> pointGrades;// 选中点位评级集合
	private List<String> deletePointIds;// 排除点位ids
	
	public List<String> getPointGrades() {
		return pointGrades;
	}
	public void setPointGrades(List<String> pointGrades) {
		this.pointGrades = pointGrades;
	}
	public List<Long> getProvinceAreaIds() {
		return provinceAreaIds;
	}
	public void setProvinceAreaIds(List<Long> provinceAreaIds) {
		this.provinceAreaIds = provinceAreaIds;
	}
	public List<Long> getCityAreaIds() {
		return cityAreaIds;
	}
	public void setCityAreaIds(List<Long> cityAreaIds) {
		this.cityAreaIds = cityAreaIds;
	}
	public List<Long> getDistrictAreaIds() {
		return districtAreaIds;
	}
	public void setDistrictAreaIds(List<Long> districtAreaIds) {
		this.districtAreaIds = districtAreaIds;
	}
	public List<Long> getPositionTypeIds() {
		return positionTypeIds;
	}
	public void setPositionTypeIds(List<Long> positionTypeIds) {
		this.positionTypeIds = positionTypeIds;
	}
	public List<String> getDeletePointIds() {
		return deletePointIds;
	}
	public void setDeletePointIds(List<String> deletePointIds) {
		this.deletePointIds = deletePointIds;
	}
}
