package com.hcc.flow.server.model.flowTask;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Customer
 * 
 * @author 韩长志
 *
 */
public class Customer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6277945133534299119L;

	private Long indexId;// 唯一标识id(只作展示用，不用作业务处理)
	private String customerId;// 客户ID
	private String companyName;// 公司名称
	private String companyAddr;// 公司地址
	private String legalRepresentative;// 法定代表人
	private String registeredCapital;// 注册资本
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	private String businessTermBegin;// 营业期限开始
	private String businessTermEnd;// 营业期限结束
	private String creditCode;// 统一社会信用代码
	private String businessUser;// 商务联系人
	private String businessUserTel;// 商务联系人电话
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;// 创建时间
	private String createUserId;// 创建用户ID
	private Integer status;// 状态(1待提交2审核中3已核准通过4已驳回5已删除)
	private String submitterId;// 提交人ID
	private String verifyUserId;// 审核人ID
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date verifyTime;// 审核时间
	private String registerPlace;// 注册地
	private String industryParaId;// 所属行业id
	private String followUserId;// 归属业务员id
	private String createOrgId;// 创建人员所属机构
	private String followOrgId;// 归属业务员机构

	// private String customerTypeId;// 客户类型id
	// private String licenseNumber;// 营业执照编号
	// private String registrationOrg;// 登记机关
	// private String legalUser;// 法定授权代表人
	// private String legalUserDuty;// 法定授权代表职务
	// private String legalUserTel;// 法定授权代表人电话
	// private String legalUserPhone;// 法定授权代表人手机号
	// private String legalUserMail;// 法定授权代表人邮箱
	// private String legalUserTax;// 法定授权代表人传真
	// private String businessUserDuty;// 商务联系人职务
	// private String businessUserPhone;// 商务联系人手机号
	// private String businessUserMail;// 商务联系人邮箱
	// private String businessUserTax;// 商务联系人传真
	// private String rejectReason;// 驳回信息
	// @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	// private Date submitTime;// 提交时间
	// @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	// private Date inTime;// 审核通过时间
	// private String taxRegistrationUrl;// 税务登记证图片地址
	// private String licenseUrl;// 营业执照图片地址
	// private String legalIdcardFaceUrl;// 法人身份证正面地址
	// private String legalIdcardBackUrl;// 法人身份证背面地址
	// private String addOrg;// 创建机构
	// private String orgId;// 客户对应机构
	// private List<Integer> customerTypeIds;// 客户类型id

	public String getIndustryParaId() {
		return industryParaId;
	}

	public void setIndustryParaId(String industryParaId) {
		this.industryParaId = industryParaId;
	}

	public Long getIndexId() {
		return indexId;
	}

	public void setIndexId(Long indexId) {
		this.indexId = indexId;
	}

	public String getRegisterPlace() {
		return registerPlace;
	}

	public void setRegisterPlace(String registerPlace) {
		this.registerPlace = registerPlace;
	}

	public String getCreateOrgId() {
		return createOrgId;
	}

	public void setCreateOrgId(String createOrgId) {
		this.createOrgId = createOrgId;
	}

	public String getFollowOrgId() {
		return followOrgId;
	}

	public void setFollowOrgId(String followOrgId) {
		this.followOrgId = followOrgId;
	}

	public String getFollowUserId() {
		return followUserId;
	}

	public void setFollowUserId(String followUserId) {
		this.followUserId = followUserId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyAddr() {
		return companyAddr;
	}

	public void setCompanyAddr(String companyAddr) {
		this.companyAddr = companyAddr;
	}

	public String getLegalRepresentative() {
		return legalRepresentative;
	}

	public void setLegalRepresentative(String legalRepresentative) {
		this.legalRepresentative = legalRepresentative;
	}

	public String getRegisteredCapital() {
		return registeredCapital;
	}

	public void setRegisteredCapital(String registeredCapital) {
		this.registeredCapital = registeredCapital;
	}

	public String getBusinessTermBegin() {
		return businessTermBegin;
	}

	public void setBusinessTermBegin(String businessTermBegin) {
		this.businessTermBegin = businessTermBegin;
	}

	public String getBusinessTermEnd() {
		return businessTermEnd;
	}

	public void setBusinessTermEnd(String businessTermEnd) {
		this.businessTermEnd = businessTermEnd;
	}

	public String getCreditCode() {
		return creditCode;
	}

	public void setCreditCode(String creditCode) {
		this.creditCode = creditCode;
	}

	public String getBusinessUser() {
		return businessUser;
	}

	public void setBusinessUser(String businessUser) {
		this.businessUser = businessUser;
	}

	public String getBusinessUserTel() {
		return businessUserTel;
	}

	public void setBusinessUserTel(String businessUserTel) {
		this.businessUserTel = businessUserTel;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getVerifyUserId() {
		return verifyUserId;
	}

	public void setVerifyUserId(String verifyUserId) {
		this.verifyUserId = verifyUserId;
	}

	public String getSubmitterId() {
		return submitterId;
	}

	public void setSubmitterId(String submitterId) {
		this.submitterId = submitterId;
	}

	public Date getVerifyTime() {
		return verifyTime;
	}

	public void setVerifyTime(Date verifyTime) {
		this.verifyTime = verifyTime;
	}

}
