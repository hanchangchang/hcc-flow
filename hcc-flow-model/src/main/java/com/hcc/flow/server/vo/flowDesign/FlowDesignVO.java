package com.hcc.flow.server.vo.flowDesign;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hcc.flow.server.common.enums.DataStatusType;

/**
 * 工作流 流程
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class FlowDesignVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6096761967550851049L;
	private String flowId;// 流程id
	private String flowCode;// 流程编码
	private String flowName;// 流程名称
	private String flowType;// 流程归属类型(1素材2订单3刊播4换刊5客户6品牌)
	private String flowTypeName;// 流程归属类型Name
	private String flowAttribute;//流程属性 0普通流程 1复合流程
	@SuppressWarnings("unused")
	private String flowAttributeName;//流程属性 0普通流程 1复合流程
	private String followOrgId;// 归属机构Id
	private String followOrgName;// 归属机构Name
	private String flowRemarks;// 流程说明
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;// 创建时间
	private String createUserName;// 创建人员id
	private String status;// 状态CRD
	@JsonIgnore
	private String isUsed;// 是否使用0未使用 1已使用
	@SuppressWarnings("unused")
	private String statusName;// 状态CRD
	@SuppressWarnings("unused")
	private String isUsedName;// 是否使用0未使用 1已使用

	public String getFollowOrgId() {
		return followOrgId;
	}

	public void setFollowOrgId(String followOrgId) {
		this.followOrgId = followOrgId;
	}

	public String getFlowId() {
		return flowId;
	}

	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public String getFlowType() {
		return flowType;
	}

	public void setFlowType(String flowType) {
		this.flowType = flowType;
	}

	public String getFollowOrgName() {
		return followOrgName;
	}

	public void setFollowOrgName(String followOrgName) {
		this.followOrgName = followOrgName;
	}

	public String getFlowCode() {
		return flowCode;
	}

	public void setFlowCode(String flowCode) {
		this.flowCode = flowCode;
	}

	public String getFlowRemarks() {
		return flowRemarks;
	}

	public void setFlowRemarks(String flowRemarks) {
		this.flowRemarks = flowRemarks;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIsUsed() {
		return isUsed;
	}

	public void setIsUsed(String isUsed) {
		this.isUsed = isUsed;
	}

	public String getStatusName() {
		return DataStatusType.getName(status);
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getIsUsedName() {
		return "0".equals(isUsed)?"未使用":"已使用";
	}

	public void setIsUsedName(String isUsedName) {
		this.isUsedName = isUsedName;
	}

	public String getFlowTypeName() {
		return flowTypeName+"审核";
	}

	public void setFlowTypeName(String flowTypeName) {
		this.flowTypeName = flowTypeName;
	}

	public String getFlowAttribute() {
		return flowAttribute;
	}

	public void setFlowAttribute(String flowAttribute) {
		this.flowAttribute = flowAttribute;
	}

	public String getFlowAttributeName() {
		return "0".equals(flowAttribute)?"普通流程":"复合流程";
	}

	public void setFlowAttributeName(String flowAttributeName) {
		this.flowAttributeName = flowAttributeName;
	}

}
