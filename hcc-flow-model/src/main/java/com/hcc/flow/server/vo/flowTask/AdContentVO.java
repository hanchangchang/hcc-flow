package com.hcc.flow.server.vo.flowTask;

import com.hcc.flow.server.common.enums.CheckStatusType;
import com.hcc.flow.server.model.flowTask.AdContent;

/**
 * 素材表
 * 
 * @author 韩长志
 *
 */
public class AdContentVO extends AdContent {

	private static final long serialVersionUID = 2494733534892594657L;
	private String adTypeName;// 广告类型Name
	private String statusName;// 状态(0待提交1审核中2已核准3已拒绝4已删除)
	private String createUserName;// 创建人员Name
	private String verifyUserName;// 审核人Name
	private String submitterName;//提交人name

	public String getSubmitterName() {
		return submitterName;
	}

	public void setSubmitterName(String submitterName) {
		this.submitterName = submitterName;
	}

	public String getAdTypeName() {
		return adTypeName;
	}

	public void setAdTypeName(String adTypeName) {
		this.adTypeName = adTypeName;
	}

	public String getStatusName() {
		return super.getStatus() != null ? CheckStatusType.getName(super.getStatus()) : statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getVerifyUserName() {
		return verifyUserName;
	}

	public void setVerifyUserName(String verifyUserName) {
		this.verifyUserName = verifyUserName;
	}
}
