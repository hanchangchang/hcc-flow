package com.hcc.flow.server.vo.sys;

import com.hcc.flow.server.model.sys.Dep;

/**
 * 部门实体
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class DepShordVO extends Dep {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2332081177517535832L;

	private String depId;// 部门ID
	private String orgId;// 机构ID
	private String depName;// 部门名称
	private String parentId;// 上级部门ID
	private Integer level;// 层级
	/*@SuppressWarnings("unused")
	private String createTimeCheck;// 创建时间
	@JsonIgnore
	private Date now = new Date();//当前日期
	
	public String getCreateTimeCheck() {
		return CommUtil.dateCheck(super.getCreateTime(),now);
	}

	public void setCreateTimeCheck(String createTimeCheck) {
		this.createTimeCheck = createTimeCheck;
	}*/

	public String getDepId() {
		return depId;
	}

	public void setDepId(String depId) {
		this.depId = depId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getDepName() {
		return depName;
	}

	public void setDepName(String depName) {
		this.depName = depName;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}
}
