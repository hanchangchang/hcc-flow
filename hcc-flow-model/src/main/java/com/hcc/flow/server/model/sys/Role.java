package com.hcc.flow.server.model.sys;

import com.hcc.flow.server.model.common.BaseModel;

/**
 * 角色实体
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class Role extends BaseModel {

	private static final long serialVersionUID = -3802292814767103648L;

	private String roleId;// 角色ID
	private String orgId;// 机构ID
	private String roleName;// 角色名称
	private String description;// 角色描述
	private String status;// 状态(1未使用2已使用3已删除)
	private String parentId;// 上级角色
	private Integer level;// 角色所在层级
	
	private String orgName;// 对应机构名称
	
	private String createOrgId;//创建人员机构id
	
	public String getCreateOrgId() {
		return createOrgId;
	}

	public void setCreateOrgId(String createOrgId) {
		this.createOrgId = createOrgId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

}
