package com.hcc.flow.server.vo.flowTask;

import com.hcc.flow.server.common.enums.CheckStatusType;
import com.hcc.flow.server.model.flowTask.Customer;


/**
 * 区域
 * 
 * @author 韩长志
 *
 */
public class CustomerVO extends Customer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4523193760099299996L;

	private String industryParaName;//行业名
	private String statusName;
	private String createUserName;//创建人名
	private String submitUserName;//提交人名
	private String verifyUserName;//审核人名
	private String followUserName;//审核人名
	
	@SuppressWarnings("unused")
	private String addOrgRemark;//是否创建机构
	
	public String getIndustryParaName() {
		return industryParaName;
	}

	public void setIndustryParaName(String industryParaName) {
		this.industryParaName = industryParaName;
	}

	public String getFollowUserName() {
		return followUserName;
	}

	public void setFollowUserName(String followUserName) {
		this.followUserName = followUserName;
	}


	public String getStatusName() {
		return super.getStatus() != null ? CheckStatusType.getName(super.getStatus().toString()) : statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getSubmitUserName() {
		return submitUserName;
	}

	public void setSubmitUserName(String submitUserName) {
		this.submitUserName = submitUserName;
	}

	public String getVerifyUserName() {
		return verifyUserName;
	}

	public void setVerifyUserName(String verifyUserName) {
		this.verifyUserName = verifyUserName;
	}

}
