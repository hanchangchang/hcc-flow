package com.hcc.flow.server.model.sys;

/*import java.util.Date;*/
import java.util.List;

import com.hcc.flow.server.model.common.BaseModel;

/**
 * 权限实体
 * 
 * @author hanchangzhi
 *
 */
public class Permission extends BaseModel {

	private static final long serialVersionUID = 6180869216498363919L;

	private String permissionId;// 权限ID
	private String parentId;// 上级权限
	private String permissionName;// 权限名称
	private String css;// 样式
	private String href;// 链接地址
	private Integer type;// 类型(1目录2菜单3按钮4数据)
	private String permission;// 权限编码
	private Integer sort;// 排序
	private Integer level;// 菜单所在层级
	private Integer sysId;//子系统id

	// 不存入数据库
	private List<Permission> child;// 对应子菜单
	private List<String> parentIds;

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public List<Permission> getChild() {
		return child;
	}

	public void setChild(List<Permission> child) {
		this.child = child;
	}

	public String getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(String permissionId) {
		this.permissionId = permissionId;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getCss() {
		return css;
	}

	public void setCss(String css) {
		this.css = css;
	}

	public List<String> getParentIds() {
		return parentIds;
	}

	public void setParentIds(List<String> parentIds) {
		this.parentIds = parentIds;
	}

	public Integer getSysId() {
		return sysId;
	}

	public void setSysId(Integer sysId) {
		this.sysId = sysId;
	}

}
