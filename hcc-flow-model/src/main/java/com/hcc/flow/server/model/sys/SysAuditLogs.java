package com.hcc.flow.server.model.sys;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 
 * @author ChengYi
 *
 * @version 创建时间：2019年2月20日下午4:06:55 类说明：审核操作日志model
 */
public class SysAuditLogs implements Serializable {

	private static final long serialVersionUID = 1L;
	//操作模块id
	private String moduleId;
	// 用户id
	private String userId;
	// 操作说明
	private String opCon;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date opTime;

	private String userAcct;

	private String opUserName;

	private String orgName;

	private String depName;

	public SysAuditLogs( String userId, Date opTime, String opCon,String moduleId) {
		super();

		this.userId = userId;
		this.opCon = opCon;
		this.opTime = opTime;
		this.moduleId=moduleId;
	}

	public SysAuditLogs() {
		super();
	}

	public String getUserAcct() {
		return userAcct;
	}

	public void setUserAcct(String userAcct) {
		this.userAcct = userAcct;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getDepName() {
		return depName;
	}

	public void setDepName(String depName) {
		this.depName = depName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOpCon() {
		return opCon;
	}

	public void setOpCon(String opCon) {
		this.opCon = opCon;
	}

	public Date getOpTime() {
		return opTime;
	}

	public void setOpTime(Date opTime) {
		this.opTime = opTime;
	}

	public String getOpUserName() {
		return opUserName;
	}

	public void setOpUserName(String opUserName) {
		this.opUserName = opUserName;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

}
