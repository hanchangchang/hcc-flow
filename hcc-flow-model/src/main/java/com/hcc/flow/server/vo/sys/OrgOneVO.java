package com.hcc.flow.server.vo.sys;

import java.util.List;

import com.hcc.flow.server.common.enums.IfStatusType;
import com.hcc.flow.server.common.utils.CommUtil;
import com.hcc.flow.server.common.utils.StringUtilsV2;
import com.hcc.flow.server.model.sys.Org;

/**
 * 机构实体
 * 
 * @author 韩长志 2019-11-19
 *
 */
@SuppressWarnings("unused")
public class OrgOneVO extends Org {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2372081177517585832L;

	private String parentName;
	// 不存入数据库
	private List<String> parentIds;
	private List<String> parentNames;
	private String resourceName;// 资源是否分配
	private String createUserName;
	
	private String webLoginLogoAll;//网站登陆页log
	private String webLogoAll;//网站log
	/*@SuppressWarnings("unused")
	private String createTimeCheck;// 创建时间
	@JsonIgnore
	private Date now = new Date();//当前日期
	
	public String getCreateTimeCheck() {
		return CommUtil.dateCheck(super.getCreateTime(),now);
	}

	public void setCreateTimeCheck(String createTimeCheck) {
		this.createTimeCheck = createTimeCheck;
	}*/
	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getResourceName() {
		return StringUtilsV2.isNotBlank(super.getResource())?IfStatusType.getName2(super.getResource())+"分配":"未分配";
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}
	public List<String> getParentNames() {
		return parentNames;
	}

	public void setParentNames(List<String> parentNames) {
		this.parentNames = parentNames;
	}

	public List<String> getParentIds() {
		return parentIds;
	}

	public void setParentIds(List<String> parentIds) {
		this.parentIds = parentIds;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getWebLoginLogoAll() {
		//return StringUtilsV2.isNotBlank(super.getWebLoginLogo()) ? ParamConfig.file_path + super.getWebLoginLogo() : webLoginLogoAll;
		return StringUtilsV2.isNotBlank(super.getWebLoginLogo())?CommUtil.imgAllUrl(super.getWebLoginLogo()):webLoginLogoAll;
	}

	public void setWebLoginLogoAll(String webLoginLogoAll) {
		this.webLoginLogoAll = webLoginLogoAll;
	}

	public String getWebLogoAll() {
		//return StringUtilsV2.isNotBlank(super.getWebLogo()) ? ParamConfig.file_path + super.getWebLogo() : webLogoAll;
		return StringUtilsV2.isNotBlank(super.getWebLogo())?CommUtil.imgAllUrl(super.getWebLogo()):webLogoAll;
	}

	public void setWebLogoAll(String webLogoAll) {
		this.webLogoAll = webLogoAll;
	}
	
}
