package com.hcc.flow.server.model.sys;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 监听实体（主子平台）
 */
public class SqlMonitor implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 180807445209959356L;

	private String smId;//监听id
	private String method;//具体方法
	private String sqlStr;//SQL语句
	private String timems;//耗时
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date createTime;// 操作时间
	private String createUserId;// 操作人ID
	private int handle = 0;//是否已处理（上传云平台 0未处理 1处理成功 2处理失败）
	private String handleExplain;// 处理说明，处理成功记录处理时间，处理失败.记录处理时间+异常信息
	
	public String getSmId() {
		return smId;
	}
	public void setSmId(String smId) {
		this.smId = smId;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getSqlStr() {
		return sqlStr;
	}
	public void setSqlStr(String sqlStr) {
		this.sqlStr = sqlStr;
	}
	public String getTimems() {
		return timems;
	}
	public void setTimems(String timems) {
		this.timems = timems;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public int getHandle() {
		return handle;
	}
	public void setHandle(int handle) {
		this.handle = handle;
	}
	public String getHandleExplain() {
		return handleExplain;
	}
	public void setHandleExplain(String handleExplain) {
		this.handleExplain = handleExplain;
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
