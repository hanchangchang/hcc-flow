package com.hcc.flow.server.where.sys;

import java.io.Serializable;
import java.util.List;

import com.hcc.flow.server.common.enums.DataStatusType;
import com.hcc.flow.server.common.page.table.PageTableRequest;
import com.hcc.flow.server.common.utils.CommUtil;
import com.hcc.flow.server.common.where.PublicPara;

/**
 * @author 韩长长 
 * @version createTime：2018年9月28日 上午11:18:59
 * 
 */
public class OrgWhere extends PublicPara implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String orgName;// 机构名称

	String type;// 0 归属机构设备 1 归属机构点位 null 所有机构

	List<String> orgTypeIds;// 机构类型ID

	List<String> statuss;// 使用状态

	String sqlOrder;
	Integer offset;
	Integer limit;

	String notStatus = DataStatusType.DELETE.getCode();

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNotStatus() {
		return notStatus;
	}

	public void setNotStatus(String notStatus) {
		this.notStatus = notStatus;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getSqlOrder() {
		return sqlOrder;
	}

	public void setSqlOrder(String sqlOrder) {
		this.sqlOrder = sqlOrder;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public List<String> getOrgTypeIds() {
		return orgTypeIds;
	}

	public void setOrgTypeIds(List<String> orgTypeIds) {
		this.orgTypeIds = orgTypeIds;
	}

	public List<String> getStatuss() {
		return statuss;
	}

	public void setStatuss(List<String> statuss) {
		this.statuss = statuss;
	}

	public OrgWhere() {
		super();
	}

	public OrgWhere(PageTableRequest strCon) {
		super();
		this.limit = strCon.getLimit();
		this.offset = strCon.getOffset();
		this.orgName = CommUtil.null2String(strCon.getParams().get("orgName"));
	}
}
