package com.hcc.flow.server.vo.workbench;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hcc.flow.server.common.enums.CheckStatusType;
import com.hcc.flow.server.common.enums.MsgType;
import com.hcc.flow.server.common.utils.StringUtilsV2;

@SuppressWarnings("unused")
public class MsgNoticeReadVO implements Serializable {

	private static final long serialVersionUID = -4401913568806243090L;

	private String readId;
	private String title;// title
	private String pushUserName;// 发送人
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date pushTime;// 发送时间
	@JsonIgnore
	private String status;// 状态（0未读 1已读 2删除）
	
	private String statusName;// 状态Name（未读 已读 删除）
	private String flowName;//流程名称
	private String flowVerifyStatus;// 审核状态(2审核中 3已核准 4已驳回 6已转办)
	private String flowVerifyStatusName;// 审核状态Name(2审核中 3已核准 4已驳回 6已转办)
	private String flowVerifyReason;//审核意见
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date readTime;//阅读时间
	private String readUserName;//阅读人
	private String content;//审核内容
	@JsonIgnore
	private String flowTempNodeId;// 节点id
	
	private String flowFromId;// 表单id
	
	private String type;// 消息类型（1任务消息2系统消息3站内信4结果消息）
	
	private String typeName;// 消息类型（1任务消息2系统消息3站内信4结果消息）
	
	private String taskType;// 具体任务类型（1素材2订单3刊播4换刊5客户6品牌7其他）
	
	private String taskTypeName;// 具体任务类型（1素材2订单3刊播4换刊5客户6品牌7其他）
	
	private String href;// 详情页地址
	
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getFlowTempNodeId() {
		return flowTempNodeId;
	}
	public void setFlowTempNodeId(String flowTempNodeId) {
		this.flowTempNodeId = flowTempNodeId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getReadId() {
		return readId;
	}
	public void setReadId(String readId) {
		this.readId = readId;
	}
	public String getPushUserName() {
		return pushUserName;
	}
	public void setPushUserName(String pushUserName) {
		this.pushUserName = pushUserName;
	}
	public Date getPushTime() {
		return pushTime;
	}
	public void setPushTime(Date pushTime) {
		this.pushTime = pushTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusName() {
		return "0".equals(status)?"未读":"已读";
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public Date getReadTime() {
		return readTime;
	}
	public void setReadTime(Date readTime) {
		this.readTime = readTime;
	}
	public String getFlowVerifyStatus() {
		return flowVerifyStatus;
	}
	public void setFlowVerifyStatus(String flowVerifyStatus) {
		this.flowVerifyStatus = flowVerifyStatus;
	}
	public String getFlowVerifyStatusName() {
		String fvsn = CheckStatusType.getName(flowVerifyStatus);
		if(flowVerifyStatus != null && !CheckStatusType.WAIT_AUDITED.getCode().equals(flowVerifyStatus) && readTime == null) {
			return fvsn.replace("已", "已被他人");
		}
		return fvsn != null?fvsn:flowVerifyStatusName;
	}
	public void setFlowVerifyStatusName(String flowVerifyStatusName) {
		this.flowVerifyStatusName = flowVerifyStatusName;
	}
	public String getFlowVerifyReason() {
		return StringUtilsV2.isNotBlank(flowVerifyReason)?flowVerifyReason:"无";
	}
	public void setFlowVerifyReason(String flowVerifyReason) {
		this.flowVerifyReason = flowVerifyReason;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getReadUserName() {
		return readUserName;
	}
	public void setReadUserName(String readUserName) {
		this.readUserName = readUserName;
	}
	public String getFlowName() {
		return flowName;
	}
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	public String getFlowFromId() {
		return flowFromId;
	}
	public void setFlowFromId(String flowFromId) {
		this.flowFromId = flowFromId;
	}
	public String getTaskType() {
		return taskType;
	}
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	public String getTaskTypeName() {
		return taskTypeName;
	}
	public void setTaskTypeName(String taskTypeName) {
		this.taskTypeName = taskTypeName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTypeName() {
		return MsgType.getName(type);
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
}
