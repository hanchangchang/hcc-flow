package com.hcc.flow.server.model.sys;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 参数类型实体
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class ParaType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1492035297715805477L;

	private Long paraTypeId;// 参数类型ID
	private String paraTypeCode;// 参数类型编码
	private String paraTypeName;// 参数类型名称
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date createTime;// 创建时间
	private String createUserId;// 创建人ID

	public Long getParaTypeId() {
		return paraTypeId;
	}

	public void setParaTypeId(Long paraTypeId) {
		this.paraTypeId = paraTypeId;
	}

	public String getParaTypeCode() {
		return paraTypeCode;
	}

	public void setParaTypeCode(String paraTypeCode) {
		this.paraTypeCode = paraTypeCode;
	}

	public String getParaTypeName() {
		return paraTypeName;
	}

	public void setParaTypeName(String paraTypeName) {
		this.paraTypeName = paraTypeName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
}
