package com.hcc.flow.server.vo.sys;

import com.hcc.flow.server.common.enums.IfStatusType;
import com.hcc.flow.server.model.sys.ParaValue;

/**
 * 参数实体
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class ParaValueDetailVO extends ParaValue {

	private static final long serialVersionUID = -6440972911667536686L;
	// 不是数据库字段
	private String paraTypeName;// 参数类型名称

	private String createUserName;// 创建人
	private String updateUserName;// 更新人
	private String isDelName;// 是否可删除(Y是N否)
	private String isUpdateName;// 是否可修改(Y是N否)
	
	/*private String createTimeCheck;// 创建时间
	private String updateTimeCheck;// 更新时间
	@JsonIgnore
	private Date now = new Date();//当前日期
	
	public String getCreateTimeCheck() {
		return CommUtil.dateCheck(super.getCreateTime(),now);
	}

	public void setCreateTimeCheck(String createTimeCheck) {
		this.createTimeCheck = createTimeCheck;
	}

	public String getUpdateTimeCheck() {
		return CommUtil.dateCheck(super.getUpdateTime(),now);
	}

	public void setUpdateTimeCheck(String updateTimeCheck) {
		this.updateTimeCheck = updateTimeCheck;
	}*/
	public String getIsDelName() {
		return super.getIsDel() != null ?IfStatusType.getName(super.getIsDel()):isDelName;
	}


	public void setIsDelName(String isDelName) {
		this.isDelName = isDelName;
	}


	public String getIsUpdateName() {
		return super.getIsUpdate() != null ?IfStatusType.getName(super.getIsUpdate()):isUpdateName;
	}


	public void setIsUpdateName(String isUpdateName) {
		this.isUpdateName = isUpdateName;
	}


	public String getCreateUserName() {
		return createUserName;
	}


	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	public String getParaTypeName() {
		return paraTypeName;
	}

	public void setParaTypeName(String paraTypeName) {
		this.paraTypeName = paraTypeName;
	}


	public String getUpdateUserName() {
		return updateUserName;
	}


	public void setUpdateUserName(String updateUserName) {
		this.updateUserName = updateUserName;
	}

}
