package com.hcc.flow.server.vo.sys;

import java.io.Serializable;

/**
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class SurveyVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4513193760019299996L;

	private String code;// 编码
	private String value;// 值
	
	public SurveyVO() {

	}
	public SurveyVO(String code, String value) {
		super();
		this.code = code;
		this.value = value;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}
