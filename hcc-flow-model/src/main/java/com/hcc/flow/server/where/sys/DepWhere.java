package com.hcc.flow.server.where.sys;

import java.io.Serializable;
import java.util.List;

import com.hcc.flow.server.common.enums.DataStatusType;
import com.hcc.flow.server.common.where.PublicPara;

/**
 * @author 韩长志 2019-11-19
 * 
 */
public class DepWhere extends PublicPara implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String depName;// 部门名称
	String sqlOrder;
	Integer offset;
	Integer limit;
	List<String> statuss;// 使用状态
	String notStatus = DataStatusType.DELETE.getCode();

	public String getNotStatus() {
		return notStatus;
	}

	public void setNotStatus(String notStatus) {
		this.notStatus = notStatus;
	}

	public List<String> getStatuss() {
		return statuss;
	}

	public void setStatuss(List<String> statuss) {
		this.statuss = statuss;
	}

	public String getSqlOrder() {
		return sqlOrder;
	}

	public void setSqlOrder(String sqlOrder) {
		this.sqlOrder = sqlOrder;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public String getDepName() {
		return depName;
	}

	public void setDepName(String depName) {
		this.depName = depName;
	}

}
