package com.hcc.flow.server.model.flowDesign;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 工作流 流程 副本
 * @author 韩长志 2019-11-19
 *
 */
public class FlowDesignTemp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6096761967550851049L;
	private String flowTempId;//流程id
	private String flowId;//流程id
	private String flowCode;//流程编码
	private String flowName;//流程名称
	private String flowData;//流程具体内容
	private String flowRemarks;//流程说明
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date createTime;//创建时间
	private String createUserId;//创建人员id
	private String isUsed;//是否使用0未使用 1已使用
	private String editLog;// 操作日志
	private Integer version;// 版本号
	
	public String getFlowTempId() {
		return flowTempId;
	}
	public void setFlowTempId(String flowTempId) {
		this.flowTempId = flowTempId;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public String getFlowId() {
		return flowId;
	}
	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}
	public String getFlowName() {
		return flowName;
	}
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	public String getFlowData() {
		return flowData;
	}
	public void setFlowData(String flowData) {
		this.flowData = flowData;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getIsUsed() {
		return isUsed;
	}
	public void setIsUsed(String isUsed) {
		this.isUsed = isUsed;
	}
	public String getFlowCode() {
		return flowCode;
	}
	public void setFlowCode(String flowCode) {
		this.flowCode = flowCode;
	}
	public String getFlowRemarks() {
		return flowRemarks;
	}
	public void setFlowRemarks(String flowRemarks) {
		this.flowRemarks = flowRemarks;
	}
	public String getEditLog() {
		return editLog;
	}
	public void setEditLog(String editLog) {
		this.editLog = editLog;
	}
	public Integer getVarsion() {
		return version;
	}
	public void setVarsion(Integer version) {
		this.version = version;
	}
}
