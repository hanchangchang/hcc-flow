package com.hcc.flow.server.vo.flowDesign;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 工作流 流程副本
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class FlowDesignTempLogVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4124460858188067722L;
	private String flowId;// 流程id
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;// 创建时间
	private String createUserName;// 创建人员Name
	private String editLog;// 操作日志
	private Integer version;// 版本号
	
	public String getFlowId() {
		return flowId;
	}

	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getEditLog() {
		return editLog;
	}

	public void setEditLog(String editLog) {
		this.editLog = editLog;
	}

	public Integer getVarsion() {
		return version;
	}

	public void setVarsion(Integer version) {
		this.version = version;
	}
}
