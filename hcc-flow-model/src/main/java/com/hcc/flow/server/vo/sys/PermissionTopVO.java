package com.hcc.flow.server.vo.sys;

import java.io.Serializable;

/**
 * 权限实体
 * 
 * @author hanchangzhi
 *
 */
public class PermissionTopVO implements Serializable {

	private static final long serialVersionUID = -4038625433886629941L;
	private String permissionId;// 权限ID
	private String permissionName;// 权限名称
	private String href;// 链接地址
	private Integer type;// 类型(1目录2菜单3按钮4数据)
	private Integer sort;// 排序
	private Integer level;// 菜单所在层级

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}


	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}


	public String getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(String permissionId) {
		this.permissionId = permissionId;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

}
