package com.hcc.flow.server.dto.flowDesign;

import java.io.Serializable;

/**
 * 工作流 流程
 * @author 韩长志 2019-11-19
 *
 */
public class FlowDesignJsonDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6096761967550851049L;
	private String flowId;//流程id
	private String flowCode;//流程id
	private String flowName;//流程Name
	private String flowData;//流程具体内容
	private String flowRemarks;// 流程说明

	public String getFlowId() {
		return flowId;
	}
	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}
	
	public String getFlowData() {
		return flowData;
	}
	public void setFlowData(String flowData) {
		this.flowData = flowData;
	}
	public String getFlowCode() {
		return flowCode;
	}
	public void setFlowCode(String flowCode) {
		this.flowCode = flowCode;
	}
	public String getFlowRemarks() {
		return flowRemarks;
	}
	public void setFlowRemarks(String flowRemarks) {
		this.flowRemarks = flowRemarks;
	}
	public String getFlowName() {
		return flowName;
	}
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
}
