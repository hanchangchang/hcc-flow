package com.hcc.flow.server.vo.sys;

import java.io.Serializable;

/**
 * 参数
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class ParaValueVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6440972911667536686L;
	private String paraId;// 参数ID
	private String paraName;// 参数名称

	public ParaValueVO(String paraId, String paraName) {
		super();
		this.paraId = paraId;
		this.paraName = paraName;
	}

	public String getParaId() {
		return paraId;
	}

	public void setParaId(String paraId) {
		this.paraId = paraId;
	}

	public String getParaName() {
		return paraName;
	}

	public void setParaName(String paraName) {
		this.paraName = paraName;
	}
}
