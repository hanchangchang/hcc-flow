package com.hcc.flow.server.vo.sys;

import java.util.List;

import com.hcc.flow.server.common.vo.SelectVO;

/**
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class SurveyDetailVO extends SurveyVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4513193760019299996L;

	
	private String name;// 名称
	private List<SelectVO> select;// 可选项
	
	public SurveyDetailVO(){
		
	}
	
	public SurveyDetailVO(String code,String name,String value,List<SelectVO> select){
		this.name = name;
		this.select = select;
		super.setCode(code);
		super.setValue(value);
	}
	
	
	public List<SelectVO> getSelect() {
		return select;
	}
	
	public void setSelect(List<SelectVO> select) {
		this.select = select;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
