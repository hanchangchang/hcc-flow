package com.hcc.flow.server.dto.sys;

import java.util.List;

import com.hcc.flow.server.model.sys.Role;

public class RoleDto extends Role {

	private static final long serialVersionUID = 4218495592167610193L;
	private Long orgTypeId;// 角色对应的类型Id
	private String orgTypeName;// 机构对应的类型Name
	private List<String> permissionIds;

	public List<String> getPermissionIds() {
		return permissionIds;
	}

	public void setPermissionIds(List<String> permissionIds) {
		this.permissionIds = permissionIds;
	}

	public Long getOrgTypeId() {
		return orgTypeId;
	}

	public void setOrgTypeId(Long orgTypeId) {
		this.orgTypeId = orgTypeId;
	}

	public String getOrgTypeName() {
		return orgTypeName;
	}

	public void setOrgTypeName(String orgTypeName) {
		this.orgTypeName = orgTypeName;
	}
}
