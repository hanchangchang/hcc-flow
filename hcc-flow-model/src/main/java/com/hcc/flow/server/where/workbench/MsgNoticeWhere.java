package com.hcc.flow.server.where.workbench;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.hcc.flow.server.common.enums.DataStatusType;
import com.hcc.flow.server.common.where.PublicPara;

/**
 * @author 韩长志 2019-11-19
 * 
 */
public class MsgNoticeWhere extends PublicPara implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String titleOrContent;// 标题或者文字
	String pushUserId;// 发送人
	Date beginTime;
	Date endTime;
	String type;// 消息类型（2系统消息3站内信消息）
	List<String> orgIds;//接收机构ids
	List<String> roleIds;//接收角色ids
	List<String> collectUserIds;//接收用户ids
	String sqlOrder;
	Integer offset;
	Integer limit;
	
	String notStatus = DataStatusType.DELETE.getCode();// 不用查询的状态
	
	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getNotStatus() {
		return notStatus;
	}

	public void setNotStatus(String notStatus) {
		this.notStatus = notStatus;
	}

	public String getSqlOrder() {
		return sqlOrder;
	}

	public void setSqlOrder(String sqlOrder) {
		this.sqlOrder = sqlOrder;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public String getTitleOrContent() {
		return titleOrContent;
	}

	public void setTitleOrContent(String titleOrContent) {
		this.titleOrContent = titleOrContent;
	}

	public String getPushUserId() {
		return pushUserId;
	}

	public void setPushUserId(String pushUserId) {
		this.pushUserId = pushUserId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<String> getOrgIds() {
		return orgIds;
	}
	
	public void setOrgIds(List<String> orgIds) {
		if(!CollectionUtils.isEmpty(orgIds) && orgIds.contains("-1")){
			orgIds = null;
		}
		this.orgIds = orgIds;
	}
	
	public List<String> getRoleIds() {
		return roleIds;
	}
	
	public void setRoleIds(List<String> roleIds) {
		if(!CollectionUtils.isEmpty(roleIds)){
			this.orgIds = null;
			if(roleIds.contains("-1")){
				roleIds.remove("-1");
			}
		}
		this.roleIds = roleIds;
	}
	
	public List<String> getCollectUserIds() {
		return collectUserIds;
	}
	
	public void setCollectUserIds(List<String> collectUserIds) {
		if(!CollectionUtils.isEmpty(collectUserIds)){
			this.orgIds = null;
			this.roleIds = null;
			if(collectUserIds.contains("-1")){
				collectUserIds.remove("-1");
			}
		}
		this.collectUserIds = collectUserIds;
	}
	
}
