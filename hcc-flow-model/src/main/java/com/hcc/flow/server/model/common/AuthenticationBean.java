package com.hcc.flow.server.model.common;

import java.io.Serializable;

import com.hcc.flow.server.common.where.PublicPara;

/**
 * @author 作者:韩长志
 * @version 创建时间：2018年9月25日 下午5:36:50
 * 
 */
public class AuthenticationBean extends PublicPara implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7754845166408233931L;
	private String username;
	private String password;
	private String verifyId;
	private String verifyCode;
	private String sysid;
	
	public String getSysid() {
		return sysid;
	}

	public void setSysid(String sysid) {
		this.sysid = sysid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getVerifyId() {
		return verifyId;
	}

	public void setVerifyId(String verifyId) {
		this.verifyId = verifyId;
	}

	public String getVerifyCode() {
		return verifyCode;
	}

	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}
}
