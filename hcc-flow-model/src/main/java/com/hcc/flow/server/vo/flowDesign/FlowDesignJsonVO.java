package com.hcc.flow.server.vo.flowDesign;

import java.io.Serializable;

/**
 * 工作流 流程
 * @author 韩长志 2019-11-19
 *
 */
public class FlowDesignJsonVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6096761967550851049L;
	private String flowId;//流程id
	private String flowCode;//流程id
	private String flowName;//流程名称
	private String flowType;// 流程归属类型(1素材2订单3刊播4换刊5客户6品牌)
	private String flowTypeName;// 流程归属类型Name
	private String flowAttribute;//流程属性 0普通流程 1复合流程
	@SuppressWarnings("unused")
	private String flowAttributeName;//流程属性 0普通流程 1复合流程
	private String followOrgName;// 归属机构Name
	private String flowData;//流程具体内容
	private String flowRemarks;// 流程说明

	public String getFlowName() {
		return flowName;
	}
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	public String getFlowId() {
		return flowId;
	}
	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}
	
	public String getFlowData() {
		return flowData;
	}
	public void setFlowData(String flowData) {
		this.flowData = flowData;
	}
	
	public String getFlowTypeName() {
		return flowTypeName;

	}
	public void setFlowTypeName(String flowTypeName) {
		this.flowTypeName = flowTypeName;
	}
	public String getFlowCode() {
		return flowCode;
	}
	public void setFlowCode(String flowCode) {
		this.flowCode = flowCode;
	}
	public String getFlowType() {
		return flowType;
	}
	public void setFlowType(String flowType) {
		this.flowType = flowType;
	}
	public String getFollowOrgName() {
		return followOrgName;
	}
	public void setFollowOrgName(String followOrgName) {
		this.followOrgName = followOrgName;
	}
	public String getFlowRemarks() {
		return flowRemarks;
	}
	public void setFlowRemarks(String flowRemarks) {
		this.flowRemarks = flowRemarks;
	}
	public String getFlowAttribute() {
		return flowAttribute;
	}

	public void setFlowAttribute(String flowAttribute) {
		this.flowAttribute = flowAttribute;
	}

	public String getFlowAttributeName() {
		return "0".equals(flowAttribute)?"普通流程":"复合流程";
	}

	public void setFlowAttributeName(String flowAttributeName) {
		this.flowAttributeName = flowAttributeName;
	}
}
