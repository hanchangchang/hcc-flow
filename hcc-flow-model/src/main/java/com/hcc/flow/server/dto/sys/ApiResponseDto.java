package com.hcc.flow.server.dto.sys;

import java.io.Serializable;

import com.hcc.flow.server.common.where.PublicPara;

/**
 * @author 作者:韩长志
 * @version 创建时间：2018年9月25日 下午10:13:16
 * 
 */
@SuppressWarnings("serial")
public class ApiResponseDto<T> extends PublicPara implements Serializable {
	

	/* 数据 */
	private T datas;

	public T getDatas() {
		return datas;
	}

	public void setDatas(T datas) {
		this.datas = datas;
	}


}
