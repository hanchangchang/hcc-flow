package com.hcc.flow.server.vo.sys;

import java.io.Serializable;

/**
 * 区域
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class AreaVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4513193760099299996L;

	private Long areaId;// 区域ID
	private String areaName;// 区域名称
	private Long parentId;// 上级区域ID
	private Integer areaType;// 区域类型
	private String parentName;// 上级区域名称
	private Integer sort;// 顺序号
	
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Long getAreaId() {
		return areaId;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getAreaType() {
		return areaType;
	}

	public void setAreaType(Integer areaType) {
		this.areaType = areaType;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
}
