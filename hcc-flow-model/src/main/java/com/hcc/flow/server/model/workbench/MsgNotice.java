package com.hcc.flow.server.model.workbench;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class MsgNotice implements Serializable {

	private static final long serialVersionUID = -4401913568806243090L;
	private String noticeId;//消息id
	private String type;// 消息类型（1任务消息2系统消息3站内信4结果消息）
	private String title;//标题
	private String content;//内容
	private String taskType;// 具体任务类型（1素材2订单3刊播4换刊5客户6品牌7其他）
	private String href;//点击跳转的路由地址(页面)
	private String pushUserId;// 发送人ID
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date pushTime;// 发送时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date showBeginTime;// 展示开始时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date showEndTime;// 展示结束时间
	private String status;// 状态（C正常R停用D删除）
	
	private String flowTempId;// 流程副本表id
	private String flowTempNodeId;// 流程副本节点id
	private String flowFromId;// 流程表单id
	private String flowSubmitNo;//业务流程提交序号
	public String getFlowTempId() {
		return flowTempId;
	}
	public void setFlowTempId(String flowTempId) {
		this.flowTempId = flowTempId;
	}
	public String getFlowTempNodeId() {
		return flowTempNodeId;
	}
	public void setFlowTempNodeId(String flowTempNodeId) {
		this.flowTempNodeId = flowTempNodeId;
	}
	public String getFlowFromId() {
		return flowFromId;
	}
	public void setFlowFromId(String flowFromId) {
		this.flowFromId = flowFromId;
	}
	public String getNoticeId() {
		return noticeId;
	}
	public void setNoticeId(String noticeId) {
		this.noticeId = noticeId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getPushUserId() {
		return pushUserId;
	}
	public void setPushUserId(String pushUserId) {
		this.pushUserId = pushUserId;
	}
	public Date getPushTime() {
		return pushTime;
	}
	public void setPushTime(Date pushTime) {
		this.pushTime = pushTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getTaskType() {
		return taskType;
	}
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	public Date getShowBeginTime() {
		return showBeginTime;
	}
	public void setShowBeginTime(Date showBeginTime) {
		this.showBeginTime = showBeginTime;
	}
	public Date getShowEndTime() {
		return showEndTime;
	}
	public void setShowEndTime(Date showEndTime) {
		this.showEndTime = showEndTime;
	}
	public String getFlowSubmitNo() {
		return flowSubmitNo;
	}
	public void setFlowSubmitNo(String flowSubmitNo) {
		this.flowSubmitNo = flowSubmitNo;
	}
	
}
