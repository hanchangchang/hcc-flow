package com.hcc.flow.server.model.sys;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 部门实体
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class Dep implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2332081177517535832L;

	private String depId;// 部门ID
	private String orgId;// 机构ID
	private String depCode;// 部门编码
	private String depName;// 部门名称
	private String depDesc;// 部门说明
	private String parentId;// 上级部门ID
	private Integer sortNo;// 排序号
	private String depStatus;// 状态(C正常R停用D删除)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date createTime;// 创建时间
	private String createUserId;// 创建人ID
	private Integer level;// 层级

	public String getDepId() {
		return depId;
	}

	public void setDepId(String depId) {
		this.depId = depId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getDepCode() {
		return depCode;
	}

	public void setDepCode(String depCode) {
		this.depCode = depCode;
	}

	public String getDepName() {
		return depName;
	}

	public void setDepName(String depName) {
		this.depName = depName;
	}

	public String getDepDesc() {
		return depDesc;
	}

	public void setDepDesc(String depDesc) {
		this.depDesc = depDesc;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}

	public String getDepStatus() {
		return depStatus;
	}

	public void setDepStatus(String depStatus) {
		this.depStatus = depStatus;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}
}
