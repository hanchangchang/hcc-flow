package com.hcc.flow.server.model.workbench;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hcc.flow.server.common.utils.StringUtilsV2;

public class MsgNoticeRead implements Serializable {

	private static final long serialVersionUID = -4401913568806243090L;
	private String readId;//阅读id
	private String noticeId;//消息id
	private String readUserId;// 阅读人ID/待阅读人ID
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date readTime;// 阅读时间(未读时为null)
	private String status;// 状态（0未读 1已读 2删除）
	
	private String flowVerifyStatus;//审核状态(2审核中 3已核准 4已驳回 6已转办)
	private String flowVerifyReason;//审核意见
	
	public String getReadId() {
		return readId;
	}
	public void setReadId(String readId) {
		this.readId = readId;
	}
	public String getNoticeId() {
		return noticeId;
	}
	public void setNoticeId(String noticeId) {
		this.noticeId = noticeId;
	}
	public String getReadUserId() {
		return readUserId;
	}
	public void setReadUserId(String readUserId) {
		this.readUserId = readUserId;
	}
	public Date getReadTime() {
		return readTime;
	}
	public void setReadTime(Date readTime) {
		this.readTime = readTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFlowVerifyStatus() {
		return flowVerifyStatus;
	}
	public void setFlowVerifyStatus(String flowVerifyStatus) {
		this.flowVerifyStatus = flowVerifyStatus;
	}
	public String getFlowVerifyReason() {
		return StringUtilsV2.isNotBlank(flowVerifyReason)?flowVerifyReason:"无";
	}
	public void setFlowVerifyReason(String flowVerifyReason) {
		this.flowVerifyReason = flowVerifyReason;
	}

}
