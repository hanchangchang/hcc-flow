package com.hcc.flow.server.vo.workbench;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hcc.flow.server.common.enums.MsgType;

@SuppressWarnings("unused")
public class MsgNoticeVO implements Serializable {

	private static final long serialVersionUID = -4401913568806243090L;
	private String readId;//阅读消息id
	private String noticeId;//消息id
	//private String type;// 消息类型（1任务消息2系统消息3站内信4结果消息）
	private String title;//标题
	private String content;//内容
	//private String taskType;// 具体任务类型（1素材2订单3刊播4换刊5客户6品牌7其他）
	//private String href;//点击跳转的路由地址(页面)
	private String pushUserName;// 发送人Name
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date pushTime;// 发送时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date showBeginTime;// 展示开始时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date showEndTime;// 展示结束时间
	
	private String flowFromId;// 表单id
	
	private String type;// 消息类型（1任务消息2系统消息3站内信4结果消息）
	
	private String typeName;// 消息类型（1任务消息2系统消息3站内信4结果消息）
	
	private String taskType;// 具体任务类型（1素材2订单3刊播4换刊5客户6品牌7其他）
	
	private String taskTypeName;// 具体任务类型（1素材2订单3刊播4换刊5客户6品牌7其他）
	
	//private String status;// 状态（C正常R停用D删除）
	
	public String getNoticeId() {
		return noticeId;
	}
	public String getReadId() {
		return readId;
	}
	public void setReadId(String readId) {
		this.readId = readId;
	}
	public void setNoticeId(String noticeId) {
		this.noticeId = noticeId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getPushUserName() {
		return pushUserName;
	}
	public void setPushUserName(String pushUserName) {
		this.pushUserName = pushUserName;
	}
	public Date getPushTime() {
		return pushTime;
	}
	public void setPushTime(Date pushTime) {
		this.pushTime = pushTime;
	}
	public Date getShowBeginTime() {
		return showBeginTime;
	}
	public void setShowBeginTime(Date showBeginTime) {
		this.showBeginTime = showBeginTime;
	}
	public Date getShowEndTime() {
		return showEndTime;
	}
	public void setShowEndTime(Date showEndTime) {
		this.showEndTime = showEndTime;
	}
	
	public String getFlowFromId() {
		return flowFromId;
	}
	public void setFlowFromId(String flowFromId) {
		this.flowFromId = flowFromId;
	}
	public String getTaskType() {
		return taskType;
	}
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	public String getTaskTypeName() {
		return taskTypeName;
	}
	public void setTaskTypeName(String taskTypeName) {
		this.taskTypeName = taskTypeName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTypeName() {
		return MsgType.getName(type);
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
}
