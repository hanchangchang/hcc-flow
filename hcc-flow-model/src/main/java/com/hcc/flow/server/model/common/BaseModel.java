package com.hcc.flow.server.model.common;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hcc.flow.server.common.annotation.FieldTypeAnnotation;

/**
 * 实体基类
 * 
 * @author 韩长志 2019-11-19
 *
 */
public abstract class BaseModel implements Serializable {

	private static final long serialVersionUID = 2054813493011812469L;

	@FieldTypeAnnotation(exp = "创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date createTime;// 创建时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date updateTime;// 更新时间
	private String createUserId;// 创建人ID
	private String updateUserId;// 更新人ID

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = (updateTime == null ? new Date() : updateTime);
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
}
