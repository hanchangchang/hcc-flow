package com.hcc.flow.server.dto.workbench;

import java.util.List;

import org.springframework.util.CollectionUtils;

import com.hcc.flow.server.model.workbench.MsgNotice;

public class MsgNoticeDto extends MsgNotice{

	private static final long serialVersionUID = -4401913568806243090L;
	private List<String> orgIds;//接收机构ids
	private List<String> roleIds;//接收角色ids
	private List<String> collectUserIds;//接收用户ids
	
	private String flowAttribute = "0";//流程属性 0普通流程 1复合流程
	private String flowFromId;//流程表单id
	private String flowVerifyStatus;//审核状态(2审核中 3已核准 4已驳回 6已转办)
	private String flowVerifyReason;//审核意见
	
	private String flowTempId;//流程模板id
	private String flowTempNodeId;//流程模板节点id
	
	private String flowOrgId;//走审核流程的机构ID
	
	private Boolean flowFZ = false;//是否走刊播复杂流程
	
	public Boolean getFlowFZ() {
		return flowFZ;
	}

	public void setFlowFZ(Boolean flowFZ) {
		this.flowFZ = flowFZ;
	}

	public List<String> getOrgIds() {
		return orgIds;
	}
	
	public void setOrgIds(List<String> orgIds) {
		if(!CollectionUtils.isEmpty(orgIds) && orgIds.contains("-1")){
			orgIds = null;
		}
		this.orgIds = orgIds;
	}
	
	public List<String> getRoleIds() {
		return roleIds;
	}
	
	public void setRoleIds(List<String> roleIds) {
		if(!CollectionUtils.isEmpty(roleIds)){
			this.orgIds = null;
			if(roleIds.contains("-1")){
				roleIds.remove("-1");
			}
		}
		this.roleIds = roleIds;
	}
	
	public List<String> getCollectUserIds() {
		return collectUserIds;
	}
	
	public void setCollectUserIds(List<String> collectUserIds) {
		if(!CollectionUtils.isEmpty(collectUserIds)){
			this.orgIds = null;
			this.roleIds = null;
			if(collectUserIds.contains("-1")){
				collectUserIds.remove("-1");
			}
		}
		this.collectUserIds = collectUserIds;
	}

	public String getFlowAttribute() {
		return flowAttribute;
	}

	public void setFlowAttribute(String flowAttribute) {
		this.flowAttribute = flowAttribute;
	}

	public String getFlowFromId() {
		return flowFromId;
	}

	public void setFlowFromId(String flowFromId) {
		this.flowFromId = flowFromId;
	}

	public String getFlowVerifyStatus() {
		return flowVerifyStatus;
	}

	public void setFlowVerifyStatus(String flowVerifyStatus) {
		this.flowVerifyStatus = flowVerifyStatus;
	}

	public String getFlowVerifyReason() {
		return flowVerifyReason;
	}

	public void setFlowVerifyReason(String flowVerifyReason) {
		this.flowVerifyReason = flowVerifyReason;
	}

	public String getFlowTempId() {
		return flowTempId;
	}

	public void setFlowTempId(String flowTempId) {
		this.flowTempId = flowTempId;
	}

	public String getFlowTempNodeId() {
		return flowTempNodeId;
	}

	public void setFlowTempNodeId(String flowTempNodeId) {
		this.flowTempNodeId = flowTempNodeId;
	}

	public String getFlowOrgId() {
		return flowOrgId;
	}

	public void setFlowOrgId(String flowOrgId) {
		this.flowOrgId = flowOrgId;
	}

}
