package com.hcc.flow.server.dto.sys;

import java.util.List;

import com.hcc.flow.server.model.sys.SysUser;

public class UserDto extends SysUser {

	private static final long serialVersionUID = -184009306207076712L;

	private List<String> roleIds;
	
	private String orgName;
	
	private String verifyIdR;
	
	private String verifyCodeR;

	public List<String> getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(List<String> roleIds) {
		this.roleIds = roleIds;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getVerifyIdR() {
		return verifyIdR;
	}

	public void setVerifyIdR(String verifyIdR) {
		this.verifyIdR = verifyIdR;
	}

	public String getVerifyCodeR() {
		return verifyCodeR;
	}

	public void setVerifyCodeR(String verifyCodeR) {
		this.verifyCodeR = verifyCodeR;
	}

}
