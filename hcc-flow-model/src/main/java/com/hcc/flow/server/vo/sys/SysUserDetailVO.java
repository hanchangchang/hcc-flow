package com.hcc.flow.server.vo.sys;

import java.util.List;

/**
 * 系统用户实体VO
 * 
 * @author hanchangzhi
 *
 */
public class SysUserDetailVO extends SysUserVO {

	private static final long serialVersionUID = -6525908775032868837L;

	private List<String> roleIds;

	public List<String> getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(List<String> roleIds) {
		this.roleIds = roleIds;
	}

}
