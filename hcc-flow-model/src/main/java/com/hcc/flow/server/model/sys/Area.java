package com.hcc.flow.server.model.sys;

import java.io.Serializable;
import java.util.List;

/**
 * 区域实体
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class Area implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2332081177517585832L;

	private Long areaId;// 区域ID
	private String areaName;// 区域名称
	private Long parentId;// 上级区域ID
	private Integer areaType;// 区域类型
	private Integer sort;// 顺序号
	private Integer show;// 顺序号
	// 不存入数据库
	private List<Area> child;// 对应子区域
	private List<Long> parentIds;
	
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Long getAreaId() {
		return areaId;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getAreaType() {
		return areaType;
	}

	public void setAreaType(Integer areaType) {
		this.areaType = areaType;
	}

	public List<Area> getChild() {
		return child;
	}

	public void setChild(List<Area> child) {
		this.child = child;
	}

	public List<Long> getParentIds() {
		return parentIds;
	}

	public void setParentIds(List<Long> parentIds) {
		this.parentIds = parentIds;
	}

	public Integer getShow() {
		return show;
	}

	public void setShow(Integer show) {
		this.show = show;
	}
	
}
