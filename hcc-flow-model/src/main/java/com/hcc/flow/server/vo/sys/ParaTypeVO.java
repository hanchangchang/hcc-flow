package com.hcc.flow.server.vo.sys;

import com.hcc.flow.server.model.sys.ParaType;

/**
 * 参数类型实体
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class ParaTypeVO extends ParaType {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1492035297715805477L;


	private String createUserName;// 创建人ID
	/*@SuppressWarnings("unused")
	private String createTimeCheck;// 创建时间
	@JsonIgnore
	private Date now = new Date();//当前日期
	
	public String getCreateTimeCheck() {
		return CommUtil.dateCheck(super.getCreateTime(),now);
	}

	public void setCreateTimeCheck(String createTimeCheck) {
		this.createTimeCheck = createTimeCheck;
	}*/

	public String getCreateUserName() {
		return createUserName;
	}


	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	
	
}
