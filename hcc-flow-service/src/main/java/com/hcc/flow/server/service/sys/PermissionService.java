package com.hcc.flow.server.service.sys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcc.flow.server.dao.sys.PermissionDao;
import com.hcc.flow.server.model.sys.Permission;

@Service
public class PermissionService {

	private static final Logger log = LoggerFactory.getLogger("adminLogger");

	@Autowired
	private PermissionDao permissionDao;

	
	public void save(Permission permission) {
		permissionDao.save(permission);

		log.debug("新增菜单{}", permission.getPermissionName());
	}

	
	public void update(Permission permission) {
		permissionDao.update(permission);
	}

	
	@Transactional
	public void delete(String id) {
		permissionDao.deleteRolePermission(id);
		permissionDao.deleteByParentId(id);
		permissionDao.delete(id);

		log.debug("删除菜单ID: {}", id);
	}

}
