package com.hcc.flow.server.service.sys;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcc.flow.server.dao.sys.ParaValueDao;

@Service
public class SysService  {


	@Autowired
	private ParaValueDao paraValueDao;

	
	public String getWorkPrefix() {
		// TODO Auto-generated method stub
		return paraValueDao.getWorkPrefix();
	}
}
