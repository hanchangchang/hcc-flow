package com.hcc.flow.server.service.sys;

import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.hcc.flow.server.common.advice.RRException;
import com.hcc.flow.server.common.enums.DataStatusType;
import com.hcc.flow.server.common.enums.IfStatusType;
import com.hcc.flow.server.common.utils.StringUtilsV2;
import com.hcc.flow.server.common.utils.TokenUtil;
import com.hcc.flow.server.dao.sys.OrgDao;
import com.hcc.flow.server.dao.sys.PermissionDao;
import com.hcc.flow.server.dao.sys.RoleDao;
import com.hcc.flow.server.model.common.LoginUser;
import com.hcc.flow.server.model.sys.SysUser;
import com.hcc.flow.server.vo.sys.OrgOneVO;

/**
 * spring security登陆处理
 * 
 * @author 韩长长 
 *
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserService userService;
	@Autowired
	private PermissionDao permissionDao;
	@Autowired
	private RoleDao roleDao;
	@Autowired
	private OrgDao orgDao;
	
	@Override
	public UserDetails loadUserByUsername(String userAcct) throws UsernameNotFoundException {
		if (StringUtilsV2.isBlank(userAcct)) {
			throw new AuthenticationCredentialsNotFoundException("没有解析到用户账号");
		}
		SysUser sysUser = userService.getUser(userAcct);
		if(sysUser == null){
			sysUser = userService.getUserPhone(userAcct);
			if(sysUser != null && IfStatusType.N.getCode().equals(sysUser.getIsPhoneLogin())){
				throw new RRException("该用户不允许用手机号登陆,请使用账号登陆");
			}
		}
		if (sysUser == null) {
			throw new AuthenticationCredentialsNotFoundException("用户名不存在");
		} else if (DataStatusType.STOP.getCode().equals(sysUser.getStatus())) {
			throw new LockedException("用户被锁定(停用),如有疑问，请联系管理员");
		} else if (DataStatusType.DELETE.getCode().equals(sysUser.getStatus())) {
			throw new DisabledException("用户已作废");
		} else if (IfStatusType.N.getCode().equals(sysUser.getIsLogin())) {
			throw new RRException("该用户不允许登录");
		}
		LoginUser loginUser = new LoginUser();
		BeanUtils.copyProperties(sysUser, loginUser);
		//用户所有角色ids
		String userRoleIds = roleDao.getRoleIdsByUserId(sysUser.getUserId());
		loginUser.setRoldIds(userRoleIds);
		Set<String> permissions;
		Integer sysId = TokenUtil.getSysId(null);
		// 用户是超级管理员返回所有菜单数据
		if(loginUser.getIsSuperAdminRole())
			permissions = permissionDao.listPermissionCodeBySysId(sysId);
		else {
			permissions = permissionDao.listPermissionCodeByUserIdAndSysId(sysUser.getUserId(),sysId);
		}
		loginUser.setPermissions(permissions);
		if(StringUtilsV2.isNotBlank(loginUser.getOrgId())){
			OrgOneVO org = orgDao.getById(loginUser.getOrgId());
			if(org != null){
				loginUser.setOrgName(org.getOrgName());
				loginUser.setParentOrgId(org.getParentId());
				loginUser.setParentOrgName(org.getParentName());
			}
		}
		return loginUser;
	}

}
