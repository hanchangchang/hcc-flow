package com.hcc.flow.server.utils;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.hcc.flow.server.common.model.ApiResult;

/**
 * The Class CommUtil.
 */
public class CheckIdUtil {
	protected static final Log log = LogFactory.getLog(CheckIdUtil.class);

	/**
	 * Instantiates a new comm util.
	 */
	private CheckIdUtil() {
	}

	
    
    public static ApiResult<?> checkId(List<String> notIds,String checkId) {
    	//非administrator账户
		if(!"1".equals(UserUtil.getLoginUser().getUserId()) && notIds.contains(checkId)){
			return ApiResult.error("该数据为系统数据，不允许操作");
		}
		return ApiResult.ok();
	}
}
