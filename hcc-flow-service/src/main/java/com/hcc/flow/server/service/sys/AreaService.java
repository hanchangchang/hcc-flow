package com.hcc.flow.server.service.sys;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcc.flow.server.dao.sys.AreaDao;
import com.hcc.flow.server.model.sys.Area;
import com.hcc.flow.server.vo.sys.AreaCheckVO;
import com.hcc.flow.server.vo.sys.AreaVO;

@Service
public class AreaService {

	private static final Logger log = LoggerFactory.getLogger("adminLogger");

	@Autowired
	private AreaDao areaDao;

	public void save(Area area) {
		areaDao.save(area);

		log.debug("新增区域{}", area.getAreaName());
	}

	public void update(Area area) {
		areaDao.update(area);
	}

	@Transactional
	public void delete(Long id) {
		areaDao.delete(id);
		areaDao.deleteByParentId(id);

		log.debug("删除区域ID: {}", id);
	}

	public List<AreaVO> getAreasByType(Integer areaType) {

		List<AreaVO> areas = areaDao.listValidAddrAreaByAreaType(areaType);

		return areas;
	}

	public List<AreaCheckVO> getAreasCheckVoByType(Integer areaType) {

		List<AreaCheckVO> areas = null;

		//List<AreaVO> areasv = getAreasByType(areaType);

		//areas = AssembleUtil.areaCheck(areasv);

		return areas;
	}
}
