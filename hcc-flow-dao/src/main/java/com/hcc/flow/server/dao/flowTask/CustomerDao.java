package com.hcc.flow.server.dao.flowTask;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.mapping.StatementType;

import com.hcc.flow.server.common.constant.Constant;
import com.hcc.flow.server.common.where.PublicWhere;
import com.hcc.flow.server.model.flowTask.Customer;
import com.hcc.flow.server.vo.flowTask.CustomerVO;

/**
 * CustomerDao
 * 
 * @author 韩长志
 *
 */
@Mapper
public interface CustomerDao {

	@Select("select * from customer t where status='" + Constant.CHECK_STATUS_WAREHOUSING + "' order by create_time desc")
	List<Customer> listAll();
	
	@Select("select count(t.customer_id) from customer t where t.status!='"+Constant.CHECK_STATUS_DELETE+"' and t.follow_user_id = #{followUserId}")
	int countByFollowUserId(String followUserId);

	@Select("select t.customer_id customerId,t.company_name companyName "
			+ "from customer t "
			+ "where t.status='" + Constant.CHECK_STATUS_WAREHOUSING + "' order by t.create_time desc")
	List<Map<String, Object>> listAllMap();
	
	@Select("select t.customer_id customerId,t.company_name companyName "
			+ "from customer t " 
			+ "where t.status='" + Constant.CHECK_STATUS_WAREHOUSING + "' and t.company_name like concat('%', #{companyName}, '%') order by t.create_time desc limit #{limit}")
	List<Map<String, Object>> listMapByCustomerName1(@Param("companyName") String companyName,@Param("limit") Integer limit);
	
	@Select("select t.customer_id customerId,t.company_name companyName "
			+ "from customer t " 
			+ "where t.status='" + Constant.CHECK_STATUS_WAREHOUSING + "' and t.company_name like concat('%', #{companyName}, '%') order by t.create_time desc")
	List<Map<String, Object>> listMapByCustomerName(@Param("companyName") String companyName);

	@Select("select t.*,b.para_name industry_para_name,cu.USER_NAME create_user_name,su.USER_NAME submit_user_name,vu.USER_NAME verify_user_name,fu.USER_NAME follow_user_name "
			+ "from customer t "
			+ "LEFT JOIN para_value b on t.industry_para_id=b.para_id "
			+ "LEFT JOIN sys_user cu on t.create_user_id=cu.USER_ID "
			+ "LEFT JOIN sys_user su on t.submitter_id=su.USER_ID "
			+ "LEFT JOIN sys_user vu on t.verify_user_id=vu.USER_ID "
			+ "LEFT JOIN sys_user fu on t.follow_user_id=fu.USER_ID "
			+ "where t.customer_id = #{id}")
	CustomerVO getById(String id);
	
	@Select("select t.* from customer t "
			+ "where (t.create_org_id = #{createOrgId} or t.follow_org_id = #{createOrgId}) "
			+ "and t.status="+Constant.CHECK_STATUS_WAREHOUSING+" order by t.create_time desc limit 1")
	Customer getJcByName(@Param("createOrgId") String createOrgId);
	
	@Select("select t.* from customer t where t.customer_id = #{id}")
	Customer getJcById(String id);
	
	@Select("select t.* from customer t where t.credit_code = #{creditCode} and t.status != '"+Constant.CHECK_STATUS_DELETE+"' limit 1")
	Customer getByCreditCode(String creditCode);

	@Delete("delete from customer where customer_id = #{id}")
	int delete(String id);

	int update(Customer customer);

	@Update("update customer set industry_para_id=#{industryParaId} where customer_id = #{customerId} ")
	int updateIndustry(@Param("customerId") String customerId, @Param("industryParaId") String industryParaId);
	
	@SelectKey(statement="select replace(UUID(),'-','')", keyProperty="customerId", before=true, statementType=StatementType.STATEMENT,resultType=String.class)
    @Insert("insert into customer(customer_id, index_id, company_name, company_addr, legal_representative, registered_capital, business_term_begin, business_term_end, credit_code, business_user, business_user_tel, status, create_user_id, create_time, submitter_id, verify_user_id, verify_time, create_org_id, follow_user_id, follow_org_id, register_place, industry_para_id) "
    		+ "values(#{customerId}, #{indexId}, #{companyName}, #{companyAddr}, #{legalRepresentative}, #{registeredCapital}, #{businessTermBegin}, #{businessTermEnd}, #{creditCode}, #{businessUser}, #{businessUserTel}, #{status}, #{createUserId}, now(), #{submitterId}, #{verifyUserId}, #{verifyTime}, #{createOrgId}, #{followUserId}, #{followOrgId}, #{registerPlace}, #{industryParaId})")
    int save(Customer customer);

	List<CustomerVO> CustomerList(PublicWhere params);

	@Update("<script>" + "update customer set status=#{status} where customer_id in "
			+ "<foreach item='item' index='index' collection='ids' open='(' separator=',' close=')'>" + "#{item}"
			+ "</foreach>" + "</script>")
	int updateStatus(@Param("ids") List<String> ids, @Param("status") String status);
	
	@Select("select su.user_id from customer c left join sys_user su on su.org_id=c.org_id where su.status='C' and c.customer_id=#{customerId}")
	List<String> getCustomerOrgUser(String customerId);
	
	@Select("select * from customer where org_id=#{orgId}")
	Customer getCustomerByOrgId(String orgId);
	
	@Select("select customer_id from customer where org_id=#{orgId} or create_org_id=#{orgId} or follow_org_id=#{orgId}")
	List<String> getCustomersByOrgId(String orgId);
}
