package com.hcc.flow.server.dao.flowTask;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.mapping.StatementType;

import com.hcc.flow.server.common.constant.Constant;
import com.hcc.flow.server.common.vo.SelectIdLongVO;
import com.hcc.flow.server.common.vo.SelectVO;
import com.hcc.flow.server.common.where.PublicWhere;
import com.hcc.flow.server.model.flowTask.AdContent;
import com.hcc.flow.server.vo.flowTask.AdContentVO;

/**
 * AdContentDao
 * 
 * @author 韩长志
 *
 */
@Mapper
public interface AdContentDao {

	@Select("select * from ad_content t where t.status!='" + Constant.CHECK_STATUS_DELETE
			+ "' order by create_time desc")
	List<AdContent> listAll();

	@Select("select cu.USER_NAME create_user_name,CONCAT(vu.USER_NAME,'(',o.org_name,')') verify_user_name,su.USER_NAME submitter_name,ad.ad_type_name,t.* " + 
			"from ad_content t " + 
			"LEFT JOIN sys_user cu on t.create_user_id=cu.USER_ID " + 
			"LEFT JOIN sys_user vu on t.verify_user_id=vu.USER_ID " + 
			"LEFT JOIN org o on vu.org_id=o.org_id " + 
			"LEFT JOIN sys_user su on t.submitter_id=su.USER_ID " + 
			"LEFT JOIN ad_type ad on t.ad_type_id=ad.ad_type_id " + 
			"where t.ad_id = #{id}")
	AdContentVO getById(String id);

	@Delete("delete from ad_content where ad_id = #{id}")
	int delete(String id);

	@Update("<script>" + "update ad_content set status='" + Constant.CHECK_STATUS_DELETE + "' where ad_id in "
			+ "<foreach item='item' index='index' collection='ids' open='(' separator=',' close=')'>" + "#{item}"
			+ "</foreach>" + "</script>")
	int deletes(@Param("ids") List<String> ids);

	@Update("<script>" + "update ad_content set status=#{status},submitter_id=#{submitterId} where ad_id in "
			+ "<foreach item='item' index='index' collection='ids' open='(' separator=',' close=')'>" + "#{item}"
			+ "</foreach>" + " and (status=1 or status=4)" + "</script>")
	int updateStatus(@Param("ids") List<String> ids, @Param("status") String status,@Param("submitterId") String submitterId);

	int update(AdContent adContent);

	@SelectKey(statement = "select replace(UUID(),'-','')", keyProperty = "adId", before = true, statementType = StatementType.STATEMENT, resultType = String.class)
	@Insert("insert into ad_content(ad_id, ad_type_id, ad_name, ad_size, ad_second, file_type, resolution, status, create_time, create_user_id, md5, ad_url, ad_content,submitter_id,create_org_id,verify_user_id,verify_time) "
			+ "values(#{adId}, #{adTypeId}, #{adName}, #{adSize}, #{adSecond}, #{fileType}, #{resolution}, #{status}, now(), #{createUserId}, #{md5}, #{adUrl}, #{adContent}, #{submitterId},#{createOrgId},#{verifyUserId},#{verifyTime})")
	int save(AdContent adContent);

	List<AdContentVO> AdContentList(PublicWhere params);
	
	@Select("select p.ad_type_name id,t.ad_name name from ad_content t "
			+ "left join ad_type p on t.ad_type_id = p.ad_type_id where FIND_IN_SET(t.ad_id,#{adIds})")
	List<SelectVO> listVoByIds(String adIds);
	
	@Select("select p.ad_type_name id,t.ad_name name from ad_content t "
			+ "left join ad_type p on t.ad_type_id = p.ad_type_id where t.ad_id = #{adIds}")
	SelectVO listVoById(String adId);
	
	@Select("select t.* from ad_content t "
			+ "where t.md5 = #{md5} and t.create_org_id = #{createOrgId} and t.ad_name = #{adName} "
			+ "order by t.index_id desc limit 1")
	AdContent getByPara(@Param("md5") String md5,@Param("createOrgId") String createOrgId,@Param("adName") String adName);
	
	@Select("select t.ad_type_id id,t.ad_type_name name from ad_type t where status = '"+Constant.DATA_STATUS_TYPE_NORMAL+"' order by ad_type_id")
	List<SelectIdLongVO> listAdContentTypes();
}
