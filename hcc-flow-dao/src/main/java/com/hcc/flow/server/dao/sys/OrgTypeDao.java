package com.hcc.flow.server.dao.sys;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.hcc.flow.server.common.constant.Constant;
import com.hcc.flow.server.model.sys.OrgType;
import com.hcc.flow.server.where.sys.OrgTypeWhere;

@Mapper
public interface OrgTypeDao {

	@Select("select * from org_type t where status='" + Constant.DATA_STATUS_TYPE_NORMAL
			+ "' order by t.org_type_id desc")
	List<OrgType> listAll();

	@Select("select * from org_type t where t.status = #{status} order by t.org_type_id desc")
	List<OrgType> listByStausAll(String status);

	@Select("select * from org_type t where t.org_type_id = #{orgTypeId}")
	OrgType getById(Long orgTypeId);
	
	@Select("select t.org_type_id from org_type t where t.org_type_name = #{orgTypeName}")
	String getByName(String orgTypeName);

	@Delete("delete from org_type where org_type_id = #{orgTypeId}")
	int delete(String orgTypeId);

	int update(OrgType OrgType);

	@Options(useGeneratedKeys = true, keyProperty = "orgTypeId")
	@Insert("INSERT INTO `org_type` ( `org_type_name`, `status`) VALUES(#{orgTypeName}, '"
			+ Constant.DATA_STATUS_TYPE_NORMAL + "')")
	int save(OrgType OrgType);

	List<OrgType> OrgTypeList(OrgTypeWhere params);

	@Update("<script>" + "update org_type set status=#{status} where org_type_id in "
			+ "<foreach item='item' index='index' collection='ids' open='(' separator=',' close=')'>" + "#{item}"
			+ "</foreach>" + "</script>")
	int updateStatus(@Param("ids") List<String> ids, @Param("status") String status);
}
