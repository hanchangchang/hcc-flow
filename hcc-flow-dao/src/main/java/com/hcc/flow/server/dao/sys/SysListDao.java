package com.hcc.flow.server.dao.sys;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.hcc.flow.server.common.constant.Constant;
import com.hcc.flow.server.common.where.PublicWhere;
import com.hcc.flow.server.model.sys.SysList;
import com.hcc.flow.server.vo.sys.SysListVO;
/**
 * SysListDao
 * @author 韩长志
 *
 */
@Mapper
public interface SysListDao {

    @Select("select * from sys_list t where status='"+Constant.DATA_STATUS_TYPE_NORMAL+"' order by create_time desc")
	List<SysList> listAll();
    
    @Select("select t.sys_id id,t.sys_name name,t.sys_url url from sys_list t where status='"+Constant.DATA_STATUS_TYPE_NORMAL+"' order by sort")
	List<SysListVO> listSelectVOAll();

    @Select("select * from sys_list t where t.sys_list_id = #{id}")
    SysList getById(String id);

    @Delete("delete from sys_list where sys_list_id = #{id}")
    int delete(String id);

    int update(SysList sysList);
    
    @Options(useGeneratedKeys = true, keyProperty = "sysId")
    //@SelectKey(statement="select replace(UUID(),'-','')", keyProperty="id", before=true, statementType=StatementType.STATEMENT,resultType=String.class)
    @Insert("insert into sys_list(sys_id, sys_code, sys_name , sys_url, sort, status, create_time) values(#{sysId}, #{sysCode}, #{sysName}, #{sysUrl}, #{sort}, #{status}, #{createTime})")
    int save(SysList sysList);
    
    List<SysList> SysListList(PublicWhere params);
    
    /**
     * 有效状态
     */
    @Select("select `status` from sys_list where status!='"+Constant.DATA_STATUS_DELETE+"' GROUP BY `status`")
	List<String> listValidStatus();
	
	@Update("<script>"
    		+ "update sys_list set status=#{status} where sys_list_id in "
    		+ "<foreach item='item' index='index' collection='ids' open='(' separator=',' close=')'>"
            	+ "#{item}"
            + "</foreach>"
    		+ "</script>")
    int updateStatus(@Param("ids") List<String> ids,@Param("status") String status);
}
