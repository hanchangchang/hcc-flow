package com.hcc.flow.server.dao.sys;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.mapping.StatementType;

import com.hcc.flow.server.common.constant.Constant;
import com.hcc.flow.server.common.vo.SelectVO;
import com.hcc.flow.server.model.common.KeyIdName;
import com.hcc.flow.server.model.sys.Org;
import com.hcc.flow.server.vo.sys.OrgOneVO;
import com.hcc.flow.server.vo.sys.OrgVO;
import com.hcc.flow.server.where.sys.OrgWhere;

@Mapper
public interface OrgDao {

	@Select("select t.*,pt.org_name parent_name,u.user_name create_user_name from org t left join org pt on t.parent_id=pt.org_id left join sys_user u on t.create_user_id=u.user_id where t.org_id = #{id}")
	OrgOneVO getById(String id);
	
	@Select("select t.*,pt.org_name parent_name,u.user_name create_user_name from org t left join org pt on t.parent_id=pt.org_id left join sys_user u on t.create_user_id=u.user_id where t.index_id = #{indexId}")
	OrgOneVO getByIndexId(Long indexId);
	
	@Select("select t.* from org t where t.org_code = #{orgCode} and t.org_status!='" + Constant.DATA_STATUS_DELETE + "'")
	Org getByCode(String orgCode);
	
	@Select("select t.* from org t where t.org_code = #{orgName}")
	Org getByName(String orgName);

	@Select("select count(t.org_id) from org t where t.parent_id = #{id} and t.org_status!='" + Constant.DATA_STATUS_DELETE + "'")
	int childOrgCountByParendId(String id);
	
	@Select("select t.parent_id from org t where t.org_id = #{id} and t.org_status!='" + Constant.DATA_STATUS_DELETE + "'")
	String getParendIdById(String id);

	@Delete("delete from org where org_id = #{id}")
	int delete(String id);

	int update(Org org);

	@SelectKey(statement = "select replace(UUID(),'-','')", keyProperty = "orgId", before = true, statementType = StatementType.STATEMENT, resultType = String.class)
	@Insert("INSERT INTO org (org_id, org_code, org_type_id, org_trade_type_id, org_name, org_des, org_status, create_user_id, create_time,parent_id,level,create_org_id,resource,web_login_logo,web_logo,web_title) "
			+ "VALUES (#{orgId}, #{orgCode}, #{orgTypeId},#{orgTradeTypeId}, #{orgName}, #{orgDes}, '" + Constant.DATA_STATUS_TYPE_NORMAL
			+ "', #{createUserId}, #{createTime},#{parentId},#{level},#{createOrgId},#{resource},#{webLoginLogo},#{webLogo},#{webTitle})")
	int save(Org org);

	@Select("select * from org t where org_type_id = #{orgTypeId} and org_status='" + Constant.DATA_STATUS_TYPE_NORMAL
			+ "' order by t.create_time ")
	List<Org> listbyTypeId(Long orgTypeId);
	
	@Select("select * from org t where org_type_id != #{orgTypeId} and org_status='" + Constant.DATA_STATUS_TYPE_NORMAL
			+ "' order by t.create_time ")
	List<Org> listNotbyTypeId(Long orgTypeId);
	
	@Select("select t.org_id id,t.org_name name from org t where org_type_id = #{orgTypeId} and org_status='" + Constant.DATA_STATUS_TYPE_NORMAL
			+ "' order by t.create_time ")
	List<SelectVO> listSelectVObyTypeId(Long orgTypeId);
	
	@Select("select t.org_id id,t.org_name name from org t where org_type_id != #{orgTypeId} and org_status='" + Constant.DATA_STATUS_TYPE_NORMAL
			+ "' order by t.create_time ")
	List<SelectVO> listSelectVONotbyTypeId(Long orgTypeId);
	
	@Select("select t.org_id id,t.org_name name from org t where org_status='" + Constant.DATA_STATUS_TYPE_NORMAL + "' order by t.create_time ")
	List<SelectVO> listVoAll();
	
	@Select("select t.org_id id,t.org_name name "
			+ "from org t "
			+ "where FIND_IN_SET(#{orgTypeId},t.org_type_id) "
			+ "and org_status='" + Constant.DATA_STATUS_TYPE_NORMAL+ "' "
			+ "and (FIND_IN_SET(t.ORG_ID,#{childOrgIds}) or FIND_IN_SET(t.create_org_id,#{childOrgIds})) "
			+ "order by t.create_time")
	List<SelectVO> listSelectVObyTypeIdAndIds(@Param("orgTypeId") Long orgTypeId,@Param("childOrgIds") String childOrgIds);

	@Select("select t.org_id id,t.org_name name from org t where t.org_id = #{id}")
	SelectVO getSelectVoById(String id);
	
	@Select("select t.org_id orgId,t.org_name orgName from org t where org_type_id= #{orgTypeId} and org_status='"
			+ Constant.DATA_STATUS_TYPE_NORMAL + "' order by t.create_time ")
	List<Map<String, Object>> listMapByTypeId(Long orgTypeId);
	
	@Select("select t.org_id orgId,t.org_name orgName from org t where org_status='"
			+ Constant.DATA_STATUS_TYPE_NORMAL + "' and (FIND_IN_SET(t.org_id,#{childOrgIds}) or FIND_IN_SET(t.create_org_id,#{childOrgIds})) order by t.create_time ")
	List<Map<String, Object>> listMapByInOrgId(String childOrgIds);

	@Select("select * from org t where t.org_status='" + Constant.DATA_STATUS_TYPE_NORMAL + "' order by t.create_time ")
	List<Org> listAll();

	@Select("select t.org_id orgId,t.org_name orgName from org t where org_status='" + Constant.DATA_STATUS_TYPE_NORMAL
			+ "' order by t.create_time ")
	List<Map<String, Object>> listAllMap();

	@Select("select t.org_id orgId,t.org_name orgName from org t where org_name like concat('%', #{orgName}, '%') and org_status='"
			+ Constant.DATA_STATUS_TYPE_NORMAL + "' order by t.create_time ")
	List<Map<String, Object>> listByOrgName(String orgName);

	@Select("select org_type_id key_id,org_type_name key_name,'org_type' key_type from org_type t where status='"
			+ Constant.DATA_STATUS_TYPE_NORMAL + "' ")
	List<KeyIdName> getOrgType();

	@Select("select org_id key_id,org_name key_name,'org' key_type from org t where org_status='"
			+ Constant.DATA_STATUS_TYPE_NORMAL + "' and org_type_id=#{typeId} order by t.create_time")
	List<KeyIdName> getShortOrgByType(Long typeId);

	List<OrgVO> OrgList(OrgWhere log);

	@Update("<script>" + "update org set org_status=#{status} where org_id in "
			+ "<foreach item='item' index='index' collection='ids' open='(' separator=',' close=')'>" + "#{item}"
			+ "</foreach>" + "</script>")
	int updateStatus(@Param("ids") List<String> ids, @Param("status") String status);
	
	@Select("<script>select t.*,u.USER_NAME create_user_name,pt.org_name parent_name,"
			+ "(SELECT GROUP_CONCAT(p.org_type_name) from org_type p where FIND_IN_SET(p.org_type_id,t.org_type_id)) org_type_name  " 
			+ "from org t " 
			+ "left join org pt on t.parent_id=pt.org_id " 
			+ "LEFT JOIN sys_user u on t.create_user_id=u.USER_ID "
			+ "where t.org_status = '"+Constant.DATA_STATUS_TYPE_NORMAL+"' "
			+ "<if test=\"orgIds != null and orgIds != ''\">"
			+ "and FIND_IN_SET(t.org_id,#{orgIds}) "
			+ "</if>"
			+ "<if test=\"keys != null and keys != ''\">"
			+ "and t.org_name like concat('%', #{keys}, '%') "
			+ "</if>"
			+ "order by t.org_id != #{orderById},t.create_time</script>")
	List<OrgVO> allByOrgIds(@Param("orgIds") String orgIds,@Param("keys") String keys,@Param("orderById") String orderById);

	List<OrgVO> getOrgsByTypeOrLike(OrgWhere where);
	
	List<OrgVO> getOrgList(@Param("followOrgIds")List<String> followOrgIds);
	
	@Update("update org t LEFT JOIN sys_user u on t.create_user_id = u.USER_ID set t.create_org_id = u.ORG_ID where t.create_org_id is null")
	int updateCreateOrgIdByNull();
}
