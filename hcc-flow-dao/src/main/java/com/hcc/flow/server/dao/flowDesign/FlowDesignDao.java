package com.hcc.flow.server.dao.flowDesign;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.mapping.StatementType;

import com.hcc.flow.server.common.constant.Constant;
import com.hcc.flow.server.common.where.PublicWhere;
import com.hcc.flow.server.model.flowDesign.FlowDesign;
import com.hcc.flow.server.vo.flowDesign.FlowDesignJsonVO;
import com.hcc.flow.server.vo.flowDesign.FlowDesignVO;

/**
 * FlowDao
 * @author admin
 *
 */
@Mapper
public interface FlowDesignDao {

    @Select("select * from flow_design t where status='"+Constant.DATA_STATUS_TYPE_NORMAL+"' order by create_time desc")
	List<FlowDesign> listAll();

    @Select("select * from flow_design t where t.flow_id = #{id}")
    FlowDesign getById(String id);
    
    @Select("select t.flow_id,t.flow_code,t.flow_name,t.flow_type,t.flow_attribute,t.follow_org_id,t.flow_remarks,t.create_time,t.status,t.is_used,"
    		+ "o.org_name follow_org_name,u.USER_NAME create_user_name,ftt.task_type_name flow_type_name " 
    		+ "from flow_design t " 
    		+ "LEFT JOIN org o on t.follow_org_id=o.org_id " 
    		+ "LEFT JOIN sys_user u on t.create_user_id=u.USER_ID "
    		+ "LEFT JOIN flow_task_type ftt on t.flow_type=ftt.task_type_id " 
    		+ "where t.flow_id = #{id}")
    FlowDesignVO getBaseById(String id);
    
    @Select("select t.flow_id,t.flow_code,t.flow_name,t.flow_data,t.flow_type,t.flow_attribute,t.flow_remarks,"
    		+ "o.org_name follow_org_name,ftt.task_type_name flow_type_name " 
    		+ "from flow_design t " 
    		+ "LEFT JOIN org o on t.follow_org_id=o.org_id " 
    		+ "LEFT JOIN flow_task_type ftt on t.flow_type=ftt.task_type_id " 
    		+ "where t.flow_id = #{id}")
    FlowDesignJsonVO getJsonById(String id);
    
    @Select("select t.flow_id,t.flow_code,t.flow_name,t.flow_data,t1.flow_type,t1.flow_attribute,t.flow_remarks,"
    		+ "o.org_name follow_org_name,ftt.task_type_name flow_type_name "
    		+ "from flow_design_temp t " 
    		+ "LEFT JOIN flow_design t1 on t.flow_id = t1.flow_id " 
    		+ "LEFT JOIN org o on t1.follow_org_id=o.org_id "
    		+ "LEFT JOIN flow_task_type ftt on t1.flow_type=ftt.task_type_id " 
    		+ "where t.flow_temp_id = #{flowTempId}")
    FlowDesignJsonVO getTempJsonByTempId(String flowTempId);
    
    @Select("select * from flow_design t where t.flow_code = #{flowCode} and t.status != '"+Constant.DATA_STATUS_DELETE+"' limit 1")
    FlowDesign getByCode(String flowCode);
    
    @Select("select * from flow_design t where t.follow_org_id = #{followOrgId} and t.flow_type = #{flowType} and t.flow_attribute = #{flowAttribute} and t.status = #{status} order by t.create_time desc")
    List<FlowDesign> getByOrgIdAndStatusAndAttribute(@Param("followOrgId") String followOrgId,@Param("flowType") String flowType,@Param("status") String status,@Param("flowAttribute") String flowAttribute);
    
    @Select("select * from flow_design t where t.follow_org_id = #{followOrgId} and t.flow_type = #{flowType} and t.flow_attribute = #{flowAttribute} and t.status = #{status} order by t.create_time desc limit 1")
    FlowDesign getOneByOrgIdAndStatusAndAttribute(@Param("followOrgId") String followOrgId,@Param("flowType") String flowType,@Param("status") String status,@Param("flowAttribute") String flowAttribute);
    
    @Select("select * from flow_design t where t.flow_code = #{flowCode} and t.follow_org_id = #{followOrgId} and t.status != '"+Constant.DATA_STATUS_DELETE+"' limit 1")
    FlowDesign getByCodeAndFollowOrgId(@Param("flowCode") String flowCode,@Param("followOrgId") String followOrgId);

    @Delete("delete from flow_design where flow_id = #{id}")
    int delete(String id);

    int update(FlowDesign flowDesign);
    
    //@Options(useGeneratedKeys = true, keyProperty = "id")
    @SelectKey(statement="select replace(UUID(),'-','')", keyProperty="flowId", before=true, statementType=StatementType.STATEMENT,resultType=String.class)
    @Insert("insert into flow_design(flow_id, flow_code, flow_name, flow_type,flow_attribute, flow_data, follow_org_id,flow_remarks, create_time, create_user_id, status, is_used) values(#{flowId}, #{flowCode}, #{flowName}, #{flowType}, #{flowAttribute}, #{flowData}, #{followOrgId}, #{flowRemarks}, now(), #{createUserId}, #{status}, #{isUsed})")
    int save(FlowDesign flowDesign);
    
    List<FlowDesignVO> FlowList(PublicWhere params);
    
    /**
     * 有效状态
     */
    @Select("select `status` from flow_design where status!='"+Constant.DATA_STATUS_DELETE+"' GROUP BY `status`")
	List<String> listValidStatus();
	
	@Update("<script>"
    		+ "update flow_design set status=#{status} where flow_id in "
    		+ "<foreach item='item' index='index' collection='ids' open='(' separator=',' close=')'>"
            	+ "#{item}"
            + "</foreach>"
    		+ "</script>")
    int updateStatus(@Param("ids") List<String> ids,@Param("status") String status);
}
