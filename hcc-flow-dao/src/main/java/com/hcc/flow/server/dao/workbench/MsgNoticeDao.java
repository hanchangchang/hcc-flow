package com.hcc.flow.server.dao.workbench;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.mapping.StatementType;

import com.hcc.flow.server.common.constant.Constant;
import com.hcc.flow.server.model.workbench.MsgNotice;
import com.hcc.flow.server.vo.workbench.MsgNoticeReadVO;
import com.hcc.flow.server.vo.workbench.MsgNoticeVO;
import com.hcc.flow.server.where.workbench.MsgNoticeWhere;

/**
 * MsgNoticeDao
 * @author 韩长志 20190604
 *
 */
@Mapper
public interface MsgNoticeDao {

    @Select("select * from msg_notice t where status='"+Constant.DATA_STATUS_TYPE_NORMAL+"' order by create_time desc")
	List<MsgNotice> listAll();

    @Select("select * from msg_notice t where t.notice_id = #{id}")
    MsgNotice getById(String id);
    
    @Select("<script>"
    		+ "select * from msg_notice t "
    		+ "where t.type = '1' "
    		+ "and t.flow_from_id = #{flowFromId} "
			+ "<if test=\"flowSubmitNo != null and flowSubmitNo != ''\">"
			+ "and t.flow_submit_no = #{flowSubmitNo} "
			+ "</if>"
			+ "<if test=\"flowTempId != null and flowTempId != ''\">"
			+ "and t.flow_temp_id = #{flowTempId} "
			+ "</if>"
			+ "<if test=\"flowTempNodeId != null and flowTempNodeId != ''\">"
    		+ "and t.flow_temp_node_id = #{flowTempNodeId} "
    		+ "</if>"
    		+ "order by t.push_time desc limit 1"
    		+ "</script>")
    MsgNotice getMaxNewByFlowFromId(@Param("flowFromId") String flowFromId,@Param("flowSubmitNo") String flowSubmitNo,@Param("flowTempId") String flowTempId,@Param("flowTempNodeId") String flowTempNodeId);
    
    @Select("<script>"
    		+ "select t.notice_id,r.status,r.flow_verify_status,r.flow_verify_reason,r.read_time,t.title,t.push_time,"
    		+ "CONCAT(u.user_name,'(',o.org_name,')') push_user_name,t.flow_temp_node_id,"
    		+ "CONCAT(ru.USER_NAME,'(',ro.org_name,')') read_user_name,ftt.task_type_name  "
    		+ "from msg_notice t "
    		+ "LEFT JOIN msg_notice_read r on t.notice_id = r.notice_id "
    		+ "<if test=\"isRead == true \">"
    		+ "and r.read_time is not null "
    		+ "</if>"
    		+ "LEFT JOIN sys_user u on t.push_user_id=u.user_id "
    		+ "LEFT JOIN org o on u.ORG_ID=o.org_id "
    		+ "LEFT JOIN sys_user ru on r.read_user_id=ru.user_id " 
    		+ "LEFT JOIN org ro on ru.ORG_ID=ro.org_id "
    		+ "LEFT JOIN flow_task_type ftt on t.task_type=ftt.task_type_id " 
    		+ "where t.type = '1' "
    		+ "<if test=\"flowFromId != null and flowFromId != ''\">"
    		+ "and t.flow_from_id = #{flowFromId} "
    		+ "</if>"
    		+ "<if test=\"flowSubmitNo != null and flowSubmitNo != ''\">"
    		+ "and t.flow_submit_no = #{flowSubmitNo} "
    		+ "</if>"
    		+ "<if test=\"flowTempId != null and flowTempId != ''\">"
    		+ "and t.flow_temp_id = #{flowTempId} "
    		+ "</if>"
    		+ "<if test=\"flowTempNodeId != null and flowTempNodeId != ''\">"
    		+ "and t.flow_temp_node_id = #{flowTempNodeId} "
    		+ "</if>"
    		+ "order by "
    		+ "<choose>"
    		+ "	<when test=\"torderBy == 0 \">"
    		+ "		t.push_time desc"
    		+ "	</when>"
    		+ "	<when test=\"torderBy == 1 \">"
    		+ "		r.read_time"
    		+ "	</when>"
    		+ "	<otherwise>"
    		+ "		r.read_time desc"
    		+ "	</otherwise>"
    		+ "</choose>"
    		+ "</script>")
    List<MsgNoticeReadVO> getNoticeReadVOByMore(@Param("flowFromId") String flowFromId,@Param("flowSubmitNo") String flowSubmitNo,@Param("flowTempId") String flowTempId,@Param("flowTempNodeId") String flowTempNodeId,@Param("isRead") Boolean isRead,@Param("torderBy") int torderBy);
    
    @Select("select #{readId} read_id,t.notice_id,t.title,t.content,t.push_time,t.show_begin_time,t.show_end_time,"
    		+ "CONCAT(u.user_name,'(',o.org_name,')') push_user_name,t.flow_from_id,t.type,t.task_type,ftt.task_type_name "
    		+ "from msg_notice t "
    		+ "left join sys_user u on t.push_user_id = u.user_id "
    		+ "left join org o on u.ORG_ID = o.org_id "
    		+ "LEFT JOIN flow_task_type ftt on t.task_type=ftt.task_type_id " 
    		+ "where t.notice_id =  #{noticeId}")
    MsgNoticeVO getVoById(@Param("noticeId") String noticeId,@Param("readId") String readId);

    @Delete("delete from msg_notice where notice_id = #{id}")
    int delete(String id);

    int update(MsgNotice msgNotice);
    
    //@Options(useGeneratedKeys = true, keyProperty = "id")
    @SelectKey(statement="select replace(UUID(),'-','')", keyProperty="noticeId", before=true, statementType=StatementType.STATEMENT,resultType=String.class)
    @Insert("insert into msg_notice(notice_id, type, title, content, task_type, href, push_user_id, push_time,show_begin_time,show_end_time, status,flow_temp_id,flow_temp_node_id,flow_from_id,flow_submit_no) values(#{noticeId}, #{type}, #{title}, #{content}, #{taskType}, #{href}, #{pushUserId}, now(),#{showBeginTime},#{showEndTime}, '"+Constant.DATA_STATUS_TYPE_NORMAL+"',#{flowTempId},#{flowTempNodeId},#{flowFromId},#{flowSubmitNo})")
    int save(MsgNotice msgNotice);
    
    List<MsgNotice> MsgNoticeList(MsgNoticeWhere params);
	
	@Update("<script>"
    		+ "update msg_notice set status=#{status} where notice_id in "
    		+ "<foreach item='item' index='index' collection='ids' open='(' separator=',' close=')'>"
            	+ "#{item}"
            + "</foreach>"
    		+ "</script>")
    int updateStatus(@Param("ids") List<String> ids,@Param("status") String status);
}
