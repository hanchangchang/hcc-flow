package com.hcc.flow.server.dao.sys;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.mapping.StatementType;

import com.hcc.flow.server.common.where.AuditLogWhere;
import com.hcc.flow.server.model.sys.SysAuditLogs;
import com.hcc.flow.server.model.sys.SysLogs;
import com.hcc.flow.server.where.sys.LogWhere;

@Mapper
public interface SysLogsDao {

	@SelectKey(statement = "select replace(UUID(),'-','')", keyProperty = "logId", before = true, statementType = StatementType.STATEMENT, resultType = String.class)
	@Insert("insert into sys_logs(log_id,user_id, log_type, module, flag, remark, create_time) values(#{logId}, #{userId}, #{logType}, #{module}, #{flag}, #{remark}, now())")
	int save(SysLogs sysLogs);

	@Delete("delete from sys_logs where create_time <= #{time}")
	int deleteLogs(String time);
	
	@Delete("delete from sys_logs where log_id = #{logId}")
	int delete(String logId);

	List<SysLogs> listLog(LogWhere log);

	List<SysLogs> LogList(LogWhere log);
	
	List<SysAuditLogs> LogAuditList(AuditLogWhere auditLogWhere);
	
	
	@Insert("insert into sys_audit_logs(user_id,op_con,op_time,module_id) values(#{userId},#{opCon},#{opTime},#{moduleId})")
	int saveAuditLogs(SysAuditLogs sysAuditLogs);
	
	@Select("select log.module_id log_id,log.op_time create_time,log.op_con module,su.user_name from sys_audit_logs log left join sys_user su on log.user_id=su.user_id where log.module_id=#{id} and op_con like concat('%',#{string},'%')")
	List<SysLogs> labelListLog(@Param("id")String id,@Param("string") String string);
}
