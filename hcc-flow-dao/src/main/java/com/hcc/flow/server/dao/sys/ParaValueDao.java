package com.hcc.flow.server.dao.sys;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.mapping.StatementType;

import com.hcc.flow.server.common.constant.Constant;
import com.hcc.flow.server.common.vo.SelectVO;
import com.hcc.flow.server.common.where.PublicWhere;
import com.hcc.flow.server.model.sys.ParaValue;
import com.hcc.flow.server.vo.sys.ParaValueDetailVO;
import com.hcc.flow.server.vo.sys.ParaValueVO;

@Mapper
public interface ParaValueDao {

	@Select("select t.*,p.para_type_name from para_value t LEFT JOIN para_type p on t.para_type_id=p.para_type_id order by t.para_type_id,t.order_no")
	List<ParaValue> findAll();

	@Select("select t.para_id,t.para_name,t.para_code paraCode,p.para_type_name paraTypeName from para_value t LEFT JOIN para_type p on t.para_type_id=p.para_type_id order by t.para_type_id,t.order_no")
	List<ParaValueVO> findAllMap();
	
	@Select("select t.para_id id,t.para_name name from para_value t LEFT JOIN para_type p on t.para_type_id=p.para_type_id  where p.para_type_code=#{paraTypeCode} order by t.order_no")
	List<SelectVO> listSelectVOByParaTypeCode(String paraTypeCode);
	
	@Select("select t.para_id id,t.para_name name from para_value t LEFT JOIN para_type p on t.para_type_id=p.para_type_id  where p.para_type_id=#{paraTypeId} order by t.order_no")
	List<SelectVO> listSelectVOByParaTypeId(Long paraTypeId);
	
	@Select("select count(t.para_id) from para_value t LEFT JOIN para_type p on t.para_type_id=p.para_type_id  where p.para_type_code=#{paraTypeCode}")
	int countByParaTypeCode(String paraTypeCode);

	@Select("select t.para_id,t.para_name,t.para_code paraCode,p.para_type_name paraTypeName from para_value t LEFT JOIN para_type p on t.para_type_id=p.para_type_id where t.para_type_id = #{paraTypeId} order by t.order_no")
	List<ParaValueVO> findByParaTypeId(Long paraTypeId);

	@Select("select t.para_id,t.para_name from para_value t LEFT JOIN para_type p on t.para_type_id = p.para_type_id where p.para_type_code = #{paraTypeCode} order by t.order_no")
	List<ParaValueVO> findByParaTypeCode(String paraTypeCode);
	
	@Select("select t.para_value from para_value t LEFT JOIN para_type p on t.para_type_id = p.para_type_id where p.para_type_code = #{paraTypeCode} and t.para_code=#{paraCode}")
	String getParaValueByTypeCodeAndCode(@Param("paraTypeCode") String paraTypeCode,@Param("paraCode") String paraCode);
	
	@Select("select t.para_name para_id,t.para_name from para_value t LEFT JOIN para_type p on t.para_type_id = p.para_type_id where p.para_type_code = #{paraTypeCode} order by t.order_no")
	List<ParaValueVO> findIdIsNameByParaTypeCode(String paraTypeCode);
	
	@Select("select t.* from para_value t LEFT JOIN para_type p on t.para_type_id=p.para_type_id  where p.para_type_code = #{paraTypeCode} order by t.order_no")
	List<ParaValue> findDataByParaTypeCode(String paraTypeCode);
	
	@Select("select t.para_name id,t.para_name name from para_value t LEFT JOIN para_type p on t.para_type_id=p.para_type_id  where p.para_type_code = #{paraTypeCode} order by t.order_no")
	List<SelectVO> findVoByParaTypeCode(String paraTypeCode);
	
	@Select("select t.*,s.USER_NAME create_user_name,sn.USER_NAME update_user_name,pt.para_type_name from para_value t "
			+ "LEFT JOIN sys_user s on t.create_user_id = s.USER_ID "
			+ "LEFT JOIN sys_user sn on t.update_user_id = sn.USER_ID "
			+ "LEFT JOIN para_type pt on t.para_type_id=pt.para_type_id where t.para_id = #{id}")
	ParaValueDetailVO getById(String id);
	
	@Select("select t.para_id id,t.para_name name from para_value t where t.para_id = #{id}")
	SelectVO getSelectVoById(String id);
	
	@Select("select t.* from para_value t where t.para_code = #{paraCode}")
	ParaValue getByCode(String paraCode);
	
	@Select("select t.para_id from para_value t where t.para_code = #{paraCode} and t.para_type_id = #{paraTypeId}")
	String getIdByCodeAnd(@Param("paraCode") String paraCode,@Param("paraTypeId") Long paraTypeId);

	@Delete("delete from para_value where para_id = #{id}")
	int delete(String id);

	int update(ParaValue para_value);

	@SelectKey(statement = "select replace(UUID(),'-','')", keyProperty = "paraId", before = true, statementType = StatementType.STATEMENT, resultType = String.class)
	@Insert("INSERT INTO `para_value` (`para_id`, `para_type_id`, `para_code`, `para_name`, `create_time`, `create_user_id`, `update_time`, `update_user_id`, `is_del`, `is_update`, `order_no`) "
			+ "VALUES (#{paraId}, #{paraTypeId}, #{paraCode}, #{paraName}, now(), #{createUserId}, now(), #{updateUserId}, #{isDel}, #{isUpdate}, #{orderNo})")
	int save(ParaValue oparaValue);
	
	List<ParaValueDetailVO> ParaValueList(PublicWhere params);
	
	@Select("select t.para_id from para_value t where t.para_type_id = #{paraTypeId} and t.para_name = #{paraName}")
    String getParaIdIdByParaTypeIdAndParaName(@Param("paraTypeId") Long paraTypeId,@Param("paraName") String paraName);
	
	@Select("select para_value from sys_para where para_code='"+Constant.WORK_PREFIX_CODE+"'")
	String getWorkPrefix();
}
