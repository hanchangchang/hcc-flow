package com.hcc.flow.server.dao.flowDesign;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.hcc.flow.server.common.constant.Constant;
import com.hcc.flow.server.common.vo.SelectIdLongVO;
import com.hcc.flow.server.common.where.PublicWhere;
import com.hcc.flow.server.model.flowDesign.FlowTaskType;
import com.hcc.flow.server.vo.flowDesign.FlowTaskTypeVO;
/**
 * FlowTaskTypeDao
 * @author 韩长志
 *
 */
@Mapper
public interface FlowTaskTypeDao {

    @Select("select * from flow_task_type t where status='"+Constant.DATA_STATUS_TYPE_NORMAL+"' order by create_time desc")
	List<FlowTaskType> listAll();
    
    @Select("select t.task_type_id id,t.task_type_name name from flow_task_type t where status='"+Constant.DATA_STATUS_TYPE_NORMAL+"' order by create_time")
	List<SelectIdLongVO> listAllVo();

    @Select("select * from flow_task_type t where t.task_type_id = #{id}")
    FlowTaskType getById(String id);
    
    @Select("select * from flow_task_type t where t.model = #{model} order by task_type_id limit 1")
    FlowTaskType getByModel(String model);

    @Delete("delete from flow_task_type where task_type_id = #{id}")
    int delete(String id);

    int update(FlowTaskType flowTaskType);
    
    @Options(useGeneratedKeys = true, keyProperty = "taskTypeId")
    //@SelectKey(statement="select replace(UUID(),'-','')", keyProperty="taskTypeId", before=true, statementType=StatementType.STATEMENT,resultType=String.class)
    @Insert("insert into flow_task_type(task_type_id, task_type_name, href, model, status, create_time) values(#{taskTypeId}, #{taskTypeName}, #{href}, #{model}, #{status}, #{createTime})")
    int save(FlowTaskType flowTaskType);
    
    List<FlowTaskTypeVO> FlowTaskTypeList(PublicWhere params);
	
	@Update("<script>"
    		+ "update flow_task_type set status=#{status} where task_type_id in "
    		+ "<foreach item='item' index='index' collection='ids' open='(' separator=',' close=')'>"
            	+ "#{item}"
            + "</foreach>"
    		+ "</script>")
    int updateStatus(@Param("ids") List<String> ids,@Param("status") String status);
}
