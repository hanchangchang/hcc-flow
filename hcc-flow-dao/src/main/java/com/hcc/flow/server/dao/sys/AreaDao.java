package com.hcc.flow.server.dao.sys;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.hcc.flow.server.model.sys.Area;
import com.hcc.flow.server.vo.sys.AreaVO;

@Mapper
public interface AreaDao {

	@Select("select * from area t order by t.sort")
	List<Area> listAll();

	@Select("select * from area t where t.areaType = 0 and t.show = 0 order by t.sort")
	List<Area> listParents();

	@Select("select area_name from area t where t.area_id = #{id}")
	String getAreaNameById(Long id);

	@Select("select * from area t where t.parent_id = #{parentId} and t.show = 0 order by t.sort")
	List<Area> listByParentId(Long parentId);

	@Select("select * from area t where t.area_id = #{id}")
	Area getById(Long id);
	
	/*@Select("select count(t.area_id) from area t where t.area_type = #{areaType}")
	int countByType(Integer areaType);*/

	@Options(useGeneratedKeys = true, keyProperty = "areaId")
	@Insert("INSERT INTO `area` (`area_id`, `parent_id`, `area_name`, `area_type`,sort,show) VALUES (#{areaId}, #{parentId}, #{areaName}, #{areaType},#{sort},#{show})")
	int save(Area area);

	@Update("update area t set parent_id = #{parentId}, area_name = #{areaName}, area_type = #{areaType},sort=#{sort},show=#{show} where t.area_id = #{areaId}")
	int update(Area area);

	@Delete("delete from area where area_id = #{areaId}")
	int delete(Long areaId);

	@Delete("delete from area where parent_id = #{areaId}")
	int deleteByParentId(Long areaId);
	
	@Select("select t.area_id,t.area_name,t.parent_id,t.area_type,t.sort,b.area_name parent_name from area t LEFT JOIN area b on t.parent_id=b.area_id where t.area_type = #{areaType} and t.show = 0 order by t.sort")
	List<AreaVO> listValidAddrAreaByAreaType(Integer areaType);
	
	@Select("select t.area_id,t.area_name,t.parent_id,t.area_type,t.sort,b.area_name parent_name from area t LEFT JOIN area b on t.parent_id=b.area_id where FIND_IN_SET(t.area_id,#{ids}) and t.show = 0 order by t.sort")
	List<AreaVO> listValidAddrAreaByIds(String ids);
	
	@Select("select group_concat(DISTINCT t.parent_id) from area t where FIND_IN_SET(t.area_id,#{ids}) and t.show = 0 order by t.sort")
	String getParentIdsById(String ids);
	
	@Select("select * from area t where FIND_IN_SET(t.area_id,#{ids}) and t.show = 0 order by t.sort")
	List<Area> getByIds(String ids);
	
	@Select("select group_concat(t.area_id) from area t where FIND_IN_SET(t.parent_id,#{ids}) and t.show = 0 order by t.sort")
	String getIdsByParentIds(String ids);
	
	@Select("select * from area t where FIND_IN_SET(t.parent_id,#{ids}) and t.show = 0 order by t.sort")
	List<Area> getByParentIds(String ids);
	
	@Select("SELECT GROUP_CONCAT(area_name separator '|') areas from area where area_type in (1,2)")
	String getNamesByType12();
}
