package com.hcc.flow.server.dao.sys;

import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.mapping.StatementType;

import com.hcc.flow.server.model.sys.Permission;
import com.hcc.flow.server.vo.sys.PermissionChildVO;
import com.hcc.flow.server.vo.sys.PermissionTopVO;

@Mapper
public interface PermissionDao {

	@Select("select * from sys_permission t order by t.sort")
	List<Permission> listAll();
	
	@Select("select p.PERMISSION from sys_role_user t "
			+ "LEFT JOIN sys_role_permission rp on t.ROLE_ID = rp.ROLE_ID "
			+ "LEFT JOIN sys_permission p on rp.PERMISSION_ID = p.PERMISSION_ID "
			+ "where t.USER_ID = #{userId} and p.PARENT_ID = (select sp.PERMISSION_ID from sys_permission sp where sp.PERMISSION=#{permission} limit 1) "
			+ "GROUP BY p.PERMISSION_ID ")
	List<String> childPermissionByUserIdAndPermission(@Param("userId") String userId,@Param("permission") String permission);

	@Select("select * from sys_permission t where t.type = 1 order by t.sort")
	List<Permission> listParents();

	@Select("select * from sys_permission t where t.type != 4 order by t.sort")
	List<Permission> listParentsNotBtn();

	@Select("select distinct p.* from sys_permission p inner join sys_role_permission rp on p.permission_id = rp.permission_id inner join sys_role_user ru on ru.role_id = rp.role_id where ru.user_id = #{userId} order by p.sort")
	List<Permission> listByUserId(String userId);
	
	
	@Select("<script>"
			+ "select distinct p.* from sys_permission p "
			+ "inner join sys_role_permission rp on p.permission_id = rp.permission_id "
			+ "inner join sys_role_user ru on ru.role_id = rp.role_id "
			+ "where ru.user_id = #{userId} and p.parent_id = '0' "
			+ "<if test=\"sysId != null\">"
			+ " and sys_id = #{sysId} "
			+ "</if>"
			+ "order by p.sort"
			+ "</script>")
	List<PermissionTopVO> listTopByUserId(@Param("userId") String userId,@Param("sysId") String sysId);
	
	@Select("<script>select distinct p.* from sys_permission p "
			+ "inner join sys_role_permission rp on p.permission_id = rp.permission_id "
			+ "inner join sys_role_user ru on ru.role_id = rp.role_id "
			+ "where ru.user_id = #{userId} "
			+ "<if test=\"sysId != null\">"
			+ " and p.sys_id = #{sysId} "
			+ "</if>"
			+ "<if test=\"notPermissionId != null and notPermissionId != ''\">"
			+ " and p.permission_id != #{notPermissionId} "
			+ "</if>"
			+ "and p.type=1 "
			+ "order by p.sort"
			+ "</script>")
	List<PermissionChildVO> listCAllByUserId(@Param("userId") String userId,@Param("sysId") Integer sysId,@Param("notPermissionId") String notPermissionId);
	
	@Select("<script>"
			+ "select p.* from sys_permission p "
			+ "where p.parent_id = '0' "
			+ "<if test=\"sysId != null\">"
			+ " and sys_id = #{sysId} "
			+ "</if>"
			+ "order by p.sort"
			+ "</script>")
	List<PermissionTopVO> listTop(@Param("sysId") String sysId);
	
	@Select("<script>"
			+ "select p.* from sys_permission p "
			+ "<where>"
			+ "<if test=\"sysId != null\">"
			+ " and sys_id = #{sysId} "
			+ "</if>"
			+ "<if test=\"notPermissionId != null and notPermissionId != ''\">"
			+ " and permission_id != #{notPermissionId} "
			+ "</if>"
			+ "and type = 1 "
			+ "</where>"
			+ "order by p.sort"
			+ "</script>")
	List<PermissionChildVO> listCAll(@Param("sysId") Integer sysId,@Param("notPermissionId") String notPermissionId);

	@Select("select p.* from sys_permission p inner join sys_role_permission rp on p.permission_id = rp.permission_id where rp.role_id = #{roleId} order by p.sort")
	List<Permission> listByRoleId(String roleId);

	@Select("select * from sys_permission t where t.permission_id = #{id}")
	Permission getById(String id);

	@SelectKey(statement = "select replace(UUID(),'-','')", keyProperty = "permissionId", before = true, statementType = StatementType.STATEMENT, resultType = String.class)
	@Insert("insert into sys_permission(permission_id,parent_Id, permission_name, css, href, type, permission, sort,create_time,create_user_id,update_time,update_user_id,level,sys_id) values(#{permissionId},#{parentId}, #{permissionName}, #{css}, #{href}, #{type}, #{permission}, #{sort},now(),#{createUserId},now(),#{updateUserId},#{level},#{sysId})")
	int save(Permission permission);

	@Update("<script>"
			+ "update sys_permission t "
			+ "<set> "
			+ "<if test=\"parentId != null and parentId != ''\">"
				+ "parent_id = #{parentId}, "
			+ "</if>"
			+ "<if test=\"permissionName != null and permissionName != ''\">"
				+ "permission_name = #{permissionName},"
			+ "</if>"
			+ "<if test=\"css != null and css != ''\">"
				+ "css = #{css},"
			+ "</if>"
			+ "<if test=\"href != null and href != ''\">"
				+ "href = #{href},"
			+ "</if>"
			+ "<if test=\"type != null\">"
				+ "type = #{type},"
			+ "</if>"
			+ "<if test=\"permission != null and permission != ''\">"
				+ "permission = #{permission},"
			+ "</if>"
			+ "<if test=\"sort != null\">"
				+ "sort = #{sort},"
			+ "</if>"
			+ "<if test=\"level != null\">"
				+ "level=#{level},"
			+ "</if>"
			+ "update_time=now(),"
			+ "<if test=\"updateUserId != null and updateUserId != ''\">"
				+ "update_user_id= #{updateUserId},"
			+ "</if>"
			+ "<if test=\"sysId != null\">"
				+ "sys_id=#{sysId}"
			+ "</if>"
			+ "</set>"
			+ " where t.permission_id = #{permissionId}"
			+ "</script>")
	int update(Permission permission);

	@Delete("delete from sys_permission where permission_id = #{permissionId}")
	int delete(String permissionId);

	@Delete("delete from sys_permission where parent_id = #{permissionId}")
	int deleteByParentId(String permissionId);

	@Delete("delete from sys_role_permission where permission_id = #{permissionId}")
	int deleteRolePermission(String permissionId);

	@Select("select ru.userId from sys_role_permission rp inner join sys_role_user ru on ru.role_id = rp.role_id where rp.permission_id = #{permissionId}")
	Set<Long> listUserIds(String permissionId);

	@Select("select rp.PERMISSION_ID from sys_role_permission rp where role_id = #{roldId}")
	List<String> selectPermissionIdsByRoleId(String roldId);
	
	@Select("select distinct p.permission "
			+ "from sys_role_user ru "
			+ "inner join sys_role_permission rp on ru.role_id = rp.role_id "
			+ "inner join sys_permission p on p.permission_id = rp.permission_id and p.sys_id = #{sysId} and p.type = 2 "
			+ "where ru.user_id = #{userId} order by p.sort")
	Set<String> listPermissionCodeByUserIdAndSysId(@Param("userId") String userId,@Param("sysId") Integer sysId);
	
	@Select("select p.permission from sys_permission p where p.sys_id = #{sysId} and type = 2")
	Set<String> listPermissionCodeBySysId(Integer sysId);
}
