package com.hcc.flow.server.dao.sys;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.mapping.StatementType;

import com.hcc.flow.server.common.constant.Constant;
import com.hcc.flow.server.common.vo.SelectCascVO;
import com.hcc.flow.server.model.sys.Role;
import com.hcc.flow.server.vo.sys.RoleOneVO;
import com.hcc.flow.server.vo.sys.RoleVO;
import com.hcc.flow.server.where.sys.RoleWhere;

@Mapper
public interface RoleDao {

	@SelectKey(statement = "select replace(UUID(),'-','')", keyProperty = "roleId", before = true, statementType = StatementType.STATEMENT, resultType = String.class)
	@Insert("insert into sys_role(role_id, role_name,org_id,status, description, create_time,create_user_id, update_time,update_user_id,parent_id,level,create_org_id) values(#{roleId},#{roleName}, #{orgId},'"
			+ Constant.USE_STATUS_TYPE_NOT_USED + "', #{description}, now(),#{createUserId}, null,null,#{parentId},#{level},#{createOrgId})")
	int save(Role role);

	@Select("select t.*,o.org_name,o.org_type_id,pt.role_name parent_name,"
			+ "(SELECT GROUP_CONCAT(p.org_type_name) from org_type p where FIND_IN_SET(p.org_type_id,o.org_type_id)) org_type_name "
			+ "from sys_role t "
			+ "LEFT JOIN org o on t.ORG_ID=o.org_id "
			+ "LEFT JOIN sys_role pt on t.parent_id=pt.role_id "
			+ "where t.role_id = #{id}")
	RoleOneVO getById(String id);
	
	@Select("select GROUP_CONCAT(t.ROLE_ID) from sys_role_user t where t.USER_ID = #{userId}")
	String getRoleIdsByUserId(String userId);
	
	@Select("select t.ROLE_ID from sys_role_user t where t.USER_ID = #{userId}")
	List<String> getListRoleIdsByUserId(String userId);
	
	@Select("select t.parent_id from sys_role t where FIND_IN_SET(t.ROLE_ID,#{roleIds})")
	List<String> getListParentIdsByRoleIds(String roleIds);
	
	@Select("select MIN(r.level) from sys_role_user t left join sys_role r on t.ROLE_ID = r.ROLE_ID where t.USER_ID = #{userId}")
	int getMinLevelByRoleIds(String userId);
	
	@Select("select t.* from sys_role t where t.role_id = #{roleId}")
	Role getByRoleId(String roleId);
	
	@Select("select t.ROLE_ID from sys_role_user t where t.USER_ID = #{userId} and t.ROLE_ID = '"+Constant.SYSTEM_MANAGE_ROLE_ID+"'")
	String getRoleIdByUserIdAndManage(String userId);
	
	@Select("select * from sys_role t where t.status!='" + Constant.USE_STATUS_DELETE + "' and t.org_id = #{orgId}")
	List<Role> getRolesByOrgId(String orgId);
	
	@Select("select t.role_id id,t.role_name name,t.org_id parent_id,o.org_name parent_name from sys_role t left join org o on t.org_id = o.org_id where t.status!='" + Constant.USE_STATUS_DELETE + "' and FIND_IN_SET(t.org_id,#{orgIds})")
	List<SelectCascVO> getRolesByOrgIds(String orgIds);
	
	@Select("select count(t.role_id) from sys_role t where t.status!='" + Constant.USE_STATUS_DELETE + "' and t.parent_id = #{parentId}")
	int countByParendId(String parentId);
	
	@Select("select count(t.ROLE_USER_ID) from sys_role_user t where t.role_id = #{roleId}")
	int countUserByRoleId(String roleId);
	
	@Select("select count(t.ROLE_PERMISSION_ID) from sys_role_permission t where t.role_id = #{roleId}")
	int countPermissionByRoleId(String roleId);
	
	@Select("select count(t.role_id) from sys_role t where t.status!='" + Constant.USE_STATUS_DELETE + "' and t.org_id = #{orgId}")
	int countByOrgId(String orgId);

	@Select("select * from sys_role t where t.role_name = #{roleName}")
	Role getRole(String name);

	int update(Role role);

	@Select("select * from sys_role t where t.status!='" + Constant.USE_STATUS_DELETE + "'")
	List<Role> listAll();

	@Select("select * from sys_role r inner join sys_role_user ru on r.role_id = ru.role_id where ru.user_id = #{userId} and r.status!='"
			+ Constant.USE_STATUS_DELETE + "'")
	List<Role> listByUserId(String userId);

	@Select("select r.role_id from sys_role r inner join sys_role_user ru on r.role_id = ru.role_id where ru.user_id = #{userId} and r.status!='"
			+ Constant.USE_STATUS_DELETE + "'")
	List<String> listIdsByUserId(String userId);

	@Delete("delete from sys_role_permission where role_id = #{roleId}")
	int deleteRolePermission(String roleId);

	int saveRolePermission(@Param("roleId") String roleId, @Param("permissionIds") List<String> permissionIds);

	@Delete("delete from sys_role where role_id = #{roleId}")
	int delete(String id);

	@Delete("delete from sys_role_user where role_id = #{roleId}")
	int deleteRoleUser(String roleId);

	List<RoleVO> RoleList(RoleWhere log);

	@Update("<script>" + "update sys_role set status=#{status} where role_id in "
			+ "<foreach item='item' index='index' collection='ids' open='(' separator=',' close=')'>" + "#{item}"
			+ "</foreach>" + "</script>")
	int updateStatus(@Param("ids") List<String> ids, @Param("status") String status);
}
