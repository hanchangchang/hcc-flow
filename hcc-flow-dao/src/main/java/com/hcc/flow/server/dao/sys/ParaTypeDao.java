package com.hcc.flow.server.dao.sys;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import com.hcc.flow.server.common.where.PublicWhere;
import com.hcc.flow.server.model.sys.ParaType;
import com.hcc.flow.server.vo.sys.ParaTypeVO;

@Mapper
public interface ParaTypeDao {

	@Select("select * from para_type t order by t.para_type_id desc")
	List<ParaType> listAll();

	@Select("select t.*,s.USER_NAME create_user_name from para_type t "
			+ "LEFT JOIN sys_user s on t.create_user_id = s.USER_ID where t.para_type_id = #{paraTypeId}")
	ParaTypeVO getById(Long paraTypeId);

	@Select("select * from para_type t where t.para_type_code = #{paraTypeCode}")
	ParaType getByParaTypeCode(String paraTypeCode);

	@Delete("delete from para_type where para_type_id = #{paraTypeId}")
	int delete(String paraTypeId);

	int update(ParaType ParaType);

	@Options(useGeneratedKeys = true, keyProperty = "paraTypeId")
	@Insert("INSERT INTO `para_type` (`para_type_code`,`para_type_name`,`create_time`,`create_user_id`) "
			+ "VALUES (#{paraTypeCode},#{paraTypeName},now(),#{createUserId})")
	int save(ParaType ParaType);
	
	List<ParaTypeVO> ParaTypeList(PublicWhere log);
}
