var urlRootPrefix = "";
//页面当前主域名
var localUrl = window.location.host;
//检查域名-本地环境
if (localUrl.indexOf("localhost") != -1 || localUrl.indexOf("127.0.0.1") != -1 || localUrl.indexOf("192.168.") != -1) {
	urlRootPrefix = "";
} else {//检查域名-外网环境(配置了nginx前缀)
	var hccprefix = "/hcc-flow";
	if(window.location.pathname.indexOf(hccprefix) != -1){
		urlRootPrefix = hccprefix;
	}
}
$.ajaxSetup({
	cache : false,
	headers : {
		"token" : localStorage.getItem(urlRootPrefix.replace("/","") + "token")
	},
	error : function(xhr, textStatus, errorThrown) {
		//debugger;
		//alert(xhr);
		var msg = xhr.responseText;
		var response = JSON.parse(msg);
		var code = response.code;
		var message = response.message;
		if (code == 400) {
			layer.msg(message);
		} else if (code == 401) {
			localStorage.removeItem(urlRootPrefix.replace("/","") + "token");
			location.href = urlRootPrefix + '/login.html';
		} else if (code == 403) {
			console.log("未授权:" + message);
			layer.msg('未授权');
		} else if (code == 500) {
			layer.msg('系统错误：' + message);
		}
	}
});
//form序列化为json
$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
//-------------列表按钮拓展-开始----------------
//列表操作-删除按钮
function buttonDel(data, permission, pers){
	if(permission != ""){
		if ($.inArray(permission, pers) < 0) {
			return "";
		}
	}
	var btn = $("<button class='layui-btn layui-btn-xs' title='删除' onclick='del(\"" + data +"\")'><i class='layui-icon'>&#xe640;</i></button>");
	return btn.prop("outerHTML");
}
//列表操作-修改按钮
function buttonEdit(href, permission, pers){
	if(permission != ""){
		if ($.inArray(permission, pers) < 0) {
			return "";
		}
	}	
	var btn = $("<button class='layui-btn layui-btn-xs' title='编辑' onclick='window.location=\"" + href +"\"'><i class='layui-icon'>&#xe642;</i></button>");
	return btn.prop("outerHTML");
}
//列表操作-提交审核按钮
function buttonSub(data, permission, pers){
	if(permission != ""){
		if ($.inArray(permission, pers) < 0) {
			return "";
		}
	}
	var btn = $("<button class='layui-btn layui-btn-xs' title='提交审核' onclick='subId(\"" + data +"\")'><i class='layui-icon'>&#xe605;</i></button>");
	return btn.prop("outerHTML");
}
//列表操作-查看文件
function buttonSelect(href){
	var btn = $("<button class='layui-btn layui-btn-xs' title='预览文件' onclick='showFiles(\""+ href + "\")'><i class='layui-icon'>&#xe61d;</i></button>");
	return btn.prop("outerHTML");
}
//列表操作-查看详情（直接跳转地址）
function buttonShowHref(href){
	var btn = $("<button class='layui-btn layui-btn-xs' title='查看详情' onclick='window.location=\""+ href + "\"'><i class='layui-icon'>&#xe615;</i></button>");
	return btn.prop("outerHTML");
}
//列表操作-查看详情（直接跳转地址）
function buttonSubStatus(data, oldStatus ,permission, pers){
	var btn;
	if(oldStatus == "C"){
		btn = $("<button class='layui-btn layui-btn-xs' title='点击停用' onclick='subStatus(\"" + data +"\",\"" + oldStatus +"\")'><i class='layui-icon'>&#xe64d;</i></button>"); 
    }else {
    	btn = $("<button class='layui-btn layui-btn-xs' title='点击启用' onclick='subStatus(\"" + data +"\",\"" + oldStatus +"\")'><i class='layui-icon'>&#xe605;</i></button>");
    }
	return btn.prop("outerHTML");
}
//列表操作-查看详情（弹窗）
function buttonShow(href, permission, pers, ico, title){
	if(permission != ""){
		if ($.inArray(permission, pers) < 0) {
			return "";
		}
	}
	if(!ico){
		ico = "&#xe65d;"; 
	}
	if(!title){
		title = "查看详情"; 
	}

	var btn = $("<button class='layui-btn layui-btn-xs' title='"+title+"' onclick='showMaxWindow(\"" + href +"\",\"" + title +"\")'><i class='layui-icon'>"+ico+"</i></button>");
	return btn.prop("outerHTML");
}
//-------------列表按钮拓展-结束----------------

function deleteCurrentTab(){
	var lay_id = $(parent.document).find("ul.layui-tab-title").children("li.layui-this").attr("lay-id");
	parent.active.tabDelete(lay_id);
}
//全屏弹窗
function showMaxWindow(url,title) {
	if(!title){
		title = "详情";
	}
	if(title == "设计流程"){
		title = "设计流程    <span style='color:#1AA094;padding-left:20px;'> [操作说明：鼠标单击画布左侧竖排的‘小图标’抓取节点，然后将鼠标移动到网格画布上单击鼠标放下该节点完成节点绘制]</span>";
	}
	var index =layer.open({
		type : 2, //Layer提供了5种层类型。可传入的值有：0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）,
		shade : 0.1, //遮罩层透明度
		maxmin: true,
		maxmin:false,//开启最大化最小化按钮  false关闭状态
		title : title,//弹出层标题
		content : url,//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
	
	});
	layer.full(index); 
}
//标准弹窗
function showWindow(url,title,width,height) {
	if(!title){
		title = "详情";
	}
	if(!width){
		width = "800px";
	}
	if(!height){
		height = "650px";
	}
	layer.open({
		type : 2, //Layer提供了5种层类型。可传入的值有：0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）,
		shade : 0.1, //遮罩层透明度
		maxmin: false,
		area: [width,height],
		maxmin:true,//开启最大化最小化按钮  false关闭状态
		title : title,//弹出层标题
		content : url,//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
	
	});

}
/**
 * 是否是json字符串
 * @param str
 * @returns
 */
function isJSON(str) {
    if (typeof str == 'string') {
        try {
            var obj=JSON.parse(str);
            if(typeof obj == 'object' && obj ){
                return true;
            }else{
                return false;
            }

        } catch(e) {
            console.log('error：'+str+'!!!'+e);
            return false;
        }
    }
    return true;
    console.log('It is not a string!')
}

/**
 * 取url参数值
 * @param name
 * @returns
 */
function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)return unescape(r[2]);
    return null;
};



//获取url后的参数值
function getUrlParam(key) {
	var href = window.location.href;
	var url = href.split("?");
	if(url.length <= 1){
		return "";
	}
	var params = url[1].split("&");
	
	for(var i=0; i<params.length; i++){
		var param = params[i].split("=");
		if(key == param[0]){
			return param[1];
		}
	}
}

/**判断是否是中文**/
function isChina(s){
    var patrn= /[\u4e00-\u9fa5]/gm;
    if (!patrn.exec(s)){
        return false ;
    } else {
        return true ;
    }
}

/**判断是否是手机号**/
function isPhoneNumber(tel) {
    var reg =/^0?1[3|4|5|6|7|8][0-9]\d{8}$/;
    return reg.test(tel);
}

/**判断是否是手机号**/
function isIdCard(card) {
    var reg = /^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/;
    return reg.test(card);
}

/**
 * 值是否不为空
 * @param value
 * @returns
 */
function isNotBlank(value) {
	if(value === "" || value == null){
        return false;
    }
	if(!isNaN(value)){
        return true;
	}else{
		if (value.toString().replace(/(^\s*)|(\s*$)/g, "") ) { // 去除空格在判断
            return true;
        } else {
            return false;
        }
	}
    return false;
}