function initRoles(orgid) {
	if(!orgid){
		return;
	}
	$.ajax({
		type : 'get',
		url : urlRootPrefix + '/roles/org/'+orgid,
		async : false,
		success : function(data) {
			var r = $("#roles");
			var data = data.data;
			for (var i = 0; i < data.length; i++) {
				var d = data[i];
				var id = d['roleId'];
				var name = d['roleName'];

				var t = "<label><input type='checkbox' value='" + id + "'>"
						+ name + "</label> &nbsp&nbsp";

				r.append(t);
			}
		}
	});
}

function getCheckedRoleIds() {
	var ids = [];
	$("#roles input[type='checkbox']").each(function() {
		if ($(this).prop("checked")) {
			ids.push($(this).val());
		}
	});

	return ids;
}

function initRoleDatas(userId) {
	$.ajax({
		type : 'get',
		url : urlRootPrefix + '/roles?userId=' + userId,
		success : function(data) {
			var data = data.data;
			var length = data.length;
			for (var i = 0; i < length; i++) {
				$("input[type='checkbox']").each(function() {
					var v = $(this).val();
					if (v == data[i]['roleId']) {
						$(this).attr("checked", true);
					}
				});
			}
		}
	});
}