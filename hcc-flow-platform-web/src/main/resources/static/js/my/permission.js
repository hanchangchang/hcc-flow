function checkPermission() {
	var pers = [];
	$.ajax({
		type : 'get',
		url : urlRootPrefix + '/permissions/owns',
		contentType : "application/json; charset=utf-8",
		async : false,
		success : function(data) {
			//console.log(data);
			pers = data.data;
			$("[permission]").each(function() {
				var per = $(this).attr("permission");
				if ($.inArray(per, pers) < 0) {
					$(this).hide();
				}
			});
		}
	});
	
	return pers;
}