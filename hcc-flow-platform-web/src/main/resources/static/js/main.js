initMenu();
function initMenu(){
	 //默认欢迎页加上前缀
	 $("#dashboardIframe").attr("src", urlRootPrefix + "/" + "pages/dashboard.html");
	 //顶部消息通知菜单链接加上前缀
	 $(".menuatag").each(function(index,item) {
	     $(this).attr("data-url", urlRootPrefix + "/" + $(this).attr("data-url"));
	 });
	 $.ajax({  
	     url:urlRootPrefix + "/permissions/current",  
	     type:"get",  
	     async:false,
	     success:function(data){
	    	 //console.log(data);

	    	 if(data.statusCode != '0'){
	    		 location.href= urlRootPrefix + '/login.html';
	    		 return;
	    	 }
	    		 
	    	 data = data.data;
	    	 var menu = $("#menu");
	    	 $.each(data, function(i,item){
	             var a = $("<a href='javascript:;'></a>");
	           
	             var css = item.css;
	             if(css!=null && css!=""){
	            	 a.append("<i aria-hidden='true' class='fa " + css +"'></i>");
	             }
	             a.append("<cite>"+item.permissionName+"</cite>");
	             a.attr("lay-id", item.permissionId);
	             
	             var href = item.href;
	             if(href != null && href != ""){
	                a.attr("data-url", urlRootPrefix + "/" + "/" + href);
	             }
	             
	             var li = $("<li class='layui-nav-item'></li>");
	             if (i == 0) {
	            	 li.addClass("layui-nav-itemed");
	             }
	             li.append(a);
	             //二级菜单
	             var child2 = item.child;
	             if(child2 != null && child2.length > 0){
	            	 $.each(child2, function(j,item2){
	            		 var ca = $("<a href='javascript:;'></a>");
	            		 if(item2.href != null && item2.href != ""){
	            			 ca.attr("data-url", urlRootPrefix + "/" + item2.href);
	            		 }
                         ca.attr("lay-id", item2.permissionId);
                         
                         var css2 = item2.css;
                         if(css2!=null && css2!=""){
                        	 ca.append("<i aria-hidden='true' class='fa " + css2 +"'></i>");
                         }
                         ca.append("<cite>"+item2.permissionName+"</cite>");
                         //三级菜单begin
                         var child3 = item2.child;
                         var dl3 ;
                         var sp3;
                         if(child3 != null && child3.length > 0){
                        	 dl3 = $("<dl class='layui-nav-child'></dl>");
                        	 //修改小箭头位置
                        	 sp3 = $("<span class='layui-nav-more' style='margin-right: 10px;'></span>");
        	            	 $.each(child3, function(z,item3){
        	            		 var ca3 = $("<a href='javascript:;'></a>");
        	            		 if(item3.href != null && item3.href != ""){
        	            			 ca3.attr("data-url", urlRootPrefix + item3.href);
        	            		 }
        	            		 ca3.attr("lay-id", item3.permissionId);
                                 
                                 var css3 = item3.css;
                                 if(css3!=null && css3!=""){
                                	 ca3.append("<i aria-hidden='true' class='fa " + css3 +"'></i>");
                                 }
                                 ca3.append("<cite>"+item3.permissionName+"</cite>");
                                 var dd3 = $("<dd></dd>");
                                 dd3.append(ca3);
                                 dl3.append(dd3);
        	            	 });
        	            }
                        ca.append(sp3);
                         var dd = $("<dd></dd>");
                         dd.append(ca);
                         dd.append(dl3);
                         var dl = $("<dl class='layui-nav-child'></dl>");
                         dl.append(dd);
                         li.append(dl);
	            	 });
	            }
	            menu.append(li);
	        });
	     }
	 });
}

// 登陆用户头像昵称
showLoginInfo();
function showLoginInfo(){
	$.ajax({
		type : 'get',
		url : urlRootPrefix + '/users/currentshort',
		async : false,
		success : function(data) {
			var data = data.data;
			$(".admin-header-user span").text(data.nickname);
			var sex = data.sex;
			var url = data.headImgUrl;
			if(url == null || url == ""){
				if(sex == 1){
					url = "/img/avatars/sunny.png";
				} else {
					url = "/img/avatars/1.png";
				}
				
				url = urlRootPrefix + url;
			} else {
				url = urlRootPrefix + "/statics" + url;
			}
			var img = $(".admin-header-user img");
			img.attr("src", url);
			$(".admin-login-info").append("<span>欢迎您 "+data.userAcct+" ["+data.orgName+"-"+data.roleName+"]</span>");
		}
	});
}

showUnreadNotice();
function showUnreadNotice(){
	$.ajax({
		type : 'get',
		url : urlRootPrefix + '/msgNoticeReads/index/show',
		async : false,
		success : function(data) {
			var data = data.data;
			var allCount = data.allCount;
			if(allCount > 0){
				$(".allMsgCount").show();
				if(data.allCount > 99){
					$(".allMsgCount span").text(allCount+"+");
				}else{
					$(".allMsgCount span").text(allCount);
				}
				var taskCount = data.taskCount.allCount;
				if(taskCount == 0){
					$(".taskMsgCount").hide();
				}else {
					var cite = $("#admin-side ul li dl.layui-nav-child a[lay-id='5c62e9e3588a81ea90cc00163e13e3me']").find('cite');
					if(!cite.has('span.taskMsgCount').length){
						cite.append('&nbsp;<span style="color:red;" class="taskMsgCount"></span>');
					}
					if(taskCount > 99){
						$(".taskMsgCount").text("("+99+"+)").show();
					}else{
						$(".taskMsgCount").text("("+taskCount+")").show();
					}
				}
				var resultCount = data.resultCount;
				if(resultCount == 0){
					$(".resultMsgCount").hide();
				}else {
					var cite = $("#admin-side ul li dl.layui-nav-child a[lay-id='5c62e9e3588a91ea90cc00163e13e3me']").find('cite');
					if(!cite.has('span.resultMsgCount').length){
						cite.append('&nbsp;<span style="color:red;" class="resultMsgCount"></span>');
					}
					if(resultCount > 99){
						$(".resultMsgCount").text("("+99+"+)").show();
					}else{
						$(".resultMsgCount").text("("+resultCount+")").show();
					}
				}
			}else{
				$(".allMsgCount").hide();
				$(".taskMsgCount").hide();
				$(".resultMsgCount").hide();
			}
		}
	});
}

function logout(){
	$.ajax({
		type : 'get',
		url : urlRootPrefix + '/logout',
		success : function(data) {
			//console.log(data);
			localStorage.removeItem(urlRootPrefix.replace("/","") + "token");
			location.href = urlRootPrefix + '/login.html';
		}
	});
}

var active;

layui.use(['layer', 'element'], function() {
	var $ = layui.jquery,
	layer = layui.layer;
	var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块
    element.on('nav(demo)', function(elem){
      //layer.msg(elem.text());
    });
	
	  //触发事件  
	   active = {  
	       tabAdd: function(obj){
	    	 var lay_id = $(this).attr("lay-id");
	    	 var title = $(this).html() + '<i class="layui-icon" data-id="' + lay_id + '"></i>';
	         //新增一个Tab项  
	         element.tabAdd('admin-tab', {  
	           title: title,
	           content: '<iframe src="' + $(this).attr('data-url') + '"></iframe>',
	           id: lay_id
	         });  
	         element.tabChange("admin-tab", lay_id);    
	       }, 
	       tabDelete: function(lay_id){
    	      element.tabDelete("admin-tab", lay_id);
    	   },
	       tabChange: function(lay_id){
	         element.tabChange('admin-tab', lay_id);
	       }  
	   };  
	   //添加tab  
	   $(document).on('click','a',function(){  
	       if(!$(this)[0].hasAttribute('data-url') || $(this).attr('data-url')===''){
	    	   return;  
	       }
	       var tabs = $(".layui-tab-title").children();
	       var lay_id = $(this).attr("lay-id");

	       for(var i = 0; i < tabs.length; i++) {
	           if($(tabs).eq(i).attr("lay-id") == lay_id) { 
	        	   active.tabChange(lay_id);
	               return;  
	           }    
	       }  
	       active["tabAdd"].call(this);  
	       resize();  
	   });  
	     
	   //iframe自适应  
	   function resize(){  
	       var $content = $('.admin-nav-card .layui-tab-content');  
	       $content.height($(this).height() - 147);  
	       $content.find('iframe').each(function() {  
	           $(this).height($content.height());  
	       });  
	   }  
	   $(window).on('resize', function() {  
	       var $content = $('.admin-nav-card .layui-tab-content');  
	       $content.height($(this).height() - 147);  
	       $content.find('iframe').each(function() {  
	           $(this).height($content.height());  
	       });  
	   }).resize();  
	   
	   //toggle左侧菜单  
	   $('.admin-side-toggle').on('click', function() {
	       var sideWidth = $('#admin-side').width();  
	       if(sideWidth === 200) {  
	           $('#admin-body').animate({  
	               left: '0'  
	           });
	           $('#admin-footer').animate({  
	               left: '0'  
	           });  
	           $('#admin-side').animate({  
	               width: '0'  
	           });  
	       } else {  
	           $('#admin-body').animate({  
	               left: '200px'  
	           });  
	           $('#admin-footer').animate({  
	               left: '200px'  
	           });  
	           $('#admin-side').animate({  
	               width: '200px'  
	               });  
	           }  
	       });
	   
	    //手机设备的简单适配
	    var treeMobile = $('.site-tree-mobile'),
	    shadeMobile = $('.site-mobile-shade');
	    treeMobile.on('click', function () {
	        $('body').addClass('site-mobile');
	    });
	    shadeMobile.on('click', function () {
	        $('body').removeClass('site-mobile');
	    });
});

