var jsondata = {
	"areas" : {},
	"lines" : {
		"demo_line_5" : {
			"to" : "demo_node_4",
			"from" : "demo_node_3",
			"name" : "提交申请",
			"type" : "sl"
		},
		"demo_line_6" : {
			"to" : "demo_node_3",
			"from" : "demo_node_1",
			"name" : "",
			"type" : "sl"
		},
		"demo_line_10" : {
			"to" : "demo_node_8",
			"from" : "demo_node_4",
			"name" : "通过",
			"type" : "sl"
		},
		"demo_line_11" : {
			"M" : 157,
			"to" : "demo_node_4",
			"from" : "demo_node_9",
			"name" : "不接受",
			"type" : "tb"
		},
		"demo_line_12" : {
			"to" : "demo_node_9",
			"from" : "demo_node_8",
			"name" : "大于8000",
			"type" : "sl"
		},
		"demo_line_13" : {
			"to" : "demo_node_2",
			"from" : "demo_node_8",
			"name" : "小于8000",
			"type" : "sl"
		},
		"demo_line_14" : {
			"to" : "demo_node_2",
			"from" : "demo_node_9",
			"name" : "接受",
			"type" : "sl"
		}
	},
	"nodes" : {
		"demo_node_1" : {
			"alt" : true,
			"top" : 38,
			"left" : 42,
			"name" : "开始",
			"type" : "start round mix",
			"width" : 26,
			"height" : 26
		},
		"demo_node_2" : {
			"alt" : true,
			"top" : 42,
			"left" : 797,
			"name" : "结束",
			"type" : "end round mix",
			"width" : 26,
			"height" : 26
		},
		"demo_node_3" : {
			"alt" : true,
			"top" : 39,
			"left" : 155,
			"name" : "入职申请",
			"type" : "task",
			"width" : 104,
			"height" : 26
		},
		"demo_node_4" : {
			"alt" : true,
			"top" : 42,
			"left" : 364,
			"name" : "人力审批",
			"type" : "task",
			"width" : 104,
			"height" : 26,
			"verifyType" : "1",
			"verifyModelIds" : "1,2,3",
			"verifyModelNames" : "超级管理员,刘文杰,胡兵兵"
		},
		"demo_node_8" : {
			"alt" : true,
			"top" : 43,
			"left" : 571,
			"name" : "工资判断",
			"type" : "node",
			"width" : 104,
			"height" : 26
		},
		"demo_node_9" : {
			"alt" : true,
			"top" : 141,
			"left" : 559,
			"name" : "经理终审",
			"type" : "task",
			"width" : 104,
			"height" : 26
		}
	},
	"title" : "newFlow_1",
	"initNum" : 18
};