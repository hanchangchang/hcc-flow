//textArea换行符转,
function encodeTextAreaString(str) {
	 if(str){
		 var reg = new RegExp("\n", "g");
		 str = str.replace(reg, ",");
	 }
	 return str;
}
//, 转 textArea换行符
function decodeTextAreaString(str) {
	if(str){
	    var reg = new RegExp(",", "g");
	    str = str.replace(reg, "\n");
	}
    return str;
}
