package com.hcc.flow.server.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * swagger文档
 * 
 * @author 韩长长 
 *
 * 
 */
@Configuration
@EnableSwagger2
@ComponentScan(basePackages = { "com.hcc.flow.server.controller" })
public class SwaggerConfig {

	@Bean
	public Docket docket() {
		return new Docket(DocumentationType.SWAGGER_2).enable(false).groupName("用户失信微系统接口文档")
				.apiInfo(new ApiInfoBuilder().title("用户失信微系统接口文档").version("1.0").build()).select()
				.paths(PathSelectors.any()).build();
	}
}
