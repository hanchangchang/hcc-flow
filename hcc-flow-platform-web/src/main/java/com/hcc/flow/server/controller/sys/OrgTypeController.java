package com.hcc.flow.server.controller.sys;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hcc.flow.server.annotation.LogAnnotation;
import com.hcc.flow.server.common.constant.Constant;
import com.hcc.flow.server.common.model.ApiResult;
import com.hcc.flow.server.common.utils.StringUtilsV2;
import com.hcc.flow.server.dao.sys.OrgTypeDao;
import com.hcc.flow.server.model.sys.OrgType;
import com.hcc.flow.server.where.sys.OrgTypeWhere;

import io.swagger.annotations.ApiOperation;
/**
 * 机构类型管理
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/orgType")
public class OrgTypeController {

	@Autowired
	private OrgTypeDao orgTypeDao;

	@PostMapping
	@ApiOperation(value = "机构类型-保存")
	public OrgType save(@RequestBody OrgType orgType) {
		orgTypeDao.save(orgType);
		return orgType;
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "机构类型-根据id获取")
	public ApiResult<?> get(@PathVariable Long id) {
		if(id == null){
			return ApiResult.error("要查询的id不能为空");
		}
    	return ApiResult.data(orgTypeDao.getById(id));
	}

	@PutMapping
	@ApiOperation(value = "机构类型-修改")
	public OrgType update(@RequestBody OrgType orgType) {
		orgTypeDao.update(orgType);

		return orgType;
	}

	@GetMapping("/list")
	@ApiOperation(value = "机构类型-机构类型列表")
	public List<OrgType> List() {
		return orgTypeDao.listAll();
	}

	@PostMapping("/list")
	@ApiOperation(value = "机构类型-列表（分页）")
	public PageInfo<OrgType> ListNew(@RequestBody OrgTypeWhere strCon) {
		PageHelper.startPage(strCon.getOffset(), strCon.getLimit());
		if (StringUtilsV2.isBlank(strCon.getSqlOrder())) {
			strCon.setSqlOrder("org_type_id ");
		}
		PageHelper.orderBy(strCon.getSqlOrder());
		List<OrgType> list = orgTypeDao.OrgTypeList(strCon);
		PageInfo<OrgType> result = new PageInfo<OrgType>(list);
		return result;
	}

	@GetMapping("/listStatusC")
	@ApiOperation(value = "机构类型-机构类型列表")
	public List<OrgType> listStatusC() {
		return orgTypeDao.listByStausAll(Constant.DATA_STATUS_TYPE_NORMAL);
	}

	@LogAnnotation
	@DeleteMapping("/delete/{id}")
	@ApiOperation(value = "机构类型-物理删除")
	//@PreAuthorize("hasAuthority('sys:org:del')")
	public ApiResult<?> delete(@PathVariable String id) {
		if (StringUtilsV2.isBlank(id)) {
			return ApiResult.error("机构ID不能为空");
		}
		orgTypeDao.delete(id);
		return ApiResult.ok();
	}

	@LogAnnotation
	@DeleteMapping("/{id}")
	@ApiOperation(value = "机构类型-逻辑删除")
	//@PreAuthorize("hasAuthority('sys:org:del')")
	public ApiResult<?> deleteEditStatus(@PathVariable String id) {
		if (StringUtilsV2.isBlank(id)) {
			return ApiResult.error("机构ID不能为空");
		}
		orgTypeDao.updateStatus(Arrays.asList(id), Constant.DATA_STATUS_DELETE);
		return ApiResult.ok();
	}

	@LogAnnotation
	@DeleteMapping("/stop/{id}")
	@ApiOperation(value = "机构类型-停用")
	//@PreAuthorize("hasAuthority('sys:org:stop')")
	public ApiResult<?> stop(@PathVariable String id) {
		if (StringUtilsV2.isBlank(id)) {
			return ApiResult.error("机构ID不能为空");
		}
		orgTypeDao.updateStatus(Arrays.asList(id), Constant.DATA_STATUS_TYPE_STOP);
		return ApiResult.ok();
	}
}
