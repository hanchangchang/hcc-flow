package com.hcc.flow.server.controller.flowDesign;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hcc.flow.server.annotation.LogAnnotation;
import com.hcc.flow.server.annotation.Original;
import com.hcc.flow.server.common.constant.Constant;
import com.hcc.flow.server.common.enums.DataStatusType;
import com.hcc.flow.server.common.model.ApiResult;
import com.hcc.flow.server.common.page.table.PageTableHandler;
import com.hcc.flow.server.common.page.table.PageTableRequest;
import com.hcc.flow.server.common.page.table.PageTableResponse;
import com.hcc.flow.server.common.utils.CommUtil;
import com.hcc.flow.server.common.utils.StringUtilsV2;
import com.hcc.flow.server.common.vo.SelectIdLongVO;
import com.hcc.flow.server.common.where.PublicWhere;
import com.hcc.flow.server.dao.flowDesign.FlowTaskTypeDao;
import com.hcc.flow.server.model.flowDesign.FlowTaskType;
import com.hcc.flow.server.service.sys.UserService;
import com.hcc.flow.server.utils.CheckIdUtil;
import com.hcc.flow.server.vo.flowDesign.FlowTaskTypeVO;

import io.swagger.annotations.ApiOperation;

/**
 * 流程业务类型管理
 * @author 韩长志
 *
 */
@RestController
@RequestMapping("/flowTaskTypes")
public class FlowTaskTypeController {

    @Autowired
    private FlowTaskTypeDao flowTaskTypeDao;
	@Autowired
    private UserService userService;
    
	@LogAnnotation
    @PostMapping
    @ApiOperation(value = "保存")
    @PreAuthorize("hasAuthority('flowDesign:flowTaskType:add')")
    public ApiResult<?> save(@RequestBody FlowTaskType flowTaskType) {
    	if(flowTaskType == null){
    		return ApiResult.error("新增数据不能为空");
    	}
    	
        flowTaskTypeDao.save(flowTaskType);
        return ApiResult.ok();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "根据id获取")
    public ApiResult<?> get(@PathVariable String id) {
        if(StringUtils.isBlank(id)){
			return ApiResult.error("要查询的id不能为空");
		}
    	return ApiResult.data(flowTaskTypeDao.getById(id));
    }
    
	@LogAnnotation
    @PutMapping
    @ApiOperation(value = "修改")
    @PreAuthorize("hasAuthority('flowDesign:flowTaskType:edit')")
    public ApiResult<?> update(@RequestBody FlowTaskType flowTaskType) {
    	if(flowTaskType == null){
    		return ApiResult.error("修改数据不能为空");
    	}
    	if(StringUtilsV2.isBlankObj(flowTaskType.getTaskTypeId())){
    		return ApiResult.error("要修改的数据ID不能为空");
    	}
    	ApiResult<?> checkAr = CheckIdUtil.checkId(Arrays.asList("1","5"), flowTaskType.getTaskTypeId().toString());
		if(!checkAr.isSuccess()){
			return checkAr;
		}
        flowTaskTypeDao.update(flowTaskType);
        return ApiResult.ok();
    }
    
	@LogAnnotation
    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "物理删除")
    @PreAuthorize("hasAuthority('flowDesign:flowTaskType:del')")
    public ApiResult<?> delete(@PathVariable String id) {
    	if(StringUtils.isBlank(id)){
    		return ApiResult.error("要删除的数据ID不能为空");
    	}
    	ApiResult<?> checkAr = CheckIdUtil.checkId(Arrays.asList("1","5"), id);
		if(!checkAr.isSuccess()){
			return checkAr;
		}
        flowTaskTypeDao.delete(id);
        return ApiResult.ok();
    }
    @LogAnnotation
    @DeleteMapping("/{id}")
    @ApiOperation(value = "逻辑删除")
    @PreAuthorize("hasAuthority('flowDesign:flowTaskType:del')")
    public ApiResult<?> editStatusDelete(@PathVariable String id) {
    	if(StringUtils.isBlank(id)){
    		return ApiResult.error("要删除的数据ID不能为空");
    	}
    	ApiResult<?> checkAr = CheckIdUtil.checkId(Arrays.asList("1","5"), id);
		if(!checkAr.isSuccess()){
			return checkAr;
		}
        flowTaskTypeDao.updateStatus(Arrays.asList(id),Constant.DATA_STATUS_DELETE);
        return ApiResult.ok();
    }
    @LogAnnotation
    @DeleteMapping("/status/{id}")
    @ApiOperation(value = "停用/启用")
    @PreAuthorize("hasAuthority('flowDesign:flowTaskType:edit')")
    public ApiResult<?> stop(@PathVariable String id) {
    	if(StringUtils.isBlank(id)){
    		return ApiResult.error("要停用的数据ID不能为空");
    	}
    	ApiResult<?> checkAr = CheckIdUtil.checkId(Arrays.asList("1","5"), id);
		if(!checkAr.isSuccess()){
			return checkAr;
		}
    	FlowTaskType flowTaskType = flowTaskTypeDao.getById(id);
    	if(DataStatusType.NORMAL.getCode().equals(flowTaskType.getStatus())) {
    		flowTaskTypeDao.updateStatus(Arrays.asList(id),Constant.DATA_STATUS_TYPE_STOP);
    	}else{
    		flowTaskTypeDao.updateStatus(Arrays.asList(id),Constant.DATA_STATUS_TYPE_NORMAL);
    	}
        return ApiResult.ok();
    }
    
    @PostMapping("/list")
	@ApiOperation(value = "列表（分页）- vue版本预留")
	@PreAuthorize("hasAuthority('flowDesign:flowTaskType:query')")
	public PageInfo<FlowTaskTypeVO> ListNew(@RequestBody PublicWhere strCon) {
		PageHelper.startPage(strCon.getOffset(), strCon.getLimit());
		if (StringUtils.isBlank(strCon.getSqlOrder())) {
			strCon.setSqlOrder("t.create_time desc ");
		}
		PageHelper.orderBy(strCon.getSqlOrder());
		List<FlowTaskTypeVO> list = flowTaskTypeDao.FlowTaskTypeList(strCon);
		PageInfo<FlowTaskTypeVO> result = new PageInfo<FlowTaskTypeVO>(list);
		return result;
	}
	
	@GetMapping
	@ApiOperation(value = "列表（分页）- html版本")
	@Original
	public PageTableResponse ListNew2(PageTableRequest  strCon) {
    	PublicWhere where = new PublicWhere(strCon);
		//只能查看（当前机构+当前子机构的数据）或者（当前角色或者当前只角色的数据[同级角色数据不可看，针对销售岗位]）
    	userService.setOrgIdAndRoleIds(where,"sys:flowTaskType:query");
		PageHelper.startPage(where.getOffset(), where.getLimit());
		if (StringUtils.isBlank(where.getSqlOrder())) {
			where.setSqlOrder("t.create_time");
		}
		PageHelper.orderBy(where.getSqlOrder());
		List<FlowTaskTypeVO> list = flowTaskTypeDao.FlowTaskTypeList(where);
		PageInfo<FlowTaskTypeVO> result = new PageInfo<FlowTaskTypeVO>(list);
		return new PageTableHandler(new PageTableHandler.CountHandler() {
			@Override
			public int count(PageTableRequest request) {
				return CommUtil.null2Int(result.getTotal());
			}
		}, new PageTableHandler.ListHandler() {
			@Override
			public List<FlowTaskTypeVO> list(PageTableRequest request) {
				return result.getList();
			}
		}).handle(strCon);
	}
	
	@GetMapping("/listVo")
	@ApiOperation(value = "列表VO（不分页）")
	public List<SelectIdLongVO> listAllVo() {
		return flowTaskTypeDao.listAllVo();
	}
}
