package com.hcc.flow.server.controller.sys;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.hcc.flow.server.common.model.ApiResult;
import com.hcc.flow.server.service.sys.FileService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "文件")
@RestController
@RequestMapping("/files")
public class FileController {

	@Autowired
	private FileService fileService;

	@PostMapping
	@ApiOperation(value = "文件上传")
	public ApiResult<?> uploadFile(MultipartFile file,
			HttpServletResponse response, HttpServletRequest request) throws IOException {
		return fileService.save(file,response,request);
	}

	/**
	 * 下载文件
	 * 
	 * @param fileInfo
	 *            主要采用url地址进行下载
	 *            格式{"url":"/2018/10/31/882b7e7eb313dc6db29232b9d0c008ae.jpg"}
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/download", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public  void downloadFile(String url,
			HttpServletResponse response) {
		fileService.downloadFile(url, response);
	}
	
}
