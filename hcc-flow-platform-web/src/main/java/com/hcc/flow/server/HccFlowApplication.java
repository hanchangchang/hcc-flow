package com.hcc.flow.server;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;

import com.hcc.flow.server.annotation.EnableEncrypt;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 启动类
 * 
 * @author 韩长长 
 *
 */

@EnableSwagger2
@SpringBootApplication
@EnableEncrypt
@EnableTransactionManagement 
@EnableCaching 
@MapperScan("com.hcc.flow.server.dao")
public class HccFlowApplication {
  
	@Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }
	
	public static void main(String[] args) {
		SpringApplication.run(HccFlowApplication.class, args);
	}
	
}
