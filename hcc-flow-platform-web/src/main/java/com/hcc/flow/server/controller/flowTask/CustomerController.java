package com.hcc.flow.server.controller.flowTask;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hcc.flow.server.annotation.LogAnnotation;
import com.hcc.flow.server.annotation.Original;
import com.hcc.flow.server.common.model.ApiResult;
import com.hcc.flow.server.common.page.table.PageTableHandler;
import com.hcc.flow.server.common.page.table.PageTableRequest;
import com.hcc.flow.server.common.page.table.PageTableResponse;
import com.hcc.flow.server.common.utils.CommUtil;
import com.hcc.flow.server.common.utils.StringUtilsV2;
import com.hcc.flow.server.common.where.PublicWhere;
import com.hcc.flow.server.dao.flowTask.CustomerDao;
import com.hcc.flow.server.dto.flowTask.CustomerDto;
import com.hcc.flow.server.service.flowTask.AdCustomerService;
import com.hcc.flow.server.service.sys.UserService;
import com.hcc.flow.server.vo.flowTask.CustomerVO;

import io.swagger.annotations.ApiOperation;

/**
 * 客户管理
 * 
 * @author 韩长志
 *
 */
@RestController
@RequestMapping("/customers")
public class CustomerController {

	@Autowired
	private CustomerDao customerDao;
	@Autowired
	private UserService userService;
	@Autowired
	private AdCustomerService adCustomerService;
	
	@LogAnnotation
	@PostMapping
	@ApiOperation(value = "客户-保存")
	@PreAuthorize("hasAuthority('customer:customer:add')")
	public ApiResult<?> save(@RequestBody CustomerDto customer) {
		return adCustomerService.save(customer);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "客户-根据id获取")
	@PreAuthorize("hasAuthority('customer:customer:query') or hasAuthority('sys:msgNoticeRead:query')")
	public ApiResult<?> get(@PathVariable String id) {
		if(StringUtilsV2.isBlank(id)){
			return ApiResult.error("要查询的id不能为空");
		}
		CustomerVO vo = customerDao.getById(id);
		return ApiResult.data(vo);
	}

	@LogAnnotation
	@PutMapping
	@ApiOperation(value = "客户-修改")
	@PreAuthorize("hasAuthority('customer:customer:edit')")
	public ApiResult<?> update(@RequestBody CustomerDto customer) {
		return adCustomerService.update(customer);
	}

	@LogAnnotation
	@DeleteMapping("/{id}")
	@ApiOperation(value = "客户-逻辑删除")
	@PreAuthorize("hasAuthority('customer:customer:del')")
	public ApiResult<?> deleteEditStatus(@PathVariable String id) {
		return adCustomerService.deleteEditStatus(id);
	}

	@LogAnnotation
	@DeleteMapping("/sub/{id}")
	@ApiOperation(value = "客户-提交审核")
	@PreAuthorize("hasAuthority('customer:customer:sub')")
	public ApiResult<?> editStatusSub(@PathVariable String id) {
		return adCustomerService.editStatusSub(id);
	}

	@GetMapping
	@ApiOperation(value = "列表（分页）- html版本")
	@Original
	public PageTableResponse ListNew2(PageTableRequest  strCon) {
    	PublicWhere where = new PublicWhere(strCon);
		//只能查看（当前机构+当前子机构的数据）或者（当前角色或者当前只角色的数据[同级角色数据不可看，针对销售岗位]）
    	userService.setOrgIdAndRoleIds(where,":customer:query");
		PageHelper.startPage(where.getOffset(), where.getLimit());
		if (StringUtils.isBlank(where.getSqlOrder())) {
			where.setSqlOrder("t.create_time desc");
		}
		PageHelper.orderBy(where.getSqlOrder());
		List<CustomerVO> list = customerDao.CustomerList(where);
		PageInfo<CustomerVO> result = new PageInfo<CustomerVO>(list);
		return new PageTableHandler(new PageTableHandler.CountHandler() {
			@Override
			public int count(PageTableRequest request) {
				return CommUtil.null2Int(result.getTotal());
			}
		}, new PageTableHandler.ListHandler() {
			@Override
			public List<CustomerVO> list(PageTableRequest request) {
				return result.getList();
			}
		}).handle(strCon);
	}
	
	@PostMapping("/list")
	@ApiOperation(value = "列表（分页）- vue版本预留")
	public PageInfo<CustomerVO> ListNew(@RequestBody PublicWhere strCon) {
		userService.setOrgIdAndRoleIds(strCon,"customer:customer:query");
		PageHelper.startPage(strCon.getOffset(), strCon.getLimit());
		if (StringUtilsV2.isBlank(strCon.getSqlOrder())) {
			strCon.setSqlOrder("t.create_time desc ");
		}
		PageHelper.orderBy(strCon.getSqlOrder());
		List<CustomerVO> list = customerDao.CustomerList(strCon);
		PageInfo<CustomerVO> result = new PageInfo<CustomerVO>(list);
		return result;
	}
	
	@GetMapping("/listMapByCustomerName")
	@PreAuthorize("hasAuthority('customer:customer:query')")
	@ApiOperation(value = "客户-根据客户名称查询客户列表")
	public List<Map<String, Object>> listMapByCustomerName(String companyName,Integer limit) {
		if(limit == null){
			limit = 10;
		}
		return customerDao.listMapByCustomerName1(companyName, limit);

	}
}
