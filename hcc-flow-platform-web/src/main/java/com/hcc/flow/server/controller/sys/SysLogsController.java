package com.hcc.flow.server.controller.sys;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hcc.flow.server.annotation.Original;
import com.hcc.flow.server.common.model.ApiResult;
import com.hcc.flow.server.common.page.table.PageTableHandler;
import com.hcc.flow.server.common.page.table.PageTableRequest;
import com.hcc.flow.server.common.page.table.PageTableResponse;
import com.hcc.flow.server.common.utils.CommUtil;
import com.hcc.flow.server.common.where.AuditLogWhere;
import com.hcc.flow.server.dao.sys.SysLogsDao;
import com.hcc.flow.server.model.sys.SysAuditLogs;
import com.hcc.flow.server.model.sys.SysLogs;
import com.hcc.flow.server.service.sys.SysLogService;
import com.hcc.flow.server.where.sys.LogWhere;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "日志")
@RestController
@RequestMapping("/logs")
public class SysLogsController {

	@Autowired
	private SysLogService sysLogService;
	@Autowired
	private SysLogsDao sysLogsDao;

	@PostMapping("/list")
	@ApiOperation(value = "日志列表")
	
	
	public PageInfo<SysLogs> LogListNew(@RequestBody LogWhere strCon) {
		return sysLogService.findAllLogs(strCon);
	}

	@PostMapping("/auditLogs/list")
	@ApiOperation("获取审核日志")
	public List<SysAuditLogs> AuditLogs(@RequestBody AuditLogWhere auditLogWhere) {
		return sysLogService.findAuditLogs(auditLogWhere);
	}
	
	@GetMapping("/delete/{id}")
	
	
	public ApiResult<?> delete(@PathVariable String id) {
		sysLogsDao.delete(id);
		return ApiResult.ok();
	}
	
	@GetMapping
	@ApiOperation(value = "列表-工作流专用")
	@Original
	public PageTableResponse LogListNew(PageTableRequest  strCon) {
		LogWhere where = new LogWhere(strCon);
		//userService.setOrgIdAndRoleIds(where,"sys:role:query");
		PageHelper.startPage(where.getOffset(), where.getLimit());
		if (where.getSqlOrder() == null || where.getSqlOrder().isEmpty()) {
			where.setSqlOrder("t.create_time desc ");
		}
		PageHelper.orderBy(where.getSqlOrder());
		List<SysLogs> list = sysLogsDao.LogList(where);
		PageInfo<SysLogs> result =  new PageInfo<SysLogs>(list);
		return new PageTableHandler(new PageTableHandler.CountHandler() {
			@Override
			public int count(PageTableRequest request) {
				return CommUtil.null2Int(result.getTotal());
			}
		}, new PageTableHandler.ListHandler() {
			@Override
			public List<SysLogs> list(PageTableRequest request) {
				return result.getList();
			}
		}).handle(strCon);
		//return PageInfo<SysLogs> list = sysLogService.findAllLogs(new LogWhere(strCon));
	}
}
