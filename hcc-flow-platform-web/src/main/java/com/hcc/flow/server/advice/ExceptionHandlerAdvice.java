package com.hcc.flow.server.advice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.hcc.flow.server.common.advice.RRException;
import com.hcc.flow.server.common.model.ApiResult;

/**
 * springmvc异常处理
 * 
 * @author 韩长长 
 *
 */
@RestControllerAdvice
public class ExceptionHandlerAdvice {

	private static final Logger log = LoggerFactory.getLogger("adminLogger");

	@ExceptionHandler({ IllegalArgumentException.class })
	@ResponseStatus(HttpStatus.OK)
	public ApiResult<?> badRequestException(IllegalArgumentException exception) {
		return ApiResult.error(HttpStatus.BAD_REQUEST.value() + "", exception.getMessage());
	}

	@ExceptionHandler({ AccessDeniedException.class })
	@ResponseStatus(HttpStatus.OK)
	public ApiResult<?> badRequestException(AccessDeniedException exception) {
		return ApiResult.error(HttpStatus.FORBIDDEN.value() + "", exception.getMessage());
	}

	@ExceptionHandler({ MissingServletRequestParameterException.class, HttpMessageNotReadableException.class,
			UnsatisfiedServletRequestParameterException.class, MethodArgumentTypeMismatchException.class })
	@ResponseStatus(HttpStatus.OK)
	public ApiResult<?> badRequestException(Exception exception) {
		return ApiResult.error(HttpStatus.BAD_REQUEST.value() + "", exception.getMessage());
	}

	@ExceptionHandler(Throwable.class)
	@ResponseStatus(HttpStatus.OK)
	public ApiResult<?> exception(Throwable throwable) {
		log.error("系统异常", throwable);
		return ApiResult.error(HttpStatus.INTERNAL_SERVER_ERROR.value() + "", throwable.getMessage());

	}
	@ExceptionHandler({ DataIntegrityViolationException.class })
	@ResponseStatus(HttpStatus.OK)
	public ApiResult<?> dataIntegrityViolationException(Exception exception) {
		String msg = exception.getMessage();
		log.error(msg, exception);
		if(msg.contains("Data truncation:")){
			msg = msg.substring(msg.lastIndexOf("Data truncation:")+17).replace(" at row 1", "");
			if(msg.contains("Data too long for column ")){
				msg = msg.replace("Data too long for column ", "录入的数据超过规定长度，对应字段：");
				return ApiResult.error(HttpStatus.INTERNAL_SERVER_ERROR.value() + "", msg);
			}
		}
		return ApiResult.error(HttpStatus.INTERNAL_SERVER_ERROR.value() + "", "违背数据的完整性约束："+msg);
	}
	/**
	 * 自定义异常
	 */
	@ExceptionHandler(RRException.class)
	@ResponseStatus(HttpStatus.OK)
	public ApiResult<?> handleRRException(RRException e) {
		log.error(e.getMessage(), e);
		return ApiResult.error(e.getCode(),e.getMessage());
	}
	
	@ExceptionHandler(TokenFaileException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public ApiResult<?> handleTokenException(TokenFaileException e) {
		log.error(e.getMessage(), e);
		return ApiResult.error(e.getCode(),e.getMessage());
	}
}
