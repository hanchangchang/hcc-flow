package com.hcc.flow.server.controller.sys;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hcc.flow.server.annotation.Original;
import com.hcc.flow.server.common.model.ApiResult;
import com.hcc.flow.server.common.page.table.PageTableHandler;
import com.hcc.flow.server.common.page.table.PageTableRequest;
import com.hcc.flow.server.common.page.table.PageTableResponse;
import com.hcc.flow.server.common.utils.CommUtil;
import com.hcc.flow.server.common.utils.StringUtilsV2;
import com.hcc.flow.server.common.where.PublicWhere;
import com.hcc.flow.server.dao.sys.ParaTypeDao;
import com.hcc.flow.server.model.common.LoginUser;
import com.hcc.flow.server.model.sys.ParaType;
import com.hcc.flow.server.utils.CheckIdUtil;
import com.hcc.flow.server.utils.UserUtil;
import com.hcc.flow.server.vo.sys.ParaTypeVO;

import io.swagger.annotations.ApiOperation;
/**
 * 参数类型管理
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/paraType")
public class ParaTypeController {

	@Autowired
	private ParaTypeDao paraTypeDao;

	@PostMapping
	@ApiOperation(value = "参数类型-保存")
	//@PreAuthorize("hasAuthority('sys:paraType:add')")
	
	
	public ApiResult<?> save(@RequestBody ParaType paraType) {
		if (paraType == null) {
			return ApiResult.error("新增数据不能为空");
		}else if(StringUtilsV2.isBlank(paraType.getParaTypeCode())){
			return ApiResult.error("类型编码不能为空");
		}
		LoginUser loginUser = UserUtil.getLoginUser();
		paraType.setCreateUserId(loginUser.getUserId());
		ParaType local = paraTypeDao.getByParaTypeCode(paraType.getParaTypeCode());
		if(local != null){
			return ApiResult.error("类型编码已存在");
		}
		paraTypeDao.save(paraType);

		return ApiResult.ok();
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "参数类型-根据id获取")
	
	public ApiResult<?> get(@PathVariable Long id) {
		if(id == null){
			return ApiResult.error("要查询的id不能为空");
		}
    	return ApiResult.data(paraTypeDao.getById(id));
	}

	@PutMapping
	@ApiOperation(value = "参数类型-修改")
	//@PreAuthorize("hasAuthority('sys:paraType:edit')")
	public ApiResult<?> update(@RequestBody ParaType paraType) {
		if (paraType == null) {
			return ApiResult.error("修改数据不能为空");
		} 
		if (paraType.getParaTypeId()== null) {
			return ApiResult.error("要修改的数据ID不能为空");
		}
		ApiResult<?> checkAr = CheckIdUtil.checkId(Arrays.asList("13","21"), paraType.getParaTypeId().toString());
		if(!checkAr.isSuccess()){
			return checkAr;
		}
		ParaType local = paraTypeDao.getByParaTypeCode(paraType.getParaTypeCode());
		if(local != null && !local.getParaTypeId().equals(paraType.getParaTypeId())){
			return ApiResult.error("类型编码已存在");
		}
		paraTypeDao.update(paraType);
		return ApiResult.ok();
	}

	@GetMapping("/listAll")
	@ApiOperation(value = "参数类型-参数类型列表")
	
	public List<ParaType> listAll() {
		return paraTypeDao.listAll();
	}
	
	@PostMapping("/list")
	@ApiOperation(value = "参数类型-列表（分页）")
	//@PreAuthorize("hasAuthority('sys:paraType:query')")
	public PageInfo<ParaTypeVO> ListNew(@RequestBody PublicWhere strCon) {
		PageHelper.startPage(strCon.getOffset(), strCon.getLimit());
		if (StringUtilsV2.isBlank(strCon.getSqlOrder())) {
			strCon.setSqlOrder("t.create_time desc ");
		}
		PageHelper.orderBy(strCon.getSqlOrder());
		List<ParaTypeVO> list = paraTypeDao.ParaTypeList(strCon);
		PageInfo<ParaTypeVO> result = new PageInfo<ParaTypeVO>(list);
		return result;
	}

	@GetMapping
	@ApiOperation(value = "列表-分页-2.0")
	@Original
	public PageTableResponse ListNew2(PageTableRequest  strCon) {
		PublicWhere where = new PublicWhere(strCon);
		PageHelper.startPage(where.getOffset(), where.getLimit());
		if (StringUtils.isBlank(where.getSqlOrder())) {
			where.setSqlOrder("t.create_time desc");
		}
		PageHelper.orderBy(where.getSqlOrder());
		List<ParaTypeVO> list = paraTypeDao.ParaTypeList(where);
		PageInfo<ParaTypeVO> result = new PageInfo<ParaTypeVO>(list);
		return new PageTableHandler(new PageTableHandler.CountHandler() {
			@Override
			public int count(PageTableRequest request) {
				return CommUtil.null2Int(result.getTotal());
			}
		}, new PageTableHandler.ListHandler() {
			@Override
			public List<ParaTypeVO> list(PageTableRequest request) {
				return result.getList();
			}
		}).handle(strCon);
	}
	
	@DeleteMapping("/{id}")
	@ApiOperation(value = "参数类型-删除")
	//@PreAuthorize("hasAuthority('sys:paraType:del')")
	public ApiResult<?> delete(@PathVariable String id) {
		if (StringUtilsV2.isBlank(id)) {
			return ApiResult.error("要删除的数据ID不能为空");
		}
		ApiResult<?> checkAr = CheckIdUtil.checkId(Arrays.asList("13","21"), id);
		if(!checkAr.isSuccess()){
			return checkAr;
		}
		paraTypeDao.delete(id);
		return ApiResult.ok();
	}
}
