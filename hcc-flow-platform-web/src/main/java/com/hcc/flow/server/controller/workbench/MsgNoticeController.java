package com.hcc.flow.server.controller.workbench;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hcc.flow.server.annotation.LogAnnotation;
import com.hcc.flow.server.common.constant.Constant;
import com.hcc.flow.server.common.enums.MsgType;
import com.hcc.flow.server.common.model.ApiResult;
import com.hcc.flow.server.common.utils.StringUtilsV2;
import com.hcc.flow.server.common.vo.SelectVO;
import com.hcc.flow.server.dao.workbench.MsgNoticeDao;
import com.hcc.flow.server.dto.workbench.MsgNoticeDto;
import com.hcc.flow.server.model.workbench.MsgNotice;
import com.hcc.flow.server.service.workbench.MsgNoticeService;
import com.hcc.flow.server.utils.UserUtil;
import com.hcc.flow.server.where.workbench.MsgNoticeWhere;

import io.swagger.annotations.ApiOperation;

/**
 * 消息管理
 * @author 韩长志 20190604
 *
 */
@RestController
@RequestMapping("/msgNotices")
public class MsgNoticeController {

    @Autowired
    private MsgNoticeDao msgNoticeDao;
    @Autowired
    private MsgNoticeService msgNoticeService;
    
	@LogAnnotation
    @PostMapping
    @ApiOperation(value = "保存")
    @PreAuthorize("hasAuthority('sys:msgNotice:sys:add') or hasAuthority('sys:msgNotice:msg:add')")
    public ApiResult<?> save(@RequestBody MsgNoticeDto msgNotice) {
    	if(msgNotice == null){
    		return ApiResult.error("新增数据不能为空");
    	}
    	return msgNoticeService.save(msgNotice);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "根据id获取")
    @PreAuthorize("hasAuthority('sys:msgNotice:query')")
    public ApiResult<?> get(@PathVariable String id) {
        if(StringUtilsV2.isBlank(id)){
			return ApiResult.error("要查询的id不能为空");
		}
    	return ApiResult.data(msgNoticeDao.getVoById(id,null));
    }
    
	@LogAnnotation
    @PutMapping
    @ApiOperation(value = "修改")
    @PreAuthorize("hasAuthority('sys:msgNotice:edit')")
    public ApiResult<?> update(@RequestBody MsgNotice msgNotice) {
    	if(msgNotice == null){
    		return ApiResult.error("修改数据不能为空");
    	}
    	if(StringUtilsV2.isBlank(msgNotice.getNoticeId())){
    		return ApiResult.error("要修改的数据ID不能为空");
    	}
        msgNoticeDao.update(msgNotice);
        return ApiResult.ok();
    }
    
	@LogAnnotation
    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "物理删除")
    @PreAuthorize("hasAuthority('sys:msgNotice:del')")
    public ApiResult<?> delete(@PathVariable String id) {
    	if(StringUtilsV2.isBlank(id)){
    		return ApiResult.error("要删除的数据ID不能为空");
    	}
        msgNoticeDao.delete(id);
        return ApiResult.ok();
    }
    @LogAnnotation
    @DeleteMapping("/{id}")
    @ApiOperation(value = "逻辑删除")
    @PreAuthorize("hasAuthority('sys:msgNotice:del')")
    public ApiResult<?> editStatusDelete(@PathVariable String id) {
    	if(StringUtilsV2.isBlank(id)){
    		return ApiResult.error("要删除的数据ID不能为空");
    	}
        msgNoticeDao.updateStatus(Arrays.asList(id),Constant.DATA_STATUS_DELETE);
        //msgNoticeDao.updateStatus(Arrays.asList(id),Constant.USE_STATUS_DELETE);
        //msgNoticeDao.updateStatus(Arrays.asList(id),Constant.CHECK_STATUS_DELETE);
        return ApiResult.ok();
    }
    @LogAnnotation
    @DeleteMapping("/stop/{id}")
    @ApiOperation(value = "停用")
    @PreAuthorize("hasAuthority('sys:msgNotice:stop')")
    public ApiResult<?> stop(@PathVariable String id) {
    	if(StringUtilsV2.isBlank(id)){
    		return ApiResult.error("要停用的数据ID不能为空");
    	}
        msgNoticeDao.updateStatus(Arrays.asList(id),Constant.DATA_STATUS_TYPE_STOP);
        return ApiResult.ok();
    }
    
    @PostMapping("/list")
	@ApiOperation(value = "列表（分页）")
	@PreAuthorize("hasAuthority('sys:msgNotice:query')")
	public PageInfo<MsgNotice> ListNew(@RequestBody MsgNoticeWhere strCon) {
		PageHelper.startPage(strCon.getOffset(), strCon.getLimit());
		if (StringUtilsV2.isBlank(strCon.getSqlOrder())) {
			strCon.setSqlOrder("n.push_time desc ");
		}
		PageHelper.orderBy(strCon.getSqlOrder());
		strCon.setPushUserId(UserUtil.getLoginUser().getUserId());
		/*if(MsgType.MSG.getCode().equals(strCon.getType())){
			List<String> collectUserIds = strCon.getCollectUserIds();
			if(CollectionUtils.isEmpty(collectUserIds) && CollectionUtils.isEmpty(strCon.getRoleIds()) && CollectionUtils.isEmpty(strCon.getOrgIds())){
				collectUserIds = userService.getListUserIdsByOrgId(null);//查找登陆机构和旗下所有子机构下所有用户
			}else{
				if(CollectionUtils.isEmpty(collectUserIds)){
					if(CollectionUtils.isEmpty(strCon.getRoleIds())){
						collectUserIds = userDao.listUserIdsByOrgIds(StringUtilsV2.join(strCon.getOrgIds(),","));//查找指定机构ids下的所有用户
					}else{
						collectUserIds = userService.getListUserIdsByRoleIds(StringUtilsV2.join(strCon.getRoleIds(),","));//查找指定角色ids下的所有用户
					}
				}
			}
		}*/
		List<MsgNotice> list = msgNoticeDao.MsgNoticeList(strCon);
		PageInfo<MsgNotice> result = new PageInfo<MsgNotice>(list);
		return result;
	}
	
    @GetMapping("/menu")
    @ApiOperation(value = "首页菜单")
    @PreAuthorize("hasAuthority('sys:msgNoticeRead:query')")
    public ApiResult<?> indexMenu(HttpServletRequest request) {
    	List<SelectVO> memu = new ArrayList<>();
    	//sys:msgNotice:sys:add //sys:msgNotice:msg:add
    	memu.add(new SelectVO(MsgType.SYS.getCode(),MsgType.SYS.getName()));
		memu.add(new SelectVO(MsgType.MSG.getCode(),MsgType.MSG.getName()));
    	return ApiResult.data(memu);
    }
	
}
