package com.hcc.flow.server.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.hcc.flow.server.common.constant.ParamConfig;

@Component
public class ApplicationRunnerImpl implements ApplicationRunner {
	
	private static final Logger log = LoggerFactory.getLogger("adminLogger");
	
    @Override
    public void run(ApplicationArguments args) throws Exception {
    	//log.debug("通过实现ApplicationRunner接口，在spring boot项目启动后执行");
        init();//初始化方法
    }
    
    private void init() {

		log.debug("应用初始化完成，使用的缓存为:"+ParamConfig.redisOrEhcache);
	}
}
