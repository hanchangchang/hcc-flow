package com.hcc.flow.server.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 * 
 * @author Administrator
 *
 * @version 创建时间：2019年2月27日下午2:26:24
 *类说明：用于对model行的注解
 */
@Target({ ElementType.PARAMETER, ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ModelApiParam {
	String value() default "";
}