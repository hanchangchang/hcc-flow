package com.hcc.flow.server.controller.sys;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hcc.flow.server.annotation.LogAnnotation;
import com.hcc.flow.server.common.constant.Constant;
import com.hcc.flow.server.common.enums.DataStatusType;
import com.hcc.flow.server.common.model.ApiResult;
import com.hcc.flow.server.common.utils.StringUtilsV2;
import com.hcc.flow.server.common.vo.SelectVO;
import com.hcc.flow.server.common.where.PublicWhere;
import com.hcc.flow.server.dao.sys.SysListDao;
import com.hcc.flow.server.model.sys.SysList;
import com.hcc.flow.server.vo.sys.SysListVO;

import io.swagger.annotations.ApiOperation;

/**
 * 系统管理
 * @author 韩长志
 *
 */
@RestController
@RequestMapping("/sysLists")
public class SysListController {

    @Autowired
    private SysListDao sysListDao;

	@LogAnnotation
    @PostMapping
    @ApiOperation(value = "保存")
    //@PreAuthorize("hasAuthority('sys:sysList:add')")
    
	
    public ApiResult<?> save(@RequestBody SysList sysList) {
    	if(sysList == null){
    		return ApiResult.error("新增数据不能为空");
    	}
    	
        sysListDao.save(sysList);
        return ApiResult.ok();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "根据id获取")
    //@PreAuthorize("hasAuthority('sys:sysList:query')")
    
    public ApiResult<?> get(@PathVariable String id) {
        if(StringUtilsV2.isBlank(id)){
			return ApiResult.error("要查询的id不能为空");
		}
    	return ApiResult.data(sysListDao.getById(id));
    }
    
	@LogAnnotation
    @PutMapping
    @ApiOperation(value = "修改")
    //@PreAuthorize("hasAuthority('sys:sysList:edit')")
    
	
    public ApiResult<?> update(@RequestBody SysList sysList) {
    	if(sysList == null){
    		return ApiResult.error("修改数据不能为空");
    	}
    	if(sysList.getSysId() == null){
    		return ApiResult.error("要修改的数据ID不能为空");
    	}
        sysListDao.update(sysList);
        return ApiResult.ok();
    }
    
	@LogAnnotation
    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "物理删除")
    //@PreAuthorize("hasAuthority('sys:sysList:del')")
    
    public ApiResult<?> delete(@PathVariable String id) {
    	if(StringUtilsV2.isBlank(id)){
    		return ApiResult.error("要删除的数据ID不能为空");
    	}
        sysListDao.delete(id);
        return ApiResult.ok();
    }
    @LogAnnotation
    @DeleteMapping("/{id}")
    @ApiOperation(value = "逻辑删除")
    //@PreAuthorize("hasAuthority('sys:sysList:del')")
    
    public ApiResult<?> editStatusDelete(@PathVariable String id) {
    	if(StringUtilsV2.isBlank(id)){
    		return ApiResult.error("要删除的数据ID不能为空");
    	}
        sysListDao.updateStatus(Arrays.asList(id),Constant.DATA_STATUS_DELETE);
        //sysListDao.updateStatus(Arrays.asList(id),Constant.USE_STATUS_DELETE);
        //sysListDao.updateStatus(Arrays.asList(id),Constant.CHECK_STATUS_DELETE);
        return ApiResult.ok();
    }
    @LogAnnotation
    @DeleteMapping("/stop/{id}")
    @ApiOperation(value = "停用")
    //@PreAuthorize("hasAuthority('sys:sysList:stop')")
    
    public ApiResult<?> stop(@PathVariable String id) {
    	if(StringUtilsV2.isBlank(id)){
    		return ApiResult.error("要停用的数据ID不能为空");
    	}
        sysListDao.updateStatus(Arrays.asList(id),Constant.DATA_STATUS_TYPE_STOP);
        return ApiResult.ok();
    }
    
    @PostMapping("/list")
	@ApiOperation(value = "列表（分页）")
	//@PreAuthorize("hasAuthority('sys:sysList:query')")
	
	
	public PageInfo<SysList> ListNew(@RequestBody PublicWhere strCon) {
		PageHelper.startPage(strCon.getOffset(), strCon.getLimit());
		if (StringUtilsV2.isBlank(strCon.getSqlOrder())) {
			strCon.setSqlOrder("t.create_time desc ");
		}
		PageHelper.orderBy(strCon.getSqlOrder());
		List<SysList> list = sysListDao.SysListList(strCon);
		PageInfo<SysList> result = new PageInfo<SysList>(list);
		return result;
	}
	
	@GetMapping("/listAll")
	@ApiOperation(value = "列表（不分页）")
	//@PreAuthorize("hasAuthority('sys:sysList:query')")
	
	public List<SysList> listAll() {
		return sysListDao.listAll();
	}
	
	@GetMapping("/listVoAll")
	@ApiOperation(value = "列表（不分页）")
	
	public List<SysListVO> listVoAll() {
		return sysListDao.listSelectVOAll();
	}
	
	@GetMapping("/listValidSelect")
	@ApiOperation(value = "有效搜索条件")
	
	public Map<String, Object> listValidSelect() {
		Map<String, Object> map = new HashMap<>();
		List<String> statuss = sysListDao.listValidStatus();
		List<SelectVO> listValidStatus = new ArrayList<>();
		for (String status : statuss) {
			SelectVO svo = new SelectVO();
			svo.setId(status);
			svo.setName(DataStatusType.getName(status));
			listValidStatus.add(svo);
		}
		map.put("listValidStatus", listValidStatus);
		return map;
	}
}
