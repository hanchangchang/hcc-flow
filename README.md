# hcc-flow 流程系统

#### 介绍
1. hcc-flow是一个轻量级的流程系统，全开源，无套路，无挖矿植入，无流量偷跑，无广告植入
2. hcc-flow流程设计图数据采用json交互，拓展性强。
3. 现有市场常用工作流引擎activity/flowable太过于臃肿，学习成本高，个人维护相对困难。
   国产工作流引擎全开源的基本为零（有开源的都有所保留，想拓展得付费），其他非开源软件（宏天、泛微、红迅、飞天等等）价格太过于昂贵(之前因为 
   工作中要使用，咨询了下价格基本不低于5万)。且使用的技术相对老旧。所以自己纯手后端写做了一套，分享给大家，这也是我的初衷。
4. 功能模块：流程模型（业务类型、流程设计图）、流程业务（相关流程任务发起及管理）、流程工作台（流程任务处理、状态信息反馈）、系统管理（机构/角色/用户管理、菜单管理、字典管理、日志管理、代码生成）、等。

#### 软件架构
1.  后端：spring boot2.0+、spring security、redis/ehcache[修改配置参数随意切换]、myBatis、pageHelper等框架
2.  前端：html、bootstrap、layui、jqury、dataTable、canvas[流程设计]等
3.  vue版本开发中，相关接口已完成

#### 演示说明

1.  演示地址：http://hccflow.cn/
2.  测试账号：admin 密码：hccflow

#### 使用说明

1.  码云仓库：https://gitee.com/hanchangchang/hcc-flow  Gitlab仓库：https://gitlab.com/hanchangchang/hcc-flow
2.  QQ：269276521
3.  EMAIL：han2464@qq.com
4.  如需关注项目最新动态，请Watch、Star项目，同时也是对项目最好的支持

#### 安装教程

1.  拉取Git源码,hcc-flow-platform-web为入口项目，执行resources/db/下数据库脚本
2.  maven命令编译主项目hcc-flow
3.  启动入口项目HccFlowApplication.java
4.  本地访问端口127.0.0.1:10000
5.  开发文档(编写中)：http://139.129.99.14/guide
6.  技术讨论、二次开发等咨询、问题和建议，请QQ联系，我会在第一时间进行解答和回复！

#### 项目结构


- hcc-flow
- │
- ├─hcc-flow-common 公共模块
- │  ├─advice（exception）、annotation 异常处理、自定义注解
- │  ├─constant、enums、vo 参数、枚举
- │  ├─utiles 工具类
- │  └─page.table 分页组件
- │ 
- ├─hcc-flow-model 数据库对应实体及部分公共类
- │  ├─dto、vo 交互实体
- │  ├─model 数据库实体
- │  └─where 分页条件
- │ 
- ├─hcc-flow-dao 持久层交互模块
- │  ├─dao 相关持久层方法定义，包含部分ibatis
- │  └─resources
- │       └─mybatis-mappers dao对应MybatisMappers.xml
- │ 
- ├─hcc-flow-service 服务接口实现模块
- │  ├─service
- │  └─resources
- │       └─libs 部分三方引入 读取视频文件分辨率/时长
- │
- ├─hcc-flow-platform-web 系统入口模块
- │  ├─config  security、redis等配置注入
- │  ├─controller api 
- │  ├─filter 过滤器
- │  ├─RenrenApplication 项目启动类
- │  └─static 静态资源


#### 如何拓展新业务流程

1.  数据库新增一张业务表（如请假清单表:leave）
2.  登陆系统在菜单：流程模型->业务类型->新增->请假单审批。注意model字段为对应业务java实体名：Leave
3.  在菜单：系统管理->代码生成->输入数据库表名leave。自动生成对应基础代码文件，然后将文件拷贝到项目里
4.  根据现有demo（客户、素材审批对应代码）修改补充审批业务逻辑代码，主要查看示例文件 
    (1.customerList.html 2.addCustomer.html 3.CustomerController 4.AdCustomerService.java 5.MsgNoticeService.java)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 后续计划

1.  补全相关文档+简化及完善现有功能代码
2.  流程任务转办功能
3.  复合流程功能(流程某个节点为一个子流程)
4.  工作台代办/消息提醒使用websocket技术
5.  全自动生成审核业务相关代码（MsgNoticeService自动补全新增审核业务代码）
6.  前端vue版本的开发

#### 页面效果展示

![效果图片1](https://images.gitee.com/uploads/images/2020/0630/162611_337416a6_1347360.png "微信截图_20200630152405.png")
![效果图片2](https://images.gitee.com/uploads/images/2020/0630/162630_82c82a17_1347360.png "微信截图_20200630152547.png")
![效果图片3](https://images.gitee.com/uploads/images/2020/0630/162642_df5cea7b_1347360.png "微信截图_20200630152613.png")
![效果图片4](https://images.gitee.com/uploads/images/2020/0630/162652_7c2d5319_1347360.png "微信截图_20200630155126.png")
![效果图片5](https://images.gitee.com/uploads/images/2020/0630/162704_b38998a5_1347360.png "微信截图_20200630155210.png")
![效果图片6](https://images.gitee.com/uploads/images/2020/0630/162719_e9480c80_1347360.png "微信截图_20200630155217.png")
![效果图片7](https://images.gitee.com/uploads/images/2020/0630/162730_48e1b80b_1347360.png "微信截图_20200630155236.png")
![效果图片8](https://images.gitee.com/uploads/images/2020/0630/162739_83b7bb4e_1347360.png "微信截图_20200630155246.png")
![效果图片9](https://images.gitee.com/uploads/images/2020/0630/162750_393b7e2c_1347360.png "微信截图_20200630155300.png")
![效果图片10](https://images.gitee.com/uploads/images/2020/0630/162801_1bbf1f15_1347360.png "微信截图_20200630155320.png")
![效果图片11](https://images.gitee.com/uploads/images/2020/0630/162810_d1eef0ad_1347360.png "微信截图_20200630155604.png")
![效果图片12](https://images.gitee.com/uploads/images/2020/0630/162817_0b9d99d5_1347360.png "微信截图_20200630155611.png")
![效果图片13](https://images.gitee.com/uploads/images/2020/0630/162824_3c04a99a_1347360.png "微信截图_20200630155626.png")
![效果图片14](https://images.gitee.com/uploads/images/2020/0630/162832_4801dfe1_1347360.png "微信截图_20200630155806.png")
![效果图片15](https://images.gitee.com/uploads/images/2020/0630/162839_fae22446_1347360.png "微信截图_20200630155858.png")