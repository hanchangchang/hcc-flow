package com.hcc.flow.server.common.enums;

import java.util.ArrayList;
import java.util.List;

import com.hcc.flow.server.common.vo.SelectVO;

/**
 * 是否状态
 * 
 * @author 韩长志 2019-11-19istrator
 *
 */
public enum IfStatusType {

	/**
	 * Y
	 */
	Y("Y", "是", "有", "已","1"),
	/**
	 * N
	 */
	N("N", "否", "无", "未","0");

	private String code;
	private String name;
	private String name1;
	private String name2;
	private String nameNumber;
	private IfStatusType(String code, String name, String name1, String name2, String nameNumber) {
		this.code = code;
		this.name = name;
		this.name1 = name1;
		this.name2 = name2;
		this.nameNumber = nameNumber;
	}
	public String getName1() {
		return name1;
	}

	public String getName2() {
		return name2;
	}

	
	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getNameNumber() {
		return nameNumber;
	}

	public static String getCode(String sname) {
		IfStatusType[] businessModeEnums = values();
		for (IfStatusType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getName().equals(sname)) {
				return businessModeEnum.getCode();
			}
		}
		return null;
	}

	public static String getName(String scode) {
		IfStatusType[] businessModeEnums = values();
		for (IfStatusType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getCode().equals(scode)) {
				return businessModeEnum.getName();
			}
		}
		return null;
	}
	public static String getName1(String scode) {
		IfStatusType[] businessModeEnums = values();
		for (IfStatusType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getCode().equals(scode)) {
				return businessModeEnum.getName1();
			}
		}
		return null;
	}
	public static String getName2(String scode) {
		IfStatusType[] businessModeEnums = values();
		for (IfStatusType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getCode().equals(scode)) {
				return businessModeEnum.getName2();
			}
		}
		return null;
	}
	
	public static List<SelectVO> selectVOsNotDel() {
		List<SelectVO> list = new ArrayList<>();
		for (IfStatusType businessModeEnum : values()) {
			list.add(new SelectVO(businessModeEnum.getCode(), businessModeEnum.getName()));
		}
		return list;
	}
	public static List<SelectVO> selectVOsNotDel(int i) {
		List<SelectVO> list = new ArrayList<>();
		for (IfStatusType businessModeEnum : values()) {
			String name = businessModeEnum.getName();
			if(i == 2){
				name = businessModeEnum.getName1();
			}else if(i == 3){
				name = businessModeEnum.getName2();
			}
			list.add(new SelectVO(businessModeEnum.getCode(), name));
		}
		return list;
	}
	public String getNameNumber(String scode) {
		IfStatusType[] businessModeEnums = values();
		for (IfStatusType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getCode().equals(scode)) {
				return businessModeEnum.getNameNumber(scode);
			}
		}
		return null;
	}

	
}
