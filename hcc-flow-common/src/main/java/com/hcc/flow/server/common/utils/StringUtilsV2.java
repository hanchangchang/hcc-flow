package com.hcc.flow.server.common.utils;

import org.apache.commons.lang3.StringUtils;

public class StringUtilsV2 extends StringUtils{
	
	public static boolean isBlank(final CharSequence cs) {
		return StringUtils.isBlank(cs) || cs.toString().toLowerCase().equals("undefined") || cs.toString().toLowerCase().equals("null");
    }
	
	public static boolean isNotBlank(final CharSequence cs) {
		return !isBlank(cs);
    }
	
	public static boolean isBlankObj(final Object obj) {
		if(obj == null){
			return false;
		}
		final CharSequence cs = obj.toString();
		return StringUtils.isBlank(cs) || cs.toString().toLowerCase().equals("undefined") || cs.toString().toLowerCase().equals("null");
    }
	
	public static boolean isNotBlankObj(final Object obj) {
		if(obj == null){
			return true;
		}
		return !isBlankObj(obj);
    }
}
