package com.hcc.flow.server.common.enums;

import java.util.ArrayList;
import java.util.List;

import com.hcc.flow.server.common.vo.SelectVO;

/**
 * 数据状态CRD
 * 
 * @author 韩长志 2019-11-19istrator
 *
 */
public enum DataStatusType {

	/**
	 * 正常C
	 */
	NORMAL("C", "启用"),
	/**
	 * 停用R
	 */
	STOP("R", "停用"),
	/**
	 * 删除D
	 */
	DELETE("D", "删除"),
	/**
	 * 草稿F
	 */
	F("F", "草稿"),
	/**
	 * 已过期E
	 */
	E("E", "已过期");

	private final String code;
	private final String name;

	private DataStatusType(final String code, final String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}
	
	public String getName() {
		return name;
	}
	
	public static String getCode(String sname) {
		DataStatusType[] businessModeEnums = values();
		for (DataStatusType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getName().equals(sname)) {
				return businessModeEnum.getCode();
			}
		}
		return null;
	}

	public static String getName(String scode) {
		DataStatusType[] businessModeEnums = values();
		for (DataStatusType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getCode().equals(scode)) {
				return businessModeEnum.getName();
			}
		}
		return null;
	}
	
	public static String getName(String scode,Boolean check) {
		DataStatusType[] businessModeEnums = values();
		for (DataStatusType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getCode().equals(scode)) {
				if(check != null && check && scode.equals(DataStatusType.STOP.getCode()))
					return "已作废";
				else
					return businessModeEnum.getName();
			}
		}
		return null;
	}
	
	public static List<SelectVO> selectVOsNotDel() {
		List<SelectVO> list = new ArrayList<>();
		for (DataStatusType businessModeEnum : values()) {
			list.add(new SelectVO(businessModeEnum.getCode(), businessModeEnum.getName()));
		}
		return list;
	}
	
	public static List<SelectVO> selectVOsNORMAL() {
		List<SelectVO> list = new ArrayList<>();
		list.add(new SelectVO(DataStatusType.NORMAL.getCode(), DataStatusType.NORMAL.getName()));
		list.add(new SelectVO(DataStatusType.STOP.getCode(), DataStatusType.STOP.getName()));
		return list;
	}
}
