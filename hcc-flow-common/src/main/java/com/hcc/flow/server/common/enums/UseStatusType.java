package com.hcc.flow.server.common.enums;

import java.util.ArrayList;
import java.util.List;

import com.hcc.flow.server.common.vo.SelectVO;

/**
 * 使用状态
 * 
 * @author 韩长志 2019-11-19istrator
 *
 */
public enum UseStatusType {

	/**
	 * 未使用1
	 */
	NOT_USED("1", "未使用"),
	/**
	 * 已使用2
	 */
	ALREADY_USED("2", "已使用"),
	/**
	 * 已删除3
	 */
	DELETE("3", "已删除");

	private String code;
	private String name;

	private UseStatusType(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static String getCode(String sname) {
		UseStatusType[] businessModeEnums = values();
		for (UseStatusType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getName().equals(sname)) {
				return businessModeEnum.getCode();
			}
		}
		return null;
	}

	public static String getName(String scode) {
		UseStatusType[] businessModeEnums = values();
		for (UseStatusType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getCode().equals(scode)) {
				return businessModeEnum.getName();
			}
		}
		return null;
	}
	
	public static List<SelectVO> selectVOsNotDel() {
		List<SelectVO> list = new ArrayList<>();
		for (UseStatusType businessModeEnum : values()) {
			if(UseStatusType.DELETE.getCode().equals(businessModeEnum.getCode())){
				continue;
			}
			list.add(new SelectVO(businessModeEnum.getCode(), businessModeEnum.getName()));
		}
		return list;
	}
}
