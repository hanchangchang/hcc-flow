package com.hcc.flow.server.common.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

public class CollectionsUtils {
	/**
	 * 两个集合的差集
	 * 
	 * @param <T>
	 * 
	 * @param list1
	 * @param list2
	 * @return
	 */
	@SuppressWarnings("unused")
	public static <T> List<T> differenceSet(List<T> list1, List<T> list2) {
		List<T> list = list2.stream().filter(item -> !list1.contains(item)).collect(Collectors.toList());// 新的
		List<T> listTwo = list1.stream().filter(item -> !list2.contains(item)).collect(Collectors.toList());// 旧的

		list.addAll(list1.stream().filter(item -> !list2.contains(item)).collect(Collectors.toList()));
		return list;
	}


	public static <T> List<T> setList(List<T> list) {
		List<T> listNew = new ArrayList<>();
		Set<T> set = new HashSet<>();
		if (list != null && !list.isEmpty()) {
			list.forEach(one -> {
				if (set.add(one)) {
					listNew.add(one);
				}
			});
		}
		return listNew;
	}
	
	public static <T> List<T> castList(Object obj, Class<T> clazz)
	{
	    List<T> result = new ArrayList<T>();
	    if(obj instanceof List<?>)
	    {
	        for (Object o : (List<?>) obj)
	        {
	            result.add(clazz.cast(o));
	        }
	        return result;
	    }
	    return null;
	}
	public static <T> int getMapVelueMax(Map<T,Integer> map) {
		int t=0;
		for(Entry<T,Integer> entry: map.entrySet()) {
			if(entry.getValue()>t)
				t=entry.getValue();
		}
		return t;
	}
}
