package com.hcc.flow.server.common.where;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hcc.flow.server.common.utils.CommUtil;

/**
 * @author 
 * @version createTime：2018年9月28日 上午11:18:59
 * 
 */
public class AuditLogWhere extends PublicWhere implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private  String opUserName;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date beginTime;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date endTime;
	private String moduleId;
	

	public Date getBeginTime() {
		return beginTime != null?CommUtil.getStartOfDay(beginTime):beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = CommUtil.getStartOfDay(beginTime);
	}

	public Date getEndTime() {
		return endTime!=null?CommUtil.getEndOfDay(endTime):endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = CommUtil.getEndOfDay(endTime);
	}

	public String getOpUserName() {
		return opUserName;
	}

	public void setOpUserName(String opUserName) {
		this.opUserName = opUserName;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

}
