package com.hcc.flow.server.common.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.util.CollectionUtils;

public class TimeUtils {
	public static LocalDate date2LocalDate(Date date) {
        Instant instant = date.toInstant();
        ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());
        LocalDate localDate = zdt.toLocalDate();
        return localDate;
    }
	/**
	 * 获取某个时间段的所有日期
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	public static List<String> list(Date beginDate,Date endDate){
		List<String> dates=new ArrayList<String>();
		LocalDate dateBegin = date2LocalDate(beginDate);
		LocalDate dateEnd = date2LocalDate(endDate);
		dateEnd = LocalDate.now();

		while (dateBegin.compareTo(dateEnd) <= 0) {
			dates.add(dateBegin.format(DateTimeFormatter.ISO_LOCAL_DATE));
			dateBegin = dateBegin.plusDays(1);
			
		}
		return dates;
	}
	public static Date string2Date(String string) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			return sdf.parse(string);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
    }
	 /**
     * localDate转Date
     */
    public static Date localDate2Date(LocalDate localDate) {
        ZonedDateTime zonedDateTime = localDate.atStartOfDay(ZoneId.systemDefault());
        Instant instant1 = zonedDateTime.toInstant();
        Date from = Date.from(instant1);
        return from;
    }

	/**
	 * 1近一周 2一个月 3 一年 
	 * @param type
	 * @return
	 */
	public static List<String> list(String type,Date beginDate,Date endDate){
		if(beginDate==null) {
			beginDate=new Date();
		}
		if(endDate==null) {
			endDate=new Date();
		}
		if("1".equals(type)) {
			beginDate = localDate2Date(LocalDate.now().minusWeeks(1).plusDays(1));
		}else if("2".equals(type)) {
			beginDate = localDate2Date(LocalDate.now().minusMonths(1).plusDays(1));
		}else if("3".equals(type)) {
			beginDate = localDate2Date(LocalDate.now().minusYears(1).plusDays(1));
		}
		return list(beginDate,endDate);
	}
	/**
	 * 对set中的不全的list进行补全
	 * @param set
	 * @return
	 */
	public static List<String> list(Set<String> set){
		List<String> list=new ArrayList<>();
		if(!CollectionUtils.isEmpty(set)) {
			set=new TreeSet<>(set);
			set.stream().sorted(Comparator.reverseOrder());
			list.addAll(list(string2date(set.iterator().next()),new Date()));
		}else {
			list.addAll(list(new Date(),new Date()));
		}
		return list;
	}

	
	/**
	 * 
	 * @param type 0 为到此刻  其他为全天
	 * @return
	 */
	public static List<String> getHourList(String type){
		List<String> list=new ArrayList<>();
		int hour=24;
			if(StringUtilsV2.isBlank(type)||"0".equals(type)){
				LocalDateTime now = LocalDateTime.now();
						hour=now.getHour();
			}
			for(int i=0;i<=hour;i++) {
				list.add(i+":00");
			}
		return list;
	}
	
	public static Date string2date(String dateStr) {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return format.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static Date parseTime(String time,String formart) throws ParseException {
		DateFormat formatTime = new SimpleDateFormat(formart);
		return formatTime.parse(time);
	}
}
