package com.hcc.flow.server.common.vo;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class SelectRelationVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4513293760099299996L;

	private String parentId;// 上级ID
	private String parentName;// 上级名称
	private List<SelectCascVO> content;

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public List<SelectCascVO> getContent() {
		return content;
	}

	public void setContent(List<SelectCascVO> content) {
		this.content = content;
	}

}
