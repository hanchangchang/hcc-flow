package com.hcc.flow.server.common.model;

import java.io.Serializable;

/**
 * @author 作者:韩长志
 * @version 创建时间：2018年9月25日 下午10:13:16
 * 
 */
@SuppressWarnings("serial")
public class ApiResult<T> implements Serializable {
	/* 是否成功0成功1失败 */
	private String statusCode = "0";

	/* 状态码 */
	private String code = "200";

	/* 提示信息 */
	private String msg;

	/* 返回的数据 */
	private T data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public static ApiResult<?> ok() {
		return new ApiResult<Object>();
	}

	public static ApiResult<?> error() {
		ApiResult<?> r = new ApiResult<Object>();
		r.setMsg("未知异常，请联系管理员");
		r.setStatusCode("1");
		return r;
	}

	public static ApiResult<?> error(String msg) {
		ApiResult<?> r = new ApiResult<Object>();
		r.setMsg(msg);
		r.setStatusCode("1");
		r.setCode("500");
		return r;
	}

	public static ApiResult<?> error(String code, String msg) {
		ApiResult<?> r = new ApiResult<Object>();
		r.setMsg(msg);
		r.setStatusCode("1");
		r.setCode(code);
		return r;
	}

	public static ApiResult<Object> data(Object data) {
		ApiResult<Object> r = new ApiResult<Object>();
		r.setData(data);
		return r;
	}
	
	public static ApiResult<Object> data(Object data,String msg) {
		ApiResult<Object> r = new ApiResult<Object>();
		r.setData(data);
		r.setMsg(msg);
		return r;
	}
	
	public static ApiResult<Object> msg(String msg) {
		ApiResult<Object> r = new ApiResult<Object>();
		r.setMsg(msg);
		return r;
	}
	
	public boolean isSuccess(){
		return statusCode.equals("0")?true:false;
	}
	
	@Override
    public String toString() {
		final StringBuilder sb = new StringBuilder("ApiResult{");
		sb.append("statusCode=").append(statusCode);
		sb.append(",code=").append(code);
		if(msg == null){
			sb.append(",msg=null");
		}else{
			sb.append(",msg=").append(msg);
		}
		if(data == null){
			sb.append(",data=null");
		}else{
			sb.append(",data=").append(data.toString());
		}
		sb.append('}');
		return sb.toString();
	}
}
