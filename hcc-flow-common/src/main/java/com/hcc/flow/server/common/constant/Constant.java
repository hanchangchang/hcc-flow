package com.hcc.flow.server.common.constant;

import java.util.Arrays;
import java.util.List;

public interface Constant {

	/**
	 * 平台登录页默认logo地址
	 */
	public static final String PLATFORM_WEB_LOGIN_LOGO = "/tourapi/img/logo/webLoginLogo.png";
	/**
	 * 平台默认logo地址
	 */
	public static final String PLATFORM_WEB_LOGO = "/tourapi/img/logo/webLogo.png";
	/**
	 * 平台默认标题
	 */
	public static final String PLATFORM_WEB_TITLE = "hcc-flow运营平台";
	
	public static final String PLATFORM_USER_IMG_MAN = "/tourapi/img/logo/t_man.png";
	
	public static final String PLATFORM_USER_IMG_WOMAN = "/tourapi/img/logo/t_woman.png";
	
	public static final String HEAD_LOGIN_ID = "loginid";
	
	/**
	 * 平台所属机构类型id
	 */
	public static final Long PLATFORM_ORG_TYPE = 1L;
	/**
	 * 顶级平台机构id
	 */
	public static final String PLATFORM_ORG_ID = "1";
	
	/**
	 * 平台方hcc-flow机构id
	 */
	public static final String PLATFORM_JKLZ_ORG_ID = "2";
	/**
	 * 超级管理员角色id
	 */
	public static final String SYSTEM_MANAGE_ROLE_ID = "1";
	public static final List<String> SYSTEM_MANAGE_ROLE_IDS = Arrays.asList("1","6d9a577f5c0511e9ad8f00163e040ce3");
	/**
	 * 超级管理员用户id
	 */
	public static final String SYSTEM_MANAGE_TOP_USER_ID = "1";
	public static final List<String> SYSTEM_MANAGE_TOP_USER_IDS = Arrays.asList("1","10");
	/**
	 * 正常状态
	 */
	public static final String DATA_STATUS_TYPE_NORMAL = "C";
	/**
	 * 草稿状态
	 */
	public static final String DATA_STATUS_TYPE_F = "F";
	/**
	 * 停用状态
	 */
	public static final String DATA_STATUS_TYPE_STOP = "R";
	/**
	 * 未使用状态1
	 */
	public static final String USE_STATUS_TYPE_NOT_USED = "1";
	/**
	 * 使用状态2
	 */
	public static final String USE_STATUS_TYPE_USED = "2";

	/**
	 * 已核准通过、已核准
	 */
	public static final int CHECK_STATUS_WAREHOUSING = 3;

	/**
	 * 待提交
	 */
	public static final int CHECK_STATUS_WAIT_SUB = 1;
	/**
	 * 审核中
	 */
	public static final int CHECK_STATUS_LOADING = 2;

	/**
	 * 删除状态3（使用、未使用、删除）
	 */
	public static final String USE_STATUS_DELETE = "3";
	/**
	 * 删除状态D（数据状态正常C停用R删除D）
	 */
	public static final String DATA_STATUS_DELETE = "D";
	/**
	 * 删除状态5（审核、拒绝、删除）
	 */
	public static final String CHECK_STATUS_DELETE = "5";
	public static final int CHECK_STATUS_DELETE_INT = 5;
	/**
	 * 默认用户密码
	 */
	public static final String DEFALUT_USER_PASSWORD = "88888888";
	/**
	 * 消息未读标识
	 */
	public static final String MSG_READ_TYPE_UNREAD = "0";
	
	public static final String WORK_PREFIX_CODE = "work_prefix";
	
	/**
	 * 组织机构类型id-广告主/商
	 */
	public static final Long ORG_GGZ_TYPE_ID = 4L;
	
	public static final Long ORG_GYS_TYPE_ID = 2L;
	
	public static final String DATE_TO_DAY_REMARKS = "今天";
	
	public static final int WHILE_PAGE_SIZE = 500;
	
	public static final int WHILE_PAGE_EDIT_SIZE = 250;
	
	public static final String REST_API_WRITE_OFF_TOKEN = "writeOffToken";
	
	public static final String REST_API_REST_TOKEN = "restToken";
}
