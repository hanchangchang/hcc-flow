package com.hcc.flow.server.common.enums;

import java.util.ArrayList;
import java.util.List;

import com.hcc.flow.server.common.vo.SelectVO;

public enum OperatingSystemType {
	/**
	 * Android
	 */
	ANDROID("0", "Android"),
	/**
	 * Windows
	 */
	WINDOWS("1", "Windows"),
	/**
	 * 已核准3
	 */
	IOS("2", "IOS"),
	/**
	 * 已驳回4
	 */
	OTHER("3", "HTML5");

	private String code;
	private String name;

	private OperatingSystemType(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static String getCode(String sname) {
		OperatingSystemType[] businessModeEnums = values();
		for (OperatingSystemType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getName().equals(sname)) {
				return businessModeEnum.getCode();
			}
		}
		return null;
	}

	public static String getName(String scode) {
		OperatingSystemType[] businessModeEnums = values();
		for (OperatingSystemType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getCode().equals(scode)) {
				return businessModeEnum.getName();
			}
		}
		return null;
	}
	
	public static List<SelectVO> selectVOs() {
		List<SelectVO> list = new ArrayList<>();
		for (OperatingSystemType businessModeEnum : values()) {
			list.add(new SelectVO(businessModeEnum.getCode(), businessModeEnum.getName()));
		}
		return list;
	}
}
