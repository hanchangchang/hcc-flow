package com.hcc.flow.server.common.enums;

import java.util.ArrayList;
import java.util.List;

import com.hcc.flow.server.common.vo.SelectVO;

/**
 * 审核状态 (1待提交2审核中3已核准4已拒绝5已删除)
 * 
 * @author 韩长志 2019-11-19istrator
 *
 */
public enum CheckStatusType {

	/**
	 * 待提交1
	 */
	SUBMIT(1, "1", "待提交"),
	/**
	 * 审核中2
	 */
	WAIT_AUDITED(2, "2", "审核中"),
	/**
	 * 已核准3
	 */
	AUDITED(3, "3", "已核准"),
	/**
	 * 已驳回4
	 */
	REFUSED(4, "4", "已驳回"),
	/**
	 * 已删除5
	 */
	DELETE(5, "5", "已删除"),
	/**
	 * 已转办
	 */
	DOOFFICE(6, "6", " 已转办"),
	/**
	 * 已作废
	 */
	CANCEL(7, "7", "已作废"),
	/**
	 * 已停用
	 */
	STOP(8, "8", "已停用");

	private Integer id;
	private String code;
	private String name;

	private CheckStatusType(Integer id, String code, String name) {
		this.id = id;
		this.code = code;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static String getCode(String sname) {
		CheckStatusType[] businessModeEnums = values();
		for (CheckStatusType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getName().equals(sname)) {
				return businessModeEnum.getCode();
			}
		}
		return null;
	}

	public static String getName(String scode) {
		CheckStatusType[] businessModeEnums = values();
		for (CheckStatusType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getCode().equals(scode)) {
				return businessModeEnum.getName();
			}
		}
		return null;
	}

	public static List<SelectVO> selectVOsNotDel() {
		List<SelectVO> list = new ArrayList<>();
		for (CheckStatusType businessModeEnum : values()) {
			if (CheckStatusType.DELETE.getCode().equals(businessModeEnum.getCode())) {
				continue;
			}
			list.add(new SelectVO(businessModeEnum.getCode(), businessModeEnum.getName()));
		}
		return list;
	}

	public static String getName(String scode, Boolean check) {
		CheckStatusType[] businessModeEnums = values();
		for (CheckStatusType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getCode().equals(scode)) {
				if (check != null && check && scode.equals(CheckStatusType.AUDITED.getCode()))
					return "正常";
				else
					return businessModeEnum.getName();
			}
		}
		return null;
	}
}
