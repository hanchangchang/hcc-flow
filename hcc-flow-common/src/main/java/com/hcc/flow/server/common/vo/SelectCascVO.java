package com.hcc.flow.server.common.vo;

/**
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class SelectCascVO extends SelectVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4513193760099299996L;

	private String parentId;// 上级ID
	private String parentName;// 上级名称

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

}
