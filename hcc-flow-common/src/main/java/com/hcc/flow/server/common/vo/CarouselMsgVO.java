package com.hcc.flow.server.common.vo;

import java.io.Serializable;

/**
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class CarouselMsgVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4513193760019275996L;

	private String readId;// id
	private String content;// 名称
	public String getReadId() {
		return readId;
	}
	public void setReadId(String readId) {
		this.readId = readId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

}
