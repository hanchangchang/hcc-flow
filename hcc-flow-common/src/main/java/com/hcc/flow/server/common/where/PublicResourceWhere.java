package com.hcc.flow.server.common.where;

import java.io.Serializable;
import java.util.List;

import org.springframework.util.CollectionUtils;


/**
 * @author 韩长志 2019-11-19
 * 
 */
public class PublicResourceWhere extends PublicPara implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<Long> addrAreaIds;// 所在地理区域IDs
	private List<Long> provinceAreaIds;// 所在地理区域IDs
	private List<Long> cityAreaIds;// 所在地理区域IDs
	private List<Long> districtAreaIds;// 所在地理区域IDs
	

	private String sqlOrder;
	private Integer offset;
	private Integer limit;

	public List<Long> getAddrAreaIds() {
		return addrAreaIds;
	}

	public void setAddrAreaIds(List<Long> addrAreaIds) {
		if(!CollectionUtils.isEmpty(addrAreaIds) && addrAreaIds.contains(-1L)){
			addrAreaIds = null;
		}
		this.addrAreaIds = addrAreaIds;
	}

	public List<Long> getProvinceAreaIds() {
		return provinceAreaIds;
	}
	public void setProvinceAreaIds(List<Long> provinceAreaIds) {
		if(!CollectionUtils.isEmpty(provinceAreaIds)){
			this.addrAreaIds = null;
			if(provinceAreaIds.contains(-1L)){
				provinceAreaIds.remove(-1L);
			}
		}
		this.provinceAreaIds = provinceAreaIds;
	}
	public List<Long> getCityAreaIds() {
		return cityAreaIds;
	}
	public void setCityAreaIds(List<Long> cityAreaIds) {
		if(!CollectionUtils.isEmpty(cityAreaIds)){
			this.provinceAreaIds = null;
			this.addrAreaIds = null;
			if(cityAreaIds.contains(-1L)){
				cityAreaIds.remove(-1L);
			}
		}
		this.cityAreaIds = cityAreaIds;
	}
	public List<Long> getDistrictAreaIds() {
		return districtAreaIds;
	}
	public void setDistrictAreaIds(List<Long> districtAreaIds) {
		if(!CollectionUtils.isEmpty(districtAreaIds)){
			this.cityAreaIds = null;
			this.provinceAreaIds = null;
			this.addrAreaIds = null;
			if(districtAreaIds.contains(-1L)){
				districtAreaIds.remove(-1L);
			}
		}
		this.districtAreaIds = districtAreaIds;
	}

	public String getSqlOrder() {
		return sqlOrder;
	}

	public void setSqlOrder(String sqlOrder) {
		this.sqlOrder = sqlOrder;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	
}
