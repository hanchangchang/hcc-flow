package com.hcc.flow.server.common.vo;

import com.hcc.flow.server.common.utils.CommUtil;
import com.hcc.flow.server.common.utils.StringUtilsV2;

/**
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class SelectValueVO extends SelectVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4513193760099299996L;

	private String value;// 值

	public String getValue() {
		if(StringUtilsV2.isNotBlank(value) && value.indexOf(".") > 0 && CommUtil.isNumber(value))
			return value.replaceAll("0+?$", "").replaceAll("[.]$", "");
		else
			return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public SelectValueVO(String id,String name,String value) {
		super();
		this.value = value;
		super.setId(id);
		super.setName(name);
	}

	public SelectValueVO() {
		super();
	}

}
