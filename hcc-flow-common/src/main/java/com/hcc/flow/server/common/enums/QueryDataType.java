package com.hcc.flow.server.common.enums;

/**
 * 查询数据权限
 * 
 * @author 韩长志 2019-11-19istrator
 *
 */
public enum QueryDataType {

	/**
	 * 全部数据
	 */
	ALL("all", "全部数据"),
	/**
	 * 自己和下级数据
	 */
	ONLY("only", "自己和下级数据");

	private String code;
	private String name;

	private QueryDataType(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static String getCode(String sname) {
		QueryDataType[] businessModeEnums = values();
		for (QueryDataType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getName().equals(sname)) {
				return businessModeEnum.getCode();
			}
		}
		return null;
	}

	public static String getName(String scode) {
		QueryDataType[] businessModeEnums = values();
		for (QueryDataType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getCode().equals(scode)) {
				return businessModeEnum.getName();
			}
		}
		return null;
	}
}
