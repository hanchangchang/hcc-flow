package com.hcc.flow.server.common.advice;

/**
 * 自定义异常
 * 
 * @author 韩长志 2019-11-19
 * @date 2016年10月27日 下午10:11:27
 */
public class RRException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private String msg;
	private String code = "500";

	public RRException(String msg) {
		super(msg);
		this.msg = msg;
	}

	public RRException(String msg, Throwable e){
		super(msg, e);
		this.msg = msg;
	}

	public RRException(String msg, String code) {
		super(msg);
		this.msg = msg;
		this.code = code;
	}

	public RRException(String msg, String code, Throwable e) {
		super(msg, e);
		this.msg = msg;
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
