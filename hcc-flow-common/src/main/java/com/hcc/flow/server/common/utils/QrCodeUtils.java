package com.hcc.flow.server.common.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

/**
 * 
 * @author Administrator
 *
 * @version 创建时间：2019年9月18日下午2:55:22 :类说明:二维码生成工具类
 */
public class QrCodeUtils {
	public static String getQrCodeImageForBase64(String url, String type, Integer width, Integer height) {
		Map<EncodeHintType, Object> hints = new HashMap<>();
		// 字符集，内容使用的编码
		hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
		// 容错等级，H、L、M、Q
		hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
		// 边距，二维码距离边的空白宽度
		hints.put(EncodeHintType.MARGIN, 1);
		ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
		String qrCodeStr = null;
		try {
			// 生成二维码对象，传入参数：内容、码的类型、宽高、配置
			BitMatrix bitMatrix = new MultiFormatWriter().encode(url, BarcodeFormat.QR_CODE, width, height, hints);
			// 定义一个路径对象
			// 生成二维码，传入二维码对象、生成图片的格式、生成的路径
			MatrixToImageWriter.writeToStream(bitMatrix, type, pngOutputStream);
			qrCodeStr = Base64.encodeBase64String(pngOutputStream.toByteArray());
		} catch (WriterException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (pngOutputStream != null) {
					pngOutputStream.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return qrCodeStr;
	}
}
