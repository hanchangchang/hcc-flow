package com.hcc.flow.server.common.utils;
/**
 * 
 * @author Administrator
 *
 * @version 创建时间：2019年2月27日上午11:54:42
 *类说明：将对象转成html的工具类
 */
public class HtmlUtil {
	/**
	 * 将两个值分别用span标签包起来的时候再放入p标签中
	 * @param key
	 * @param value
	 * @return
	 */
	public static String addPandSPAN(String key,String value) {
		
		return "<p><span>"+key+"：</span><span>"+value+"<span/></p>";
	}
	
}
