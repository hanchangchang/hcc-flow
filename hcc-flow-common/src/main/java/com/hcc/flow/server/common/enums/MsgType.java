package com.hcc.flow.server.common.enums;

import java.util.ArrayList;
import java.util.List;

import com.hcc.flow.server.common.vo.SelectVO;

/**
 * 消息类型（1任务消息2系统消息3站内信消息）
 * 
 * @author 韩长志 2019-11-19istrator
 *
 */
public enum MsgType {

	/**
	 * 任务消息
	 */
	TASK("1", "任务消息"),
	/**
	 * 系统消息
	 */
	SYS("2", "系统消息"),
	/**
	 * 站内信
	 */
	MSG("3", "站内信"),
	/**
	 * 结果消息
	 */
	RESULT("4", "结果消息");

	private final String code;
	private final String name;

	private MsgType(final String code, final String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public static String getCode(String sname) {
		MsgType[] businessModeEnums = values();
		for (MsgType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getName().equals(sname)) {
				return businessModeEnum.getCode();
			}
		}
		return null;
	}

	public static String getName(String scode) {
		MsgType[] businessModeEnums = values();
		for (MsgType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getCode().equals(scode)) {
				return businessModeEnum.getName();
			}
		}
		return null;
	}
	
	public static MsgType getType(String scode) {
		MsgType[] businessModeEnums = values();
		for (MsgType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getCode().equals(scode)) {
				return businessModeEnum;
			}
		}
		return null;
	}

	public String getName() {
		return name;
	}
	public static List<SelectVO> selectVOs() {
		List<SelectVO> list = new ArrayList<>();
		for (MsgType businessModeEnum : values()) {
			list.add(new SelectVO(businessModeEnum.getCode(), businessModeEnum.getName()));
		}
		return list;
	}
	
}
