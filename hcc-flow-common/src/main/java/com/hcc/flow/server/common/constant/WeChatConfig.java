package com.hcc.flow.server.common.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;



@Configuration
@ConfigurationProperties("product.config")
public class WeChatConfig {
	/***
	 * 项目完整域名
	 */
	public static String origin;
	public static String wechatAppId;
	public static String wechatSecret;
	public static String wechatServerToken;
	public static String jwtSecurity;
	/***
	 * cookie 后缀
	 */
	public static String cookieSuffix;
	/***
	 * cookie 有效期
	 */
	public static Integer cookieTime;
	/***
	 * cookie 有效域名
	 */
	public static String cookieDomain;
	/***
	 * 项目环境
	 */
	public static String active;

	public static String herfApi;
	/***
	 * 路径 前缀
	 */
	public static String herfSuffix;
	
	/***
	 * 拼接cookie名称
	 * @param name
	 * @return
	 */
	public static String getCookieName(String name){
		return WeChatConfig.active + "-" + name + WeChatConfig.cookieSuffix;
	}
	
	public void setWechatAppId(String wechatAppId) {
		WeChatConfig.wechatAppId = wechatAppId;
	}
	public void setWechatSecret(String wechatSecret) {
		WeChatConfig.wechatSecret = wechatSecret;
	}
	public void setWechatServerToken(String wechatServerToken) {
		WeChatConfig.wechatServerToken = wechatServerToken;
	}
	public void setCookieSuffix(String cookieSuffix) {
		WeChatConfig.cookieSuffix = cookieSuffix;
	}
	public void setCookieTime(Integer cookieTime) {
		WeChatConfig.cookieTime = cookieTime;
	}
	public void setCookieDomain(String cookieDomain) {
		WeChatConfig.cookieDomain = cookieDomain;
	}
	
	@Value("${spring.profiles.active}")
	public void setActive(String active) {
		WeChatConfig.active = active;
	}

	public void setOrigin(String origin) {
		WeChatConfig.origin = origin;
	}

	public void setHerfSuffix(String herfSuffix) {
		WeChatConfig.herfSuffix = herfSuffix;
	}

	public void setHerfApi(String herfApi) {
		WeChatConfig.herfApi = herfApi;
	}
	
}
