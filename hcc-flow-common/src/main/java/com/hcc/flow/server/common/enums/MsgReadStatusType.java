package com.hcc.flow.server.common.enums;

import java.util.ArrayList;
import java.util.List;

import com.hcc.flow.server.common.vo.SelectVO;

/**
 * 消息阅读状态
 * 
 * @author 韩长志 2019-11-19istrator
 *
 */
public enum MsgReadStatusType {

	/**
	 * 未读
	 */
	UNREAD("0", "未读"),
	/**
	 * 已读
	 */
	ALREADY("1", "已读"),
	/**
	 * 删除
	 */
	DELETE("2", "删除");

	private final String code;
	private final String name;

	private MsgReadStatusType(final String code, final String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public static String getCode(String sname) {
		MsgReadStatusType[] businessModeEnums = values();
		for (MsgReadStatusType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getName().equals(sname)) {
				return businessModeEnum.getCode();
			}
		}
		return null;
	}

	public static String getName(String scode) {
		MsgReadStatusType[] businessModeEnums = values();
		for (MsgReadStatusType businessModeEnum : businessModeEnums) {
			if (businessModeEnum.getCode().equals(scode)) {
				return businessModeEnum.getName();
			}
		}
		return null;
	}

	public String getName() {
		return name;
	}
	public static List<SelectVO> selectVOsNotDel() {
		List<SelectVO> list = new ArrayList<>();
		for (MsgReadStatusType businessModeEnum : values()) {
			if(businessModeEnum.getCode().equals(MsgReadStatusType.DELETE.getCode())){
				continue;
			}
			list.add(new SelectVO(businessModeEnum.getCode(), businessModeEnum.getName()));
		}
		return list;
	}
	
}
