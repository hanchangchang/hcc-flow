package com.hcc.flow.server.common.vo;

import java.io.Serializable;

/**
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class IdVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4513197760019299996L;

	private String id;// id
	public IdVO(){
		
	}
	public IdVO(String id){
		this.id = id;

	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
