package com.hcc.flow.server.common.enums;


import java.util.ArrayList;
import java.util.List;

import com.hcc.flow.server.common.vo.SelectVO;

public enum CustomerTypeEnum {
	
	/**
	 * 其他0
	 */
	QT(0, "其他"),
	/**
	 * 平台方1
	 */
	PT(1, "平台方"),
	/**
	 * 供应商2
	 */
	GYS(2, "供应商"),
	/**
	 * 分销商3
	 */
	FXS(3, "分销商");

	private Integer id;
	private String name;

	private CustomerTypeEnum(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public static Integer getId(String sname) {
		CustomerTypeEnum[] crandTypeEnums = values();
		for (CustomerTypeEnum crandTypeEnum : crandTypeEnums) {
			if (crandTypeEnum.getName().equals(sname)) {
				return crandTypeEnum.getId();
			}
		}
		return null;
	}

	public static String getName(Integer id) {
		CustomerTypeEnum[] crandTypeEnums = values();
		for (CustomerTypeEnum crandTypeEnum : crandTypeEnums) {
			if (crandTypeEnum.getId().equals(id)) {
				return crandTypeEnum.getName();
			}
		}
		return null;
	}
	
	public static List<SelectVO> selectVOs() {
		List<SelectVO> list = new ArrayList<>();
		for (CustomerTypeEnum crandTypeEnum : values()) {
			list.add(new SelectVO(crandTypeEnum.getId().toString(), crandTypeEnum.getName()));
		}
		return list;
	}
}
