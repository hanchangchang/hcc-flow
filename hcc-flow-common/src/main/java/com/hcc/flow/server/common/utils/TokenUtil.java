package com.hcc.flow.server.common.utils;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Token过滤器
 * 
 * @author ykx
 *
 *         2018年10月14日
 */
@Component
public class TokenUtil {
	private static final String TOKEN_KEY = "token";
	private static final String SYS_ID = "sysid";

	/**
	 * 根据参数或者header获取token
	 * 
	 * @param request
	 * @return
	 */
	public static String getToken(HttpServletRequest request) {
		String  token = request.getHeader(TOKEN_KEY);
		if (StringUtilsV2.isBlank(token)) {
			token = request.getParameter(TOKEN_KEY);
		}
		return token;
	}
	/**
     * 根据参数或者header获取token
     *
     * @param request
     * @return
     */
    public static Integer getSysId(HttpServletRequest request) {
    	if(request == null) {
    		request = getHttpServletRequest();
    	}
        String token = request.getHeader(SYS_ID);
        if (StringUtilsV2.isBlank(token)) {
            token = request.getParameter(SYS_ID);
        } 
        if (StringUtilsV2.isBlank(token)) {
        	token = "1";
        }
        return CommUtil.null2Int(token);
    }
    
    /** 
     * 获取当前请求session 
     * @return 
     */  
    public static HttpServletRequest getHttpServletRequest(){  
    	return ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest(); 
    }     
}
