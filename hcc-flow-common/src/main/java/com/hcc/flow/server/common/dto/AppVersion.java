package com.hcc.flow.server.common.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 应用程序版本
 * @author ykx
 *
 */
public class AppVersion implements Serializable {

	private static final long serialVersionUID = 8583044002165691228L;
	
	private String versionId;//应用程序版本
	private String versionName;//版本名称
	private String deviceType;//设备类型逗号分隔
	private String appVersion;//版本号
	private String appName;//应用文件名称
	private String appAddr;//地址
	private String appTypeParaId;//应用分类
	private String remarks;//备注
	private String md5;//md5验证
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date createTime;//创建时间
	private String createUserId;//创建人ID
	private String status;//状态(1未使用2已使用3已删除)
	private String operatingSystemType;//app类型：0 Android 1 Windows  2 IOS  3 html5
	private String appPackageName;//包名
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getVersionName() {
		return versionName;
	}
	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getAppVersion() {
		return appVersion;
	}
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getAppAddr() {
		return appAddr;
	}
	public void setAppAddr(String appAddr) {
		this.appAddr = appAddr;
	}
	public String getAppTypeParaId() {
		return appTypeParaId;
	}
	public void setAppTypeParaId(String appTypeParaId) {
		this.appTypeParaId = appTypeParaId;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getOperatingSystemType() {
		return operatingSystemType;
	}
	public void setOperatingSystemType(String operatingSystemType) {
		this.operatingSystemType = operatingSystemType;
	}
	public String getMd5() {
		return md5;
	}
	public void setMd5(String md5) {
		this.md5 = md5;
	}
	public String getAppPackageName() {
		return appPackageName;
	}
	public void setAppPackageName(String appPackageName) {
		this.appPackageName = appPackageName;
	}
}
