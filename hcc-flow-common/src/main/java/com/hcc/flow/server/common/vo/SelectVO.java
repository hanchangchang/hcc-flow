package com.hcc.flow.server.common.vo;

import java.io.Serializable;

/**
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class SelectVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4513193760019299996L;

	private String id;// id
	private String name;// 名称
	public SelectVO(){
		
	}
	public SelectVO(String id,String name){
		this.id = id;
		this.name = name;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
