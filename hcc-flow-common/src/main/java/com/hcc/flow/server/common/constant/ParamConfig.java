package com.hcc.flow.server.common.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ParamConfig {

	public static String filePath;
	
	public static String active;
	
	public static String redisOrEhcache;
	
	public static Boolean cacheRedis;


	@Value("${system.config.file-path}")
	public void setFilePath(String filePath) {
		ParamConfig.filePath = filePath;
	}

	@Value("${spring.profiles.active}")
	public void setActive(String active) {
		ParamConfig.active = active;
	}

	@Value("${system.config.cache.redis-or-ehcache}")
	public void setRedisOrEhcache(String redisOrEhcache) {
		ParamConfig.redisOrEhcache = redisOrEhcache;
		cacheRedis = "redis".equals(redisOrEhcache);
		//setCacheRedis("redis".equals(redisOrEhcache));
	}

	public  void setCacheRedis(Boolean cacheRedis) {
		ParamConfig.cacheRedis = cacheRedis;
	}
	
}
