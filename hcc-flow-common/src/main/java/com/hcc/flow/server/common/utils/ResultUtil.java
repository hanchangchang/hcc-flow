package com.hcc.flow.server.common.utils;

import com.hcc.flow.server.common.model.ApiResult;

/**
 * @author 韩长长 
 * @version createTime：2018年9月25日 下午10:28:59
 * 
 */

public class ResultUtil {
	// 当正确时返回的值

	public static ApiResult<Object> success(Object data) {
		ApiResult<Object> result = new ApiResult<Object>();
		result.setStatusCode("0");
		result.setCode("200");
		result.setMsg("OK");
		result.setData(data);
		return result;
	}

	public static ApiResult<String> success(String data) {
		ApiResult<String> result = new ApiResult<String>();
		result.setStatusCode("0");
		result.setCode("200");
		result.setMsg("OK");
		result.setData(data);
		return result;
	}

	public static ApiResult<String> success() {
		ApiResult<String> result = new ApiResult<String>();
		result.setStatusCode("0");
		result.setCode("200");
		result.setMsg("OK");
		result.setData("");
		return result;
	}

	// 当错误时返回的值
	public static ApiResult<Object> error(String statusCode, String code, String msg) {
		ApiResult<Object> result = new ApiResult<Object>();
		result.setStatusCode(statusCode);
		result.setCode(code);
		result.setMsg(msg);
		return result;
	}

}
