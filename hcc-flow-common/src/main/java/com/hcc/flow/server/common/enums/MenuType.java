package com.hcc.flow.server.common.enums;

public enum MenuType {

	/**
	 * 菜单
	 */
	MENU(1),
	/**
	 * 按钮
	 */
	BUTTON(2);

	private Integer value;

	private MenuType(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}
}
