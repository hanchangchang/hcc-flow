package com.hcc.flow.server.common.constant;

public interface RedisKeyConstant {

	public static final String KEY_AREA_LIST = "area_list:";
	
	public static final String KEY_AREA_LIST_VO = "area_list_vo:";
	
	public static final String KEY_AREA_LIST_CHECK_VO = "area_list_check_vo:";
	
	public static final String KEY_CHILD_ORG_IDS = "child_org_ids:";
	
	public static final String KEY_PARENT_ORG_IDS = "parent_org_ids:";
	
	public static final String KEY_CHILD_USER_IDS = "child_user_ids:";
	
	public static final String KEY_MSG_NOTICE = "msg_notice:";
	
	public static final String KEY_H5_TOKEN = "h5:move_user_token:";
	
	public static final String KEY_USER_IDS_BY_ROLE_IDS = "child_user_ids:no_child:";
	
	public static final String MP_TICKET_INTO_BATCH_FILE_KEY = "mp:ticket:input_execl_id:";
	
	public static final String TOKENS_WRITE_OFF_TOKEN = "tokens:write_off_token:";
	
	public static final String TOKENS_REST_BRAND_ACCOUNT_TOKEN = "tokens:rest_brand_account_token:";
}
