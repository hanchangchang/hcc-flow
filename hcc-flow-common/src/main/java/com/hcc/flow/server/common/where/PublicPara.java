package com.hcc.flow.server.common.where;

/**
 * @author 韩长长 
 * @version createTime：2018年9月26日 下午3:52:52 公共接口参数
 */
public class PublicPara {
	String calltype;
	String version;
	String deviceToken;
	String ip;
	String orgGroupCode;
	
	String childOrgIds;// 机构和子机构ids
	String childUserIds;// 角色和子角色所有用户ids
	
	public String getChildOrgIds() {
		return childOrgIds;
	}

	public void setChildOrgIds(String childOrgIds) {
		this.childOrgIds = childOrgIds;
	}

	public String getChildUserIds() {
		return childUserIds;
	}

	public void setChildUserIds(String childUserIds) {
		this.childUserIds = childUserIds;
	}

	public String getCalltype() {
		return calltype;
	}

	public void setCalltype(String calltype) {
		this.calltype = calltype;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getOrgGroupCode() {
		return orgGroupCode;
	}

	public void setOrgGroupCode(String orgGroupCode) {
		this.orgGroupCode = orgGroupCode;
	}
}
