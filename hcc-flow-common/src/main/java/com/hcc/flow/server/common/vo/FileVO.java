package com.hcc.flow.server.common.vo;

import java.io.Serializable;

import com.hcc.flow.server.common.utils.StringUtilsV2;

/**
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class FileVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4513193760019299996L;

	private String md5;// md5
	private String contentType;//文件类型
	private String path;//文件地址
	private String url;//文件相对地址
	@SuppressWarnings("unused")
	private String downUrl;//可下载地址
	private Integer type;//文件类型 1图片2视频3静态文字4文档5压缩包6其他
	private Long size;//文件大小kb
	private String name;//文件名称
	private Long second;// 时长(S) 视频
	private String resolution;// 分辨率 图片和视频
	
	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getUrl() {
		if(StringUtilsV2.isBlank(url)) {
			return null;
		}
		return url.replaceAll("\\\\", "/");
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public String getDownUrl() {
		if(StringUtilsV2.isBlank(url)) {
			return null;
		}
		return "/files/download?url="+url.replaceAll("\\\\", "/");
	}

	public void setDownUrl(String downUrl) {
		this.downUrl = downUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getSecond() {
		return second;
	}

	public void setSecond(Long second) {
		this.second = second;
	}

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}


	
}
