package com.hcc.flow.server.common.where;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import com.hcc.flow.server.common.enums.CheckStatusType;
import com.hcc.flow.server.common.enums.DataStatusType;
import com.hcc.flow.server.common.page.table.PageTableRequest;
import com.hcc.flow.server.common.utils.CommUtil;
import com.hcc.flow.server.common.utils.StringUtilsV2;



/**
 * @author 韩长志 2019-11-19
 * 
 */
public class PublicWhere extends PublicPara implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String selectTypeId;// 查询id
	String keywords;// 名称
	String sqlOrder;//排序
	Integer offset = 1;
	Integer limit = 20;
	String status;//排序
	List<String> statuss;// 查询状态
	
	String notStatus = DataStatusType.DELETE.getCode();//不用查询的DATE状态
	String notCheckStatus = CheckStatusType.DELETE.getCode();//不用查询的状态
	public String getNotStatus() {
		return notStatus;
	}

	public void setNotStatus(String notStatus) {
		this.notStatus = notStatus;
	}

	public String getNotCheckStatus() {
		return notCheckStatus;
	}

	public void setNotCheckStatus(String notCheckStatus) {
		this.notCheckStatus = notCheckStatus;
	}

	public String getSqlOrder() {
		return sqlOrder;
	}

	public void setSqlOrder(String sqlOrder) {
		this.sqlOrder = sqlOrder;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getStatuss() {
		return statuss;
	}

	public void setStatuss(List<String> statuss) {
		this.statuss = statuss;
	}

	public String getSelectTypeId() {
		return selectTypeId;
	}

	public void setSelectTypeId(String selectTypeId) {
		this.selectTypeId = selectTypeId;
	}

	public PublicWhere(PageTableRequest strCon) {
		super();
		this.limit = strCon.getLimit();
		this.offset = strCon.getOffset();
		this.keywords = CommUtil.null2String(strCon.getParams().get("keywords"));
		this.selectTypeId = CommUtil.null2String(strCon.getParams().get("selectTypeId"));
		this.status = CommUtil.null2String(strCon.getParams().get("status"));
		if(StringUtilsV2.isNotBlank(status)){
			this.setStatuss(Arrays.asList(CommUtil.null2String(strCon.getParams().get("status")).split(",")));
		}
		super.setChildOrgIds(CommUtil.null2String(strCon.getParams().get("childOrgIds")));
	}

	public PublicWhere() {
		super();
	}
}
