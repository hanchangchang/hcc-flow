package com.hcc.flow.server.common.vo;

import java.io.Serializable;


/**
 * 
 * @author 韩长志 2019-11-19
 *
 */
public class SelectIdLongVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4513193760019299996L;

	private Long id;// id
	private String name;// 名称

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	/*public SelectVO getSelectVO(){
		return new SelectVO(CommUtil.null2String(id),name);
	}*/

}
